#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wifi-module.h"
#include "ns3/internet-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/ndnSIM-module.h"

#include "ns3/ndnSIM/model/mehdi/ndn-mehdi-consumer.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-location.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-node.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-tools.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-preparation.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace ns3;
using namespace ns3::ndn::mehdi;
using namespace std;

int main (int argc, char *argv[])
{
//	ndn::mehdi::Preparation::ReadText_Calculate_WriteBinary_CheckWrittenBinary();
	ndn::mehdi::Preparation::Start(argc, argv);

	return 0;
}

