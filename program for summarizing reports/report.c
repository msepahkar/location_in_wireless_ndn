#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <iomanip>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <math.h>

using namespace std;

const int n_elements = 8;
const int index_of_time_element = 0;
const int index_of_interests_element = 1;
const int index_of_data_element = 2;
const int index_of_req_element = 3;
const int index_of_succ_element = 4;
const int index_of_error_element = 5;
const string elements[n_elements] = {"time", "interests", "data", "req", "succ", "err", "vmem", "rmem"};
const int lengths[n_elements] = {7, 6, 6, 5, 5, 5, 10, 10};
const int length_of_sd = 5;
const int precision_of_sd = 0;
string report_dir = ".";
long min_time = 0;
long max_time = 1000 * 1000 * 1000;
int th_err = 1000;
bool detailed_report = false;
bool extract_results = false;

/*
 * read the last line of a text file
 */
bool ReadLastLine(string filename, int skip, string& lastLine)
{
    ifstream fin;
    lastLine = "";            
	bool result = true;

    fin.open(filename.c_str());
    if(fin.is_open()) 
	{
		//find the size of the file
		fin.seekg(0, fin.end);
		int length = fin.tellg();
	    int from_end = 0;
		int beginning_of_line;
		    
		do
		{
			do
			{
			    char ch;
				lastLine = "";
			    //go one place before the end or before the previously read character
			    from_end--;
			    if ( !fin.seekg(from_end, fin.end) )
				{
					result = false;
			        break;
				}
			    
			    //if this is the beginning of the file just read one line and break
			    if (from_end == -length)
			    {
			        getline(fin, lastLine);
			        break;
			    }
			    
			    //get the character
			    if (!fin.get(ch))
				{
					result = false;
			        break;
				}
			    
			    //is it an end of line and we have characters after it?
			    if (ch == '\n' && from_end < -1)
			            //read the next line
			            if (!getline(fin, lastLine))
						{
							result = false;
			                break;
						}
			    
			//repeat until a nonempty line is read or the whole file is traversed
			}while (-from_end < length && lastLine == "");
		}while (result && skip-- > 0);

	    if (lastLine == "")
	        result = false;
    }
	else
		result = false;

	return result;
}

/*
 *
 */
vector<string> ReadLines(string file_name)
{
    ifstream fin;
	vector<string> lines;
	string line;

    fin.open(file_name.c_str());
    if(fin.is_open()) 
		while (getline(fin, line))
			lines.push_back(line);
	return lines;
}

/*
 *
 */
string FindLine(long min, long max, string file_name)
{
	vector<string> lines = ReadLines(file_name);
	stringstream ss;
	
	unsigned int i = lines.size() + 1;
	long number;
	do
	{
		i--;
		do
		{
			i--;
			stringstream ss;
			ss << lines[i];
			ss >> number;
		}while (number == 0 && i > 0);
	}while (number > max && i > 0);
	if (number >= min)
		return lines[i];
	return "";
}

/*
 * extract numbers from a line of text
 */
vector<int> ExtractNumbers(string line)
{
	vector<int> v;

	stringstream ss(line);

	double i;

	while (ss >> i)
	{
		v.push_back(int(i));

		while (ss.peek() == ',' || ss.peek() == ' ' || ss.peek() == '\t')
		    ss.ignore();
	}  
	
	return v;
}

/*
 * find name of files with smart propagation
 */
vector<string> FindPropagationFiles(string dir, bool smart, bool real_data, bool other_simulation, bool exact_location)
{
	DIR *dpdf;
	struct dirent *epdf;
	vector<string> file_names;

	if (*dir.rbegin() != '/')
		dir += "/";

	dpdf = opendir(dir.c_str());
	if (dpdf != NULL)
	{
		while (epdf = readdir(dpdf))
		{
			string file_name = epdf->d_name;
			//do not consider working files and interrupted files
			if (file_name.find("_working") == string::npos && file_name.find("_interrupted") == string::npos)
			{
				//other simulation
				if (other_simulation)
				{
					//exact location
					if (exact_location)
					{
						if (file_name.substr(0,4) == "1_1_")
							file_names.push_back(dir + file_name);
					}
					//POI location
					else
					{
						if (file_name.substr(0,4) == "1_0_")
							file_names.push_back(dir + file_name);
					}
				}
				//our simulation
				else if (file_name.substr(0,4) == "0_0_")
				{
					//smart
					if (smart)
					{
						//real data
						if (real_data)
						{
							if (file_name.find("_1_1_") != string::npos)
								file_names.push_back(dir + file_name);
						}
						//prediction data
						else
						{
							if (file_name.find("_1_0_") != string::npos)
								file_names.push_back(dir + file_name);
						}
					}
					//non smart
					else
					{
						if (file_name.find("_0_0_") != string::npos)
							file_names.push_back(dir + file_name);
					}
				}
			}
		}
	}
	return file_names;
}

/*
 *
 */
int Summary(vector<string> files, int min, int max, vector<int>& average, vector<double>& standard_deviation)
{
	int n_files = 0;
	vector<vector<int> > all_numbers;

	//initialize
	average.clear();
	standard_deviation.clear();
	for (int i = 0; i < n_elements; i++)
	{
		average.push_back(0);
		standard_deviation.push_back(0);
	}

	//read files and add the numbers to average
	for (vector<string>::iterator it = files.begin(); it != files.end(); it++)
    {
		string line = FindLine(min, max, *it);
		if (line != "")
		{
			vector<int> numbers = ExtractNumbers(line);
			if (numbers.size() >= n_elements && numbers[index_of_error_element] <= th_err)
			{
				all_numbers.push_back(numbers);
				//add numbers to summaries
				n_files++;
		        for (int i = 0; i < n_elements; i++)
		            average[i] += numbers[i];
				if (detailed_report)
				{
					cout << "*** this file added: " << *it;
					for (int i = 0; i < n_elements; i++)
						cout << ", " << elements[i] << ":" << setw(lengths[i]) << numbers[i];		    
					cout << endl;
				}
			}
		}
    }

	//calcualte the average
	if (n_files > 0)
	{
		//average
		for (int i = 0; i < n_elements; i++)
			average[i] /= n_files;
		//standard deviation
		for (vector<vector<int> >::iterator it = all_numbers.begin(); it != all_numbers.end(); it++)
		{
			for (int i = 0; i < n_elements; i++)
				standard_deviation[i] += pow((*it)[i] - average[i], 2);
		}

		for (int i = 0; i < n_elements; i++)
		{
			standard_deviation[i] /= (double)n_files;
			standard_deviation[i] = sqrt(standard_deviation[i]);
		}
	}

	return n_files;
}

void HandleArgument(string name, string value)
{
	if (value != "")
	{
		if (name == "d" || name == "dir")
			report_dir = value;
		if (name == "min")
		{
			stringstream ss;
			ss << value;
			ss >> min_time;
		}
		if (name == "max")
		{
			stringstream ss;
			ss << value;
			ss >> max_time;
		}
		if (name == "around")
		{
			stringstream ss;
			ss << value;
			long l;
			ss >> l;
			max_time = l + 100000;
			min_time = l - 100000;
		}
		if (name == "e" || name == "error")
		{
			stringstream ss;
			ss << value;
			ss >> th_err;
		}
	}
	if (name == "detailed")
		detailed_report = true;
	if (name == "extract")
		extract_results = true;
	if (name == "h" || name == "help")
	{
		cout << "-d or --dir for report directory." << endl;
		cout << "--detailed for detailed report." << endl;
		cout << "--min for minimum time." << endl;
		cout << "--max for maximum time." << endl;
		cout << "--around for approximate min and max." << endl;
		cout << "-e or --error for threshold of error." << endl;
		cout << "--extact for extracting results." << endl;
		exit(0);
	}
}

void ParseCommandLine(int iargc, char *argv[])
{
  int argc = iargc;

  for (argc--, argv++; argc > 0; argc--, argv++)
    {
      // remove "--" or "-" heading.
      std::string param = *argv;
      std::string::size_type cur = param.find ("--");
      if (cur == 0)
        {
          param = param.substr (2, param.size () - 2);
        }
      else
        {
          cur = param.find ("-");
          if (cur == 0)
            {
              param = param.substr (1, param.size () - 1);
            }
          else
            {
              // invalid argument. ignore.
				cout << "unknown argument: " << param << endl;
              continue;
            }
        }
      cur = param.find ("=");
      std::string name, value;
      if (cur == std::string::npos)
        {
          name = param;
          value = "";
        }
      else
        {
          name = param.substr (0, cur);
          value = param.substr (cur + 1, param.size () - (cur+1));
        }
      HandleArgument (name, value);
    }
}

void Report()
{
	vector<string> smart_real_data_propagation_files = FindPropagationFiles(report_dir, true, true, false, false);
	vector<string> smart_predicted_data_propagation_files = FindPropagationFiles(report_dir, true, false, false, false);
	vector<string> non_smart_propagation_files = FindPropagationFiles(report_dir, false, false, false, false);
	vector<string> other_simulation_exact_location_files = FindPropagationFiles(report_dir, false, false, true, true);
	vector<string> other_simulation_POI_location_files = FindPropagationFiles(report_dir, false, false, true, false);
	vector<int> average_0_0;
	vector<int> average_1_0;
	vector<int> average_1_1;
	vector<int> average_1_0_0_0;
	vector<int> average_1_1_0_0;
	vector<double> standard_deviation_0_0;
	vector<double> standard_deviation_1_0;
	vector<double> standard_deviation_1_1;
	vector<double> standard_deviation_1_0_0_0;
	vector<double> standard_deviation_1_1_0_0;
        
	int non_smart_rounds = Summary(non_smart_propagation_files, min_time, max_time, average_0_0, standard_deviation_0_0);
	int smart_real_data_rounds = Summary(smart_real_data_propagation_files, min_time, max_time, average_1_1, standard_deviation_1_1);
	int smart_predicted_data_rounds = Summary(smart_predicted_data_propagation_files, min_time, max_time, average_1_0, standard_deviation_1_0);
	int other_simulation_exact_location_rounds = Summary(other_simulation_exact_location_files, min_time, max_time, average_1_1_0_0, standard_deviation_1_1_0_0);
	int other_simulation_POI_location_rounds = Summary(other_simulation_POI_location_files, min_time, max_time, average_1_0_0_0, standard_deviation_1_0_0_0);

	cout << endl;

	if ( non_smart_rounds > 0)
	{ 
		cout << "non smart->                            rounds: " << setw(3) << non_smart_rounds;
		for (int i = 0; i < n_elements; i++)
			cout << ", " << elements[i] << ":" << setw(lengths[i]) << average_0_0[i] << ", " << fixed << setw(length_of_sd) << setprecision(precision_of_sd) << standard_deviation_0_0[i];
		cout << endl;
	}

        //smart real data
	if ( smart_real_data_rounds > 0)
	{
		cout << "smart real data->                     rounds: " << setw(3) << smart_real_data_rounds;
		for (int i = 0; i < n_elements; i++)
			cout << ", " << elements[i] << ":" << setw(lengths[i]) << average_1_1[i] << ", " << fixed << setw(length_of_sd) << setprecision(precision_of_sd) << standard_deviation_1_1[i];
		cout << endl;
	}

        //smart predicted data
	if ( smart_predicted_data_rounds > 0)
	{
		cout << "smart predicted data->                rounds: " << setw(3) << smart_predicted_data_rounds;
		for (int i = 0; i < n_elements; i++)
			cout << ", " << elements[i] << ":" << setw(lengths[i]) << average_1_0[i] << ", " << fixed << setw(length_of_sd) << setprecision(precision_of_sd) << standard_deviation_1_0[i];
		cout << endl;
	}

	if ( other_simulation_exact_location_rounds > 0)
	{
		cout << "other simulation exact location->     rounds: " << setw(3) << other_simulation_exact_location_rounds;
		for (int i = 0; i < n_elements; i++)
			cout << ", " << elements[i] << ":" << setw(lengths[i]) << average_1_1_0_0[i] << ", " << fixed << setw(length_of_sd) << setprecision(precision_of_sd) << standard_deviation_1_1_0_0[i];
		cout << endl;
	}

	if ( other_simulation_POI_location_rounds > 0)
	{
		cout << "other simulation POI location->     rounds: " << setw(3) << other_simulation_POI_location_rounds;
		for (int i = 0; i < n_elements; i++)
			cout << ", " << elements[i] << ":" << setw(lengths[i]) << average_1_0_0_0[i] << ", " << fixed << setw(length_of_sd) << setprecision(precision_of_sd) << standard_deviation_1_0_0_0[i];
		cout << endl;
	}

	//comparison smart real data
	if (detailed_report && non_smart_rounds > 0 && smart_real_data_rounds > 0)
	{
		double interests_reduction_ratio_srd_to_ns;
		double data_reduction_ratio_srd_to_ns;
		double succ_ratio_0_0;
		double succ_ratio_1_1;
		double succ_ratio_increase_srd_to_ns;

		succ_ratio_0_0 = (double)average_0_0[index_of_succ_element] / average_0_0[index_of_req_element];
		succ_ratio_1_1 = (double)average_1_1[index_of_succ_element] / average_1_1[index_of_req_element];

		interests_reduction_ratio_srd_to_ns = (double)(average_0_0[index_of_interests_element] - average_1_1[index_of_interests_element]) / average_0_0[index_of_interests_element];
		data_reduction_ratio_srd_to_ns = (double)(average_0_0[index_of_data_element] - average_1_1[index_of_data_element]) / average_0_0[index_of_data_element];
		succ_ratio_increase_srd_to_ns = (double)(succ_ratio_1_1 - succ_ratio_0_0) / succ_ratio_0_0;
		
		cout << endl;
		cout << "smart real data: " << endl;
		cout << endl;
		cout << left << setw(25) << "  succ non smart:                       " << fixed << setprecision(2) << succ_ratio_0_0 * 100 << endl;
		cout << left << setw(25) << "  succ smart real data:                 " << fixed << setprecision(2) << succ_ratio_1_1 * 100 << endl;
		cout << endl;
		cout << left << setw(25) << "  interests reduction: " << fixed << setprecision(2) << interests_reduction_ratio_srd_to_ns * 100 << endl;
		cout << left << setw(25) << "  data reduction: " << fixed << setprecision(2) << data_reduction_ratio_srd_to_ns * 100 << endl;
		cout << left << setw(25) << "  succ increase: " << fixed << setprecision(2) << succ_ratio_increase_srd_to_ns * 100 << endl;
	}

	//comparison smart prediction data
	if (non_smart_rounds > 0 && smart_predicted_data_rounds > 0)
	{
		double interests_reduction_ratio_spd_to_ns;
		double data_reduction_ratio_spd_to_ns;
		double succ_ratio_0_0;
		double succ_ratio_1_0;
		double succ_ratio_increase_spd_to_ns;

		succ_ratio_0_0 = (double)average_0_0[index_of_succ_element] / average_0_0[index_of_req_element];
		succ_ratio_1_0 = (double)average_1_0[index_of_succ_element] / average_1_0[index_of_req_element];

		interests_reduction_ratio_spd_to_ns = (double)(average_0_0[index_of_interests_element] - average_1_0[index_of_interests_element]) / average_0_0[index_of_interests_element];
		data_reduction_ratio_spd_to_ns = (double)(average_0_0[index_of_data_element] - average_1_0[index_of_data_element]) / average_0_0[index_of_data_element];
		succ_ratio_increase_spd_to_ns = (double)(succ_ratio_1_0 - succ_ratio_0_0) / succ_ratio_0_0;
		
		cout << endl;
		cout << "smart prediction data: " << endl;
		cout << endl;
		cout << left << setw(25) << "  succ non smart:                       " << fixed << setprecision(2) << succ_ratio_0_0 * 100 << endl;
		cout << left << setw(25) << "  succ smart prediction data:           " << fixed << setprecision(2) << succ_ratio_1_0 * 100 << endl;
		cout << endl;
		cout << left << setw(25) << "  interests reduction: " << fixed << setprecision(2) << interests_reduction_ratio_spd_to_ns * 100 << endl;
		cout << left << setw(25) << "  data reduction: " << fixed << setprecision(2) << data_reduction_ratio_spd_to_ns * 100 << endl;
		cout << left << setw(25) << "  succ increase: " << fixed << setprecision(2) << succ_ratio_increase_spd_to_ns * 100 << endl;
		cout << endl;
	}
	
	//comparison other simulation exact data
	if (detailed_report && non_smart_rounds > 0 && other_simulation_exact_location_rounds > 0)
	{
		double interests_reduction_ratio_other_exact_to_ns;
		double data_reduction_ratio_other_exact_to_ns;
		double succ_ratio_0_0;
		double succ_ratio_1_1_0_0;
		double succ_ratio_increase_other_exact_to_ns;

		succ_ratio_1_1_0_0 = (double)average_1_1_0_0[index_of_succ_element] / average_1_1_0_0[index_of_req_element];

		interests_reduction_ratio_other_exact_to_ns = (double)(average_0_0[index_of_interests_element] - average_1_1_0_0[index_of_interests_element]) / average_0_0[index_of_interests_element];
		data_reduction_ratio_other_exact_to_ns = (double)(average_0_0[index_of_data_element] - average_1_1_0_0[index_of_data_element]) / average_0_0[index_of_data_element];
		succ_ratio_increase_other_exact_to_ns = (double)(succ_ratio_1_1_0_0 - succ_ratio_0_0) / succ_ratio_0_0;
		
		cout << endl;
		cout << "other simulation exact location: " << endl;
		cout << endl;
		cout << left << setw(25) << "  succ non smart:                       " << fixed << setprecision(2) << succ_ratio_0_0 * 100 << endl;
		cout << left << setw(25) << "  succ other exact location simulaion:  " << fixed << setprecision(2) << succ_ratio_1_1_0_0 * 100 << endl;
		cout << endl;
		cout << left << setw(25) << "  interests reduction: " << fixed << setprecision(2) << interests_reduction_ratio_other_exact_to_ns * 100 << endl;
		cout << left << setw(25) << "  data reduction: " << fixed << setprecision(2) << data_reduction_ratio_other_exact_to_ns * 100 << endl;
		cout << left << setw(25) << "  succ increase: " << fixed << setprecision(2) << succ_ratio_increase_other_exact_to_ns * 100 << endl;
		cout << endl;
	}

	//comparison other simulation POI data
	if (non_smart_rounds > 0 && other_simulation_POI_location_rounds > 0)
	{
		double interests_reduction_ratio_other_POI_to_ns;
		double data_reduction_ratio_other_POI_to_ns;
		double succ_ratio_0_0;
		double succ_ratio_1_0_0_0;
		double succ_ratio_increase_other_POI_to_ns;

		succ_ratio_0_0 = (double)average_0_0[index_of_succ_element] / average_0_0[index_of_req_element];
		succ_ratio_1_0_0_0 = (double)average_1_0_0_0[index_of_succ_element] / average_1_0_0_0[index_of_req_element];

		interests_reduction_ratio_other_POI_to_ns = (double)(average_0_0[index_of_interests_element] - average_1_0_0_0[index_of_interests_element]) / average_0_0[index_of_interests_element];
		data_reduction_ratio_other_POI_to_ns = (double)(average_0_0[index_of_data_element] - average_1_0_0_0[index_of_data_element]) / average_0_0[index_of_data_element];
		succ_ratio_increase_other_POI_to_ns = (double)(succ_ratio_1_0_0_0 - succ_ratio_0_0) / succ_ratio_0_0;
		
		cout << "other simulation POI location: " << endl;
		cout << endl;
		cout << left << setw(25) << "  succ non smart:                       " << fixed << setprecision(2) << succ_ratio_0_0 * 100 << endl;
		cout << left << setw(25) << "  succ other POI location simulaion:    " << fixed << setprecision(2) << succ_ratio_1_0_0_0 * 100 << endl;
		cout << endl;
		cout << left << setw(25) << "  interests reduction: " << fixed << setprecision(2) << interests_reduction_ratio_other_POI_to_ns * 100 << endl;
		cout << left << setw(25) << "  data reduction: " << fixed << setprecision(2) << data_reduction_ratio_other_POI_to_ns * 100 << endl;
		cout << left << setw(25) << "  succ increase: " << fixed << setprecision(2) << succ_ratio_increase_other_POI_to_ns * 100 << endl;
		cout << endl;
	}

}

void ExtractResults()
{
	string all_results_file_name = "all.csv";

	string ns_accuracy_111_file_name = "ns_accuracy_111.csv";
	string ns_accuracy_222_file_name = "ns_accuracy_222.csv";
	string ns_accuracy_333_file_name = "ns_accuracy_333.csv";
	string ns_interest_111_file_name = "ns_interest_111.csv";
	string ns_interest_222_file_name = "ns_interest_222.csv";
	string ns_interest_333_file_name = "ns_interest_333.csv";
	string ns_data_111_file_name = "ns_data_111.csv";
	string ns_data_222_file_name = "ns_data_222.csv";
	string ns_data_333_file_name = "ns_data_333.csv";

	string s_accuracy_111_file_name = "s_accuracy_111.csv";
	string s_accuracy_222_file_name = "s_accuracy_222.csv";
	string s_accuracy_333_file_name = "s_accuracy_333.csv";
	string s_interest_111_file_name = "s_interest_111.csv";
	string s_interest_222_file_name = "s_interest_222.csv";
	string s_interest_333_file_name = "s_interest_333.csv";
	string s_data_111_file_name = "s_data_111.csv";
	string s_data_222_file_name = "s_data_222.csv";
	string s_data_333_file_name = "s_data_333.csv";

	string o_accuracy_111_file_name = "o_accuracy_111.csv";
	string o_accuracy_222_file_name = "o_accuracy_222.csv";
	string o_accuracy_333_file_name = "o_accuracy_333.csv";
	string o_interest_111_file_name = "o_interest_111.csv";
	string o_interest_222_file_name = "o_interest_222.csv";
	string o_interest_333_file_name = "o_interest_333.csv";
	string o_data_111_file_name = "o_data_111.csv";
	string o_data_222_file_name = "o_data_222.csv";
	string o_data_333_file_name = "o_data_333.csv";

	ofstream f_all(all_results_file_name.c_str());

	ofstream f_ns_accuracy_111(ns_accuracy_111_file_name.c_str());
	ofstream f_ns_accuracy_222(ns_accuracy_222_file_name.c_str());
	ofstream f_ns_accuracy_333(ns_accuracy_333_file_name.c_str());
	ofstream f_ns_interest_111(ns_interest_111_file_name.c_str());
	ofstream f_ns_interest_222(ns_interest_222_file_name.c_str());
	ofstream f_ns_interest_333(ns_interest_333_file_name.c_str());
	ofstream f_ns_data_111(ns_data_111_file_name.c_str());
	ofstream f_ns_data_222(ns_data_222_file_name.c_str());
	ofstream f_ns_data_333(ns_data_333_file_name.c_str());
	
	ofstream f_s_accuracy_111(ns_accuracy_111_file_name.c_str());
	ofstream f_s_accuracy_222(ns_accuracy_222_file_name.c_str());
	ofstream f_s_accuracy_333(ns_accuracy_333_file_name.c_str());
	ofstream f_s_interest_111(ns_interest_111_file_name.c_str());
	ofstream f_s_interest_222(ns_interest_222_file_name.c_str());
	ofstream f_s_interest_333(ns_interest_333_file_name.c_str());
	ofstream f_s_data_111(ns_data_111_file_name.c_str());
	ofstream f_s_data_222(ns_data_222_file_name.c_str());
	ofstream f_s_data_333(ns_data_333_file_name.c_str());
	
	ofstream f_o_accuracy_111(ns_accuracy_111_file_name.c_str());
	ofstream f_o_accuracy_222(ns_accuracy_222_file_name.c_str());
	ofstream f_o_accuracy_333(ns_accuracy_333_file_name.c_str());
	ofstream f_o_interest_111(ns_interest_111_file_name.c_str());
	ofstream f_o_interest_222(ns_interest_222_file_name.c_str());
	ofstream f_o_interest_333(ns_interest_333_file_name.c_str());
	ofstream f_o_data_111(ns_data_111_file_name.c_str());
	ofstream f_o_data_222(ns_data_222_file_name.c_str());
	ofstream f_o_data_333(ns_data_333_file_name.c_str());
	
	
	DIR *dpdf_l1, *dpdf_l2;
	struct dirent *epdf_l1, *epdf_l2;
	vector<string> dir_names_l1, dir_names_l2;

	string top_dir = ".";
	
	dpdf_l1 = opendir(top_dir.c_str());
	if (dpdf_l1 != NULL)
	{
		while (epdf_l1 = readdir(dpdf_l1))
		{
			if (epdf_l1->d_type == DT_DIR)
			{
				string d_name = epdf_l1->d_name;
				if (d_name == "." || d_name == "..")
					continue;
				string dir_l1 = top_dir + "/" + d_name;
				cout << "***l1: " << dir_l1 << endl;
				dpdf_l2 = opendir(d_name.c_str());
				if (dpdf_l2 != NULL)
				{
					while (epdf_l2 = readdir(dpdf_l2))
					{
						if (epdf_l2->d_type == DT_DIR)
						{
							string d_name = epdf_l2->d_name;
							if (d_name == "." || d_name == "..")
								continue;
							string dir_l2 = dir_l1 + "/" + d_name;
							cout << "l2: " << dir_l2 << endl;

							ofstream f_all(all_results_file_name.c_str(), ios::app);
							
							for (int i = 0; i < 3; i++)
							{
								vector<int> average;
								vector<double> standard_deviation;
								bool smart = false, real_data = false, other_simulation = false, exact_location = false;
								if (i == 1)
									smart = true;
								if (i == 2)
									other_simulation = true;
								
								vector<string> files = FindPropagationFiles(dir_l2, smart, real_data, other_simulation, exact_location);

								int rounds = Summary(files, min_time, max_time, average, standard_deviation);
								
								if (rounds > 0)
								{
									if (dir_l1 == "./snret1_sfret1_nsret1")
										f_all << "111,";
									if (dir_l1 == "./snret2_sfret2_nsret2")
										f_all << "222,";
									if (dir_l1 == "./snret3_sfret3_nsret3")
										f_all << "333,";
									
									if (dir_l2.find("cache_10") != string::npos && dir_l2.find("cache_100") == string::npos)
										f_all << "10,";
									if (dir_l2.find("cache_20") != string::npos)
										f_all << "20,";
									if (dir_l2.find("cache_30") != string::npos)
										f_all << "30,";
									if (dir_l2.find("cache_40") != string::npos)
										f_all << "40,";
									if (dir_l2.find("cache_50") != string::npos)
										f_all << "50,";
									if (dir_l2.find("cache_60") != string::npos)
										f_all << "60,";
									if (dir_l2.find("cache_70") != string::npos)
										f_all << "70,";
									if (dir_l2.find("cache_80") != string::npos)
										f_all << "80,";
									if (dir_l2.find("cache_90") != string::npos)
										f_all << "90,";
									if (dir_l2.find("cache_100") != string::npos)
										f_all << "100,";
									
									if (smart)
										f_all << "s,";
									else if (other_simulation)
										f_all << "o,";
									else 
										f_all << "n,";
									f_all << rounds << ",";
									
									int n_needed_elements = n_elements - 2;
									for (int j = 0; j < n_needed_elements; j++)
									{
										f_all << elements[j] << ":" << average[j] << "," << fixed << setprecision(0) << standard_deviation[j] << ",";
									}
									f_all << fixed << setprecision(10) << (double)average[index_of_succ_element] / average[index_of_req_element] << endl;
								}	
							}
						}
					}
				}
			}
		}
	}
	f_all.close();
}

/*
 *
 */
int main(int argc, char *argv[])
{
	//check parameters
	ParseCommandLine(argc, argv);

	if (extract_results)
		ExtractResults();
	else
		Report();
	
	return 0;
}
