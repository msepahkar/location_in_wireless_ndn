#! /bin/bash

###############################
#Mehdi Sepahkar
#bash script for installing ndnsim
###############################


#Portability

sudo apt-get -y --force-yes install python-software-properties

sudo add-apt-repository -y ppa:boost-latest/ppa

sudo apt-get -y --force-yes update

sudo apt-get -y --force-yes install libboost1.55-all-dev

# add  --boost-libs=/usr/lib/x86_64-linux-gnu  to ./waf configure for ndn-cxx and ns3
# ./waf configure --boost-libs=/usr/lib/x86_64-linux-gnu

#Prerequisites

sudo apt-get -y --force-yes install python-dev python-pygraphviz python-kiwi

sudo apt-get -y --force-yes install python-pygoocanvas python-gnome2

sudo apt-get -y --force-yes install python-rsvg ipython

