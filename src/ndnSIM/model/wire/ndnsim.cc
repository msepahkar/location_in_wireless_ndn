/** -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil -*- */
/* 
 * Copyright (c) 2013, Regents of the University of California
 *                     Alexander Afanasyev
 * 
 * GNU 3.0 license, See the LICENSE file for more information
 * 
 * Author: Alexander Afanasyev <alexander.afanasyev@ucla.edu>
 */

#include "ndnsim.h"

using namespace std;

#include <ns3/header.h>
#include <ns3/packet.h>
#include <ns3/log.h>

#include "ndnsim/wire-ndnsim.h"

//mehdi adding begin
#include <vector>
#include <boost/crc.hpp>  // for boost::crc_32_type
#include "src/ndnSIM/mehdi/ndn-mehdi-report.h"
#define PACKET_BEFFER_SIZE	10000
//mehdi adding end

NS_LOG_COMPONENT_DEFINE ("ndn.wire.ndnSIM");

NDN_NAMESPACE_BEGIN

namespace wire {
namespace ndnSIM {

NS_OBJECT_ENSURE_REGISTERED (Interest);
NS_OBJECT_ENSURE_REGISTERED (Data);

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////

Interest::Interest ()
  : m_interest (Create<ndn::Interest> ())
{
}

Interest::Interest (Ptr<ndn::Interest> interest)
  : m_interest (interest)
{
}

Ptr<ndn::Interest>
Interest::GetInterest ()
{
  return m_interest;
}


TypeId
Interest::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::ndn::Interest::ndnSIM")
    .SetGroupName ("Ndn")
    .SetParent<Header> ()
    .AddConstructor<Interest> ()
    ;
  return tid;
}

TypeId
Interest::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}

Ptr<Packet>
Interest::ToWire (Ptr<const ndn::Interest> interest)
{
  Ptr<const Packet> p = interest->GetWire ();
  //mehdi commenting (enabling serializing interest every time) begin
  //every time the packet should be serialized because the location data has been changed
  //so the following 'if' is disabled
  //if (!p)
  //mehdi commenting (enabling serializing interest every time) end
    {
      Ptr<Packet> packet = Create<Packet> (*interest->GetPayload ());
      Interest wireEncoding (ConstCast<ndn::Interest> (interest));
      packet->AddHeader (wireEncoding);
      interest->SetWire (packet);

      p = packet;
    }
  
  return p->Copy ();
}

Ptr<ndn::Interest>
Interest::FromWire (Ptr<Packet> packet)
{
  Ptr<ndn::Interest> interest = Create<ndn::Interest> ();
  Ptr<Packet> wire = packet->Copy ();

  Interest wireEncoding (interest);
  packet->RemoveHeader (wireEncoding);

  interest->SetPayload (packet);
  interest->SetWire (wire);

  return interest;
}

uint32_t
Interest::GetSerializedSize (void) const
{
	size_t size =
    1/*version*/ + 1 /*type*/ + 2/*length*/ +
    (4/*nonce*/ + 1/*scope*/ + 1/*nack type*/ + 2/*timestamp*/ +
     NdnSim::SerializedSizeName (m_interest->GetName ()) +

     (2 +
    //mehdi adding begin
	sizeof(uint32_t) + //original sender of interest packet (for avoiding propagation of data in the original sender)
	sizeof(uint32_t) + //sender of the packet (just for debugging purposes)
	sizeof(uint8_t) + //force forwarding bit
	sizeof(uint8_t) + //INT
	sizeof(uint8_t) + //ACK
	sizeof(uint64_t) + //x
	sizeof(uint64_t) + //y
	sizeof(uint64_t) + //distance
	sizeof(uint8_t) + //CMD
	sizeof(uint32_t) + //CMD_selected_sender
	//mehdi adding end
      (m_interest->GetExclude () == 0 ? 0 : (1 + NdnSim::SerializedSizeExclude (*m_interest->GetExclude ())))
      )/* selectors */ +
     
     (2 + 0)/* options */);

	 //mehdi adding begin
	 size += 4;	//checksum
	 //mehdi adding end

  NS_LOG_INFO ("Serialize size = " << size);
  return size;
}
    
void
Interest::Serialize (Buffer::Iterator start) const
{
	//mehdi adding begin

	//keep the begining of the buffer
	//because in the end we want to calculate checksum
	Buffer::Iterator original_start = start;

	//mehdi adding end

  start.WriteU8 (0x80); // version
  start.WriteU8 (0x00); // packet type

  start.WriteU16 (GetSerializedSize () - 4);

  start.WriteU32 (m_interest->GetNonce ());

  //mehdi adding begin
//  mehdi::loc_chain_container *container = m_interest->GetLocationChainContainer();
//  //number of location chains
//  uint32_t number_of_location_chains = container->size(); //first read in uint32_t variable for making sure about the size
//  start.WriteU32(number_of_location_chains);
//  //serialize all locations
//  for(mehdi::loc_chain_iterator it = container->begin(); it != container->end(); it++ )
//	  if (mehdi::mehdi_compact_serializatoin)
//		  it->Serialize32(start);
//	  else
//		  it->Serialize64(start);

  start.WriteU32(m_interest->GetOriginalSender());
  start.WriteU32(m_interest->GetSender());
  start.WriteU8 (m_interest->GetForceForwarding());
  start.WriteU8 (m_interest->GetINT());
  start.WriteU8 (m_interest->GetACK());
  start.WriteU64(m_interest->GetX());
  start.WriteU64(m_interest->GetY());
  start.WriteU64(m_interest->GetDistance());
  start.WriteU8 (m_interest->GetCMD());
  start.WriteU32(m_interest->GetCMD_SelectedSender());
  //mehdi adding end

  start.WriteU8 (m_interest->GetScope ());
  start.WriteU8 (m_interest->GetNack ());

  NS_ASSERT_MSG (0 <= m_interest->GetInterestLifetime ().ToInteger (Time::S) && m_interest->GetInterestLifetime ().ToInteger (Time::S) < 65535,
                 "Incorrect InterestLifetime (should not be smaller than 0 and larger than 65535");
  
  // rounding timestamp value to seconds
  start.WriteU16 (static_cast<uint16_t> (m_interest->GetInterestLifetime ().ToInteger (Time::S)));

  NdnSim::SerializeName (start, m_interest->GetName ());

  if (m_interest->GetExclude () == 0)
    {
      start.WriteU16 (0); // no selectors
    }
  else
    {
      start.WriteU16 (1 + NdnSim::SerializedSizeExclude (*m_interest->GetExclude ()));
      start.WriteU8 (0x01);
      NdnSim::SerializeExclude (start, *m_interest->GetExclude ());
    }
  
  start.WriteU16 (0); // no options

  //mehdi adding begin
  try
  {
	  char buffer[PACKET_BEFFER_SIZE];
	  for (uint i = 0; i < GetSerializedSize() - 4; i++)
		  buffer[i] = original_start.ReadU8();
	  boost::crc_32_type  content;
	  content.process_bytes(buffer, GetSerializedSize() - 4);
	  start.WriteU32(content.checksum());
  }catch(const std::exception& e)
  {
	  MEHDI_ERROR_LINE("Error occurred while calculating checksum of interest packet for sending: " << e.what());
	  start.WriteU32(0);
  }
  //mehdi adding end
}

uint32_t
Interest::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;
  
  if (i.ReadU8 () != 0x80)
    throw new InterestException ();

  if (i.ReadU8 () != 0x00)
    throw new InterestException ();

  i.ReadU16 (); // length, don't need it right now
  
  m_interest->SetNonce (i.ReadU32 ());

  //mehdi adding begin
//  //clear previous location chains
//  m_interest->ClearLocationChainContainer();
//  //first read number of chains
//  uint32_t numberOfLocationChains = i.ReadU32();
//  //now read all chains one by one
//  for (uint32_t j = 0; j < numberOfLocationChains; j++)
//  {
//	  //create an empty chain of locations
//	  mehdi::LocationChain location_chain;
//	  if (mehdi::mehdi_compact_serializatoin)
//		  location_chain.Deserialize32(i);
//	  else
//		  location_chain.Deserialize64(i);
//	  //add the chain to the chain collection
//	  m_interest->AddLocationChain(location_chain);
//  }
  m_interest->SetOriginalSender(i.ReadU32());
  m_interest->SetSender(i.ReadU32());
  m_interest->SetForceForwarding(i.ReadU8());
  m_interest->SetINT(i.ReadU8());
  m_interest->SetACK(i.ReadU8());
  m_interest->SetX(i.ReadU64());
  m_interest->SetY(i.ReadU64());
  m_interest->SetDistance(i.ReadU64());
  m_interest->SetCMD(i.ReadU8());
  m_interest->SetCMD_SelectedSender(i.ReadU32());
  //mehdi adding end

  m_interest->SetScope (i.ReadU8 ());
  m_interest->SetNack (i.ReadU8 ());

  m_interest->SetInterestLifetime (Seconds (i.ReadU16 ()));

  //mehdi adding begin
  try{
  //mehdi adding end
  m_interest->SetName (NdnSim::DeserializeName (i));
  //mehdi adding begin
  }
  catch(const exception& e)
  {
	  MEHDI_ERROR_LINE("error setting the name of the received interest while deserializing: " << e.what());
  }
  //mehdi adding end
  
  uint32_t selectorsLen = i.ReadU16 ();
  if (selectorsLen > 0)
    {
      if (i.ReadU8 () != 0x01) // exclude filter only
        throw InterestException ();

      m_interest->SetExclude (NdnSim::DeserializeExclude (i));
    }
  i.ReadU16 ();

  //mehdi adding begin
  bool checksum_ok = false;
  try
  {
	  Buffer::Iterator i2 = start;
	  char buffer[PACKET_BEFFER_SIZE];
	  for (uint j = 0; j < i.GetDistanceFrom(start); j++)
		  buffer[j] = i2.ReadU8();
	  boost::crc_32_type  content;
	  content.process_bytes(buffer, GetSerializedSize() - 4);
	  if (content.checksum() == i.ReadU32())
		  checksum_ok = true;
	  else
	  {
		  mehdi::Report::IncrementErrorCounter();
		  MEHDI_ERROR_LINE("checksum of the received interest does not match.");
	  }
  }
  catch(const exception& e)
  {
	  MEHDI_ERROR_LINE("error in calculating checksum of received interest: " << e.what());
	  mehdi::Report::IncrementErrorCounter();
  }
  //if checksum is not ok, forget about the assert
  if (checksum_ok)
  //mehdi adding end
	  NS_ASSERT (GetSerializedSize () == (i.GetDistanceFrom (start)));

  return i.GetDistanceFrom (start);
}

void
Interest::Print (std::ostream &os) const
{
  m_interest->Print (os);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////


TypeId
Data::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::ndn::Data::ndnSIM")
    .SetGroupName ("Ndn")
    .SetParent<Header> ()
    .AddConstructor<Data> ()
    ;
  return tid;
}

TypeId
Data::GetInstanceTypeId (void) const
{
  return GetTypeId ();
}
  

Data::Data ()
  : m_data (Create<ndn::Data> ())
{
}

Data::Data (Ptr<ndn::Data> data)
  : m_data (data)
{
}

Ptr<ndn::Data>
Data::GetData ()
{
  return m_data;
}

Ptr<Packet>
Data::ToWire (Ptr<const ndn::Data> data)
{
  Ptr<const Packet> p = data->GetWire ();
  //mehdi commenting (enabling every time serialization of data) begin
  //every time the packet should be serialized because the location data has been changed
  //so the following 'if' is disabled
  //if (!p)
  //mehdi commenting (enabling every time serialization of data) end
    {
      Ptr<Packet> packet = Create<Packet> (*data->GetPayload ());
      Data wireEncoding (ConstCast<ndn::Data> (data));
      packet->AddHeader (wireEncoding);
      data->SetWire (packet);

      p = packet;
    }
  
  return p->Copy ();
}

Ptr<ndn::Data>
Data::FromWire (Ptr<Packet> packet)
{
  Ptr<ndn::Data> data = Create<ndn::Data> ();
  Ptr<Packet> wire = packet->Copy ();

  Data wireEncoding (data);
  packet->RemoveHeader (wireEncoding);

  data->SetPayload (packet);
  data->SetWire (wire);

  return data;
}

uint32_t
Data::GetSerializedSize () const
{
	//mehdi adding begin
	size_t mehdi_size;
	//for saving number of locations
	mehdi_size = sizeof(uint32_t);
	//get location container of data packet
	mehdi::loc_container *container = m_data->GetLocationContainer();
	//add size of serialized elements for locations of the chain
	if (mehdi::mehdi_compact_serializatoin)
		mehdi_size += sizeof(uint32_t) * mehdi::Location::GetNumberOf_32_bit_SerializedItems() * container->size();
	else
		mehdi_size += sizeof(uint64_t) * mehdi::Location::GetNumberOf_64_bit_SerializedItems() * container->size();

	//mehdi adding end

  uint32_t size = 1 + 1 + 2 +
    ((2 + 2) +
     NdnSim::SerializedSizeName (m_data->GetName ()) +

	//mehdi adding begin
	 mehdi_size + //location chains
	sizeof(uint32_t) + //original sender of data packet (for avoiding propagation of interest in the data producer)
	sizeof(uint32_t) + //sender id (just for debugging purposes)
	 sizeof(uint8_t) + //force forwarding bit
	 //mehdi adding end

     (2 + 2 + 4 + 2 + 2 + (2 + 0)));
  if (m_data->GetSignature () != 0)
    size += 4;

  //mehdi adding begin
  size += 4;	//checksum
  //mehdi adding end
  
  NS_LOG_INFO ("Serialize size = " << size);
  return size;
}

void
Data::Serialize (Buffer::Iterator start) const
{
	//mehdi adding begin

	//keep the begining of the buffer
	//because in the end we want to calculate checksum
	Buffer::Iterator original_start = start;

	//mehdi adding end

  start.WriteU8 (0x80); // version
  start.WriteU8 (0x01); // packet type
  start.WriteU16 (GetSerializedSize () - 4); // length
  
  if (m_data->GetSignature () != 0)
    {
      start.WriteU16 (6); // signature length
      start.WriteU16 (0xFF00); // "fake" simulator signature
      start.WriteU32 (m_data->GetSignature ());
    }
  else
    {
      start.WriteU16 (2); // signature length
      start.WriteU16 (0); // empty signature
    }

  // name
  NdnSim::SerializeName (start, m_data->GetName ());

  //mehdi adding begin
  mehdi::loc_container *container = m_data->GetLocationContainer();
  uint32_t number_of_locations = container->size(); //first read in uint32_t variable for making sure about the size
  start.WriteU32(number_of_locations);
  //serialize all locations
  for(mehdi::loc_iterator it = container->begin(); it != container->end(); it++ )
  {
	  if (mehdi::mehdi_compact_serializatoin)
		  it->Serialize32(start);
	  else
		  it->Serialize64(start);
  }
  start.WriteU32(m_data->GetOriginalSender());
  start.WriteU32(m_data->GetSender());
  start.WriteU8(m_data->GetForceForwarding());
  //mehdi adding end

  // content
  // for now assume that contentdata length is zero
  start.WriteU16 (2 + 4 + 2 + 2 + (2 + 0));
  start.WriteU16 (4 + 2 + 2 + (2 + 0));
  start.WriteU32 (static_cast<uint32_t> (m_data->GetTimestamp ().ToInteger (Time::S)));
  start.WriteU16 (static_cast<uint16_t> (m_data->GetFreshness ().ToInteger (Time::S)));
  start.WriteU16 (0); // reserved 
  start.WriteU16 (0); // Length (ContentInfoOptions)

  // that's it folks

  //mehdi adding begin
  try
  {
	  char buffer[PACKET_BEFFER_SIZE];
	  for (uint i = 0; i < GetSerializedSize() - 4; i++)
		  buffer[i] = original_start.ReadU8();
	  boost::crc_32_type  content;
	  content.process_bytes(buffer, GetSerializedSize() - 4);
	  start.WriteU32(content.checksum());
  }catch(const std::exception& e)
  {
	  MEHDI_ERROR_LINE("Error occurred while calculating checksum of interest packet for sending: " << e.what());
	  start.WriteU32(0);
  }
  //mehdi adding end
}

uint32_t
Data::Deserialize (Buffer::Iterator start)
{
  Buffer::Iterator i = start;

  if (i.ReadU8 () != 0x80)
    throw new DataException ();

  if (i.ReadU8 () != 0x01)
    throw new DataException ();

  i.ReadU16 (); // length

  uint32_t signatureLength = i.ReadU16 ();
  if (signatureLength == 6)
    {
      if (i.ReadU16 () != 0xFF00) // signature type
        throw new DataException ();
      m_data->SetSignature (i.ReadU32 ());
    }
  else if (signatureLength == 2)
    {
      if (i.ReadU16 () != 0) // signature type
        throw new DataException ();
      m_data->SetSignature (0);
    }
  else
    throw new DataException ();

  //mehdi adding begin
  try{
  //mehdi adding end

  m_data->SetName (NdnSim::DeserializeName (i));

  //mehdi adding begin
  }
  catch(const exception& e)
  {
	  MEHDI_ERROR_LINE("error setting the name of the received data while serializing: " << e.what());
  }
  //mehdi adding end

  //mehdi adding begin
  m_data->ClearLocationContainer();
  //first read number of locations
  uint32_t numberOfLocations = i.ReadU32();
  //now read all chains one by one
  for (uint32_t j = 0; j < numberOfLocations; j++)
  {
	  //create a location
	  mehdi::Location location;
	  if (mehdi::mehdi_compact_serializatoin)
		  location.Deserialize32(i);
	  else
		  location.Deserialize64(i);
	  //add the location to the container
	  m_data->AddLocation(location);
  }
  m_data->SetOriginalSender(i.ReadU32());
  m_data->SetSender(i.ReadU32());
  m_data->SetForceForwarding(i.ReadU8());
  //mehdi adding end

  if (i.ReadU16 () != (2 + 4 + 2 + 2 + (2 + 0))) // content length
    throw new DataException ();

  if (i.ReadU16 () != (4 + 2 + 2 + (2 + 0))) // Length (content Info)
    throw new DataException ();

  m_data->SetTimestamp (Seconds (i.ReadU32 ()));
  m_data->SetFreshness (Seconds (i.ReadU16 ()));

  if (i.ReadU16 () != 0) // Reserved
    throw new DataException ();
  if (i.ReadU16 () != 0) // Length (ContentInfoOptions)
    throw new DataException ();

  //mehdi adding begin
  bool checksum_ok = false;
  try
  {
	  Buffer::Iterator i2 = start;
	  char buffer[PACKET_BEFFER_SIZE];
	  for (uint j = 0; j < i.GetDistanceFrom(start); j++)
		  buffer[j] = i2.ReadU8();
	  boost::crc_32_type  content;
	  content.process_bytes(buffer, GetSerializedSize() - 4);
	  if (content.checksum() == i.ReadU32())
		  checksum_ok = true;
	  else
	  {
		  MEHDI_ERROR_LINE("checksum of the received data does not match.");
		  mehdi::Report::IncrementErrorCounter();
	  }
  }
  catch(const exception& e)
  {
	  MEHDI_ERROR_LINE("error in calculating checksum of received data: " << e.what());
	  mehdi::Report::IncrementErrorCounter();
  }
  //if checksum is not ok, forget about the assert
  if (checksum_ok)
  //mehdi adding end
	  NS_ASSERT_MSG (i.GetDistanceFrom (start) == GetSerializedSize (), "Something wrong with Data::Deserialize");
  
  return i.GetDistanceFrom (start);
}

void
Data::Print (std::ostream &os) const
{
  m_data->Print (os);
}

} // ndnSIM
} // wire

NDN_NAMESPACE_END
