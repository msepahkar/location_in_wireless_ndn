/*
 * ndn-mehdi-consumer.h
 *
 *  Created on: Jun 13, 2016
 *      Author: mehdi
 */

#ifndef NDN_MEHDI_CONSUMER_H_
#define NDN_MEHDI_CONSUMER_H_

#include "ns3/ndn-app.h"
#include "ns3/ndn-name.h"
#include <boost/thread/mutex.hpp>

namespace ns3 {
namespace ndn {
namespace mehdi {

using namespace std;

class MehdiConsumer : public ndn::App
{
public:
  // register NS-3 type "MehdiConsumer"
  static TypeId
  GetTypeId ();

  MehdiConsumer ();

  // (overridden from ndn::App) Processing upon start of the application
  virtual void
  StartApplication ();

  // (overridden from ndn::App) Processing when application is stopped
  virtual void
  StopApplication ();

  // (overridden from ndn::App) Callback that will be called when Data arrives
  virtual void
  OnData (Ptr<const ndn::Data> contentObject);

private:
  void
  SendInterest ();

  void
  ScheduleNextPacket();

private:
  bool m_isRunning;
  bool m_firstTime;
  bool m_data_received;
  bool m_retransmission;
  unsigned int m_non_smart_retransmission_count;
  unsigned int m_smart_normal_retransmission_count;
  unsigned int m_smart_force_forward_retransmission_count;
  bool m_force_forwarding;
  EventId m_sendEvent;
  ndn::Name m_name;
};

} // namespace mehdi
} // namespace ndn
} // namespace ns3

#endif /* NDN_MEHDI_CONSUMER_H_ */
