/*
 * ndn-mehdi-consumer.cc
 *
 *  Created on: Jun 13, 2016
 *      Author: mehdi
 */

#include "ndn-mehdi-consumer.h"
#include "ns3/ptr.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/packet.h"
#include "ns3/random-variable.h"
#include "ns3/string.h"

#include "ns3/ndn-app-face.h"
#include "ns3/ndn-interest.h"
#include "ns3/ndn-data.h"

#include "src/ndnSIM/mehdi/ndn-mehdi-node.h"

NS_LOG_COMPONENT_DEFINE ("ndn.mehdi.MehdiConsumer");

namespace ns3 {
namespace ndn {
namespace mehdi {

using namespace std;

NS_OBJECT_ENSURE_REGISTERED (MehdiConsumer);

// register NS-3 type
TypeId
MehdiConsumer::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::ndn::mehdi::MehdiConsumer")
    .SetParent<ndn::App> ()
    .AddConstructor<MehdiConsumer> ()

    .AddAttribute ("Prefix", "Requested name",
                   StringValue ("/dumb-interest"),
                   ndn::MakeNameAccessor (&MehdiConsumer::m_name),
                   ndn::MakeNameChecker ())
    ;
  return tid;
}

MehdiConsumer::MehdiConsumer ()
  : m_isRunning (false),
	m_firstTime (false),
	m_data_received(true),
	m_retransmission(false),
	m_non_smart_retransmission_count(0),
	m_smart_normal_retransmission_count(0),
	m_smart_force_forward_retransmission_count(0),
	m_force_forwarding(false)
{
}

// Processing upon start of the application
void
MehdiConsumer::StartApplication ()
{
  // initialize ndn::App
  ndn::App::StartApplication ();

  m_isRunning = true;
  m_firstTime = true;
  Simulator::ScheduleNow (&MehdiConsumer::ScheduleNextPacket, this);
}

// Processing when application is stopped
void
MehdiConsumer::StopApplication ()
{
  m_isRunning = false;
  // cleanup ndn::App
  ndn::App::StopApplication ();
}

void
MehdiConsumer::SendInterest ()
{

	if (!m_isRunning) return;

	//not retransmission? so this is a new request.
	if (!m_retransmission)
	{
		//get the node of the consumer
		Ptr<MehdiNode> node = this->GetNode()->GetObject<MehdiNode>();
		//is there any request in the queue?
		if (node->request_times.size() == 0)
			return;
		//set the name of interest
		m_name = node->request_times.front().first;
		node->request_times.pop();
	}


  /////////////////////////////////////
  // Sending one Interest packet out //
  /////////////////////////////////////

  Ptr<ndn::Name> prefix = Create<ndn::Name> (m_name); // another way to create name

  // Create and configure ndn::Interest
  Ptr<ndn::Interest> interest = Create<ndn::Interest> ();
  UniformVariable rand (0,std::numeric_limits<uint32_t>::max ());
  interest->SetNonce            (rand.GetValue ());
  interest->SetName             (prefix);
  interest->SetInterestLifetime (Seconds (1.0));

  if (m_force_forwarding)
	  interest->SetForceForwarding(1);

  NS_LOG_DEBUG ("Sending Interest packet for " << *prefix);


  // Call trace (for logging purposes)
  m_transmittedInterests (interest, this, m_face);

  m_data_received = false;

  if (!m_retransmission)
	  Report::n_requests++;

  // Forward packet to lower (network) layer
  m_face->ReceiveInterest (interest);

  Simulator::Schedule (Seconds (3.0), &MehdiConsumer::ScheduleNextPacket, this);

}

void
MehdiConsumer::ScheduleNextPacket()
{
	Ptr<MehdiNode> node = this->GetNode()->GetObject<MehdiNode>();

	//first suppose no retransmission
	m_retransmission = false;
	//no data received?
	if (!m_data_received)
	{
		//smart retransmission
		if (!Parameters::other_simulation && Parameters::smart_propagation)
		{
			//without force forwarding
			if (m_smart_normal_retransmission_count < Parameters::max_smart_normal_retransmit)
			{
				m_force_forwarding = false;
				m_retransmission = true;
				m_smart_normal_retransmission_count++;
				m_sendEvent = Simulator::ScheduleNow(&MehdiConsumer::SendInterest, this);
			}
			//with force forwarding
			else if (m_smart_force_forward_retransmission_count < Parameters::max_smart_force_forward_retransmit)
			{
				m_force_forwarding = true;
				m_retransmission = true;
				m_smart_force_forward_retransmission_count++;
				m_sendEvent = Simulator::ScheduleNow(&MehdiConsumer::SendInterest, this);
			}
		}
		//non-smart retransmission
		else if (m_non_smart_retransmission_count < Parameters::max_non_smart_retransmit)
		{
			m_retransmission = true;
			m_non_smart_retransmission_count++;
			m_sendEvent = Simulator::ScheduleNow(&MehdiConsumer::SendInterest, this);
		}
	}
	//this is a new interest not a retransmission
	//either because m_data_received is true or because maximum number of retransmissions are done
	if (!m_retransmission)
	{
		//reset parameters
		m_force_forwarding = false;
		m_retransmission = false;
		m_non_smart_retransmission_count = 0;
		m_smart_normal_retransmission_count = 0;
		m_smart_force_forward_retransmission_count = 0;

		const elapsed_time min_interval_between_requests = 10;

		if (node->request_times.size() > 0)
		{
			elapsed_time t = node->request_times.front().second;
			elapsed_time interval = m_firstTime ? 0 : min_interval_between_requests;
			if (t >  Simulator::Now().GetSeconds() + interval)
				t -= Simulator::Now().GetSeconds();
			else
				t = interval;	//at least 10 seconds between requests
			if (m_firstTime)
			{
				m_sendEvent = Simulator::Schedule(Seconds(t), &MehdiConsumer::SendInterest, this);
				m_firstTime = false;
			}
			else if (!m_sendEvent.IsRunning ())
			{
				m_sendEvent = Simulator::Schedule(Seconds(t), &MehdiConsumer::SendInterest, this);
			}
		}
	}
}

void
MehdiConsumer::OnData (Ptr<const ndn::Data> contentObject)
{
  NS_LOG_DEBUG ("Receiving Data packet for " << contentObject->GetName ());
  m_data_received = true;
  Report::n_successful_requests++;
}

} // namespace mehdi
} // namespace ndn
} // namespace ns3




