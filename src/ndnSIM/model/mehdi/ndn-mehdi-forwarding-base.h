/*
 * ndn-mehdi-forwarding.h
 *
 *  Created on: Feb 15, 2016
 *      Author: mehdi
 */

#ifndef NDN_MEHDI_FORWARDING_BASE_H_
#define NDN_MEHDI_FORWARDING_BASE_H_

#include "ns3/ndnSIM/model/fw/nacks.h"
#include "ns3/log.h"
#include "ns3/random-variable.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-location.h"

namespace ns3 {
namespace ndn {
namespace mehdi {
using namespace fw;

/**
 * @ingroup ndn-fw
 * @brief mehdi flooding strategy
 *
 * Interests will be forwarded ...
 *
 * Usage example:
 * @code
 *     ndnHelper.SetForwardingStrategy ("ns3::ndn::mehdi::MehdiForwarding");
 *     ...
 *     ndnHelper.Install (nodes);
 * @endcode
 */
class MehdiForwardingBase :
    public Nacks
{
private:
  typedef Nacks super;

public:
  static TypeId
  GetTypeId ();

  /**
   * @brief Helper function to retrieve logging name for the forwarding strategy
   */
  static std::string
  GetLogName ();

  /**
   * @brief Default constructor
   */
  MehdiForwardingBase ();

  /**
   * \brief Actual processing of incoming Ndn interests. Note, interests do not have payload
   *
   * Processing Interest packets
   * @param face     incoming face
   * @param interest Interest packet
   */
  virtual void
  OnInterest (Ptr<Face> face,
              Ptr<Interest> interest);

  /**
   * \brief Actual processing of incoming Ndn content objects
   *
   * Processing Data packets
   * @param face    incoming face
   * @param data    Data packet
   */
  virtual void
  OnData (Ptr<Face> face,
          Ptr<Data> data);

  /**
   * @brief An event that is fired every time a new PIT entry is created
   *
   * Note that if NDN node is receiving a similar interest (interest for the same name),
   * then either DidReceiveDuplicateInterest, DidSuppressSimilarInterest, or DidForwardSimilarInterest
   * will be called
   *
   * Suppression of similar Interests is controlled using ShouldSuppressIncomingInterest virtual method
   *
   * @param inFace  incoming face
   * @param header  deserialized Interest header
   * @param pitEntry created PIT entry (incoming and outgoing face sets are empty)
   *
   * @see DidReceiveDuplicateInterest, DidSuppressSimilarInterest, DidForwardSimilarInterest, ShouldSuppressIncomingInterest
   */
  void
  DidCreatePitEntry (Ptr<Face> inFace,
                     Ptr<const Interest> interest,
                     Ptr<pit::Entry> pitEntry);

  /**
   * @brief An event that is fired every time when a similar Interest is received and suppressed (collapsed)
   *
   * This even is the last action that is performed before the Interest processing is halted
   *
   * @param inFace  incoming face
   * @param interest Interest packet
   * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
   *
   * @see DidReceiveDuplicateInterest, DidForwardSimilarInterest, ShouldSuppressIncomingInterest
   */
  void
  DidSuppressSimilarInterest (Ptr<Face> inFace,
                              Ptr<const Interest> interest,
                              Ptr<pit::Entry> pitEntry);

  /**
   * @brief An event that is fired every time when a similar Interest is received and further forwarded (not suppressed/collapsed)
   *
   * This even is fired just before handling the Interest to PropagateInterest method
   *
   * @param inFace  incoming face
   * @param interest Interest packet
   * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
   *
   * @see DidReceiveDuplicateInterest, DidSuppressSimilarInterest, ShouldSuppressIncomingInterest
   */
  virtual void
  DidForwardSimilarInterest (Ptr<Face> inFace,
                             Ptr<const Interest> interest,
                             Ptr<pit::Entry> pitEntry);

  /**
   * @brief Actual procedure to satisfy Interest
   *
   * Note that when Interest is satisfied from the cache, incoming face will be 0
   *
   * @param inFace  incoming face
   * @param data    Data packet
   * @param pitEntry an existing PIT entry, corresponding to the duplicated Interest
   */
  virtual void
  SatisfyPendingInterest (Ptr<Face> inFace, // 0 allowed (from cache)
                          Ptr<const Data> data,
                          Ptr<pit::Entry> pitEntry);

  /**
   * @brief Wrapper method, which performs general tasks and calls DoPropagateInterest method
   *
   * General tasks so far are adding face to the list of incoming face, updating
   * PIT entry lifetime, calling DoPropagateInterest, and retransmissions (enabled by default).
   *
   * @param inFace     incoming face
   * @param interest   Interest packet
   * @param pitEntry   reference to PIT entry (reference to corresponding FIB entry inside)
   *
   * @see DoPropagateInterest
   */
  virtual void
  PropagateInterest (Ptr<Face> inFace,
                     Ptr<const Interest> interest,
                     Ptr<pit::Entry> pitEntry);

  /**
   * @brief Method to check whether Interest can be send out on the particular face or not
   *
   * In the base class, this method perfoms two checks:
   * 1. If inFace is equal to outFace (when equal, Interest forwarding is prohibited)
   * 2. Whether Interest should be suppressed (list of outgoing faces include outFace),
   * considering (if enabled) retransmission logic
   *
   * @param inFace     incoming face of the Interest
   * @param outFace    proposed outgoing face of the Interest
   * @param interest   Interest packet
   * @param pitEntry   reference to PIT entry (reference to corresponding FIB entry inside)
   *
   * @see DetectRetransmittedInterest
   */
  virtual bool
  CanSendOutInterest (Ptr<Face> inFace,
                      Ptr<Face> outFace,
                      Ptr<const Interest> interest,
                      Ptr<pit::Entry> pitEntry);


  /**
   * @brief Event which is fired every time an unsolicited DATA packet (no active PIT entry) is received
   *
   * The current implementation allows ignoring unsolicited DATA (by default), or cache it by setting
   * attribute CacheUnsolicitedData true
   *
   * @param inFace  incoming face
   * @param data    Data packet
   * @param didCreateCacheEntry flag indicating whether a cache entry was added for this data packet or not (e.g., packet already exists in cache)
   */
  virtual void
  DidReceiveUnsolicitedData (Ptr<Face> inFace,
                             Ptr<const Data> data,
                             bool didCreateCacheEntry);



protected:
  // inherited from  Nacks/ForwardingStrategy
  virtual bool
  DoPropagateInterest (Ptr<Face> inFace,
                       Ptr<const Interest> interest,
                       Ptr<pit::Entry> pitEntry);

  std::vector<mehdi::Location>
  GetNodeLocations();

  void
  SelectForwarder(Ptr<Face> inFace,
          Ptr<Interest> interest,
          Ptr<pit::Entry> pitEntry);

  void
  OnInterestOtherSimulation(Ptr<Face> inFace,
		  Ptr<Interest> interest);

protected:
  static LogComponent g_log;
}; //class MehdiForwardingBase

} // namespace mehdi
} // namespace ndn
} // namespace ns3




#endif /* NDN_MEHDI_FORWARDING_BASE_H_ */
