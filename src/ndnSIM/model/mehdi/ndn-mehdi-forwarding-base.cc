/*
 * ndn-mehdi-forwarding-base.cc
 *
 *  Created on: April 24, 2016
 *      Author: mehdi
 */

#include "ndn-mehdi-forwarding-base.h"
//mehdi adding begin
#include "src/ndnSIM/mehdi/ndn-mehdi-location.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-node.h"
#include "ns3/node-container.h"
#include "ns3/node-list.h"
#include "ns3/random-variable.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-tools.h"
#include <sstream>
//mehdi adding begin

#include "ns3/ndn-data.h"
#include "ns3/ndn-fib.h"
#include "ns3/ndn-content-store.h"
#include "ns3/ndn-face.h"
#include "ns3/ptr.h"
#include "ns3/string.h"
#include "ns3/ndnSIM/utils/ndn-fw-hop-count-tag.h"
#include "ns3/ndn-interest.h"
#include "ns3/ndn-pit.h"
#include "ns3/ndn-pit-entry.h"
#include "ns3/assert.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/boolean.h"
#include <boost/ref.hpp>
#include <boost/foreach.hpp>
#include <boost/lambda/lambda.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/tuple/tuple.hpp>
namespace ll = boost::lambda;


namespace ns3 {
namespace ndn {
namespace mehdi {

NS_OBJECT_ENSURE_REGISTERED (MehdiForwardingBase);

LogComponent MehdiForwardingBase::g_log = LogComponent (MehdiForwardingBase::GetLogName ().c_str ());

std::string
MehdiForwardingBase::GetLogName ()
{
  return super::GetLogName ()+".MehdiForwardingBase";
}

TypeId MehdiForwardingBase::GetTypeId ()
{
  static TypeId tid = TypeId ("ns3::ndn::mehdi::MehdiForwardingBase")
    .SetGroupName ("Ndn")
    .SetParent <Nacks> ()
    .AddConstructor <MehdiForwardingBase> ()
    ;
  return tid;
}

MehdiForwardingBase::MehdiForwardingBase ()
{
}

bool
MehdiForwardingBase::DoPropagateInterest (Ptr<Face> inFace,
                               Ptr<const Interest> interest,
                               Ptr<pit::Entry> pitEntry)
{
  NS_LOG_FUNCTION (this);

  int propagatedCount = 0;

  BOOST_FOREACH (const fib::FaceMetric &metricFace, pitEntry->GetFibEntry ()->m_faces.get<fib::i_metric> ())
    {
      NS_LOG_DEBUG ("Trying " << boost::cref(metricFace));
      if (metricFace.GetStatus () == fib::FaceMetric::NDN_FIB_RED) // all non-read faces are in the front of the list
        break;

      if (!TrySendOutInterest (inFace, metricFace.GetFace (), interest, pitEntry))
        {
          continue;
        }

      propagatedCount++;
    }

  NS_LOG_INFO ("Propagated to " << propagatedCount << " faces");
  return propagatedCount > 0;
}

void
MehdiForwardingBase::OnInterest (Ptr<Face> inFace,
                                Ptr<Interest> interest)
{
	//	INT
	//		->do we have data?
	//1++			->response with data
	//		->no data?
	//2++			->add self position and distance to the sender
	//3++			->reset INT, set ACK, change Nonce
	//4++			->schedule responding time to 1/distance
	//
	//	ACK
	//		->no pit entry
	//5++			->return
	//		->data available
	//6++			->return
	//		->no data?
	//7++			->find the quarter and reset waiting for ack if exists
	//					->no waiting for ack in that quarter
	//8++					->return
	//9++			->set the selected sender
	//10++			->reset ACK, set CMD, change Nonce
	//11++			->forward interest
	//
	//	CMD
	//		->not for us
	//12++			->return
	//		->for us
	//			->do we have data
	//13++				->response with data
	//			->no data?
	//14++				->set INT and reset CMD, change Nonce
	//15++				->set waiting for ACK in pit entry
	//16++				->forward interest
	// 	new interest from upper layer
	//		->do we have data?
	//17++			->response with data
	//		->no data?
	//18++			->set INT
	//19++			->set waiting for ack in pit
	//20++			->change nonce
	//21++			->forward interest

	//mehdi report begin
	//find the user friendly name of the sender
	uint id = interest->GetSender();
	Ptr<Node> sender_node = NULL;
	NodeContainer nc = 	NodeContainer::GetGlobal ();
	  for (NodeList::Iterator i = NodeList::Begin (); i != NodeList::End (); ++i)
	    {
		  sender_node = *i;
	      if (sender_node->GetId() == id)
	    	  break;
	    }
	  if (sender_node == NULL)
		  MEHDI_ERROR_LINE("cannot find sender of interest from its id.");
	  else
	  {
		  Report::ReportPacketHappening("-->>INTEREST",
				  sender_node->GetObject<MehdiNode>()->GetUserFriendlyName(),
				  this->GetObject<ndn::mehdi::MehdiNode>()->GetUserFriendlyName(),
				  interest->GetName().toUri(),
				  Simulator::Now().GetSeconds());
	  }
	//mehdi report end

   //mehdi adding begin
	//+12+
	//if this is a CMD which is not for us just return
	if (Parameters::other_simulation && interest->GetCMD() && interest->GetCMD_SelectedSender() != this->GetObject<MehdiNode>()->GetId())
	{
		Report::n_dropped_cmd++;
		return;
	}
	//mehdi adding end

  NS_LOG_FUNCTION (inFace << interest->GetName ());
  m_inInterests (interest, inFace);

  Ptr<pit::Entry> pitEntry = m_pit->Lookup (*interest);
  bool similarInterest = true;
  //---if this is a new interest, a new pit entry should be made for it
  if (pitEntry == 0)
    {
	  //mehdi adding begin
	  //+5+
	  //if this is an ACK for which we are not waiting just return
	  if (Parameters::other_simulation && interest->GetACK())
	  {
		  Report::n_dropped_ack++;
		  return;
	  }
	  //mehdi adding end

	  similarInterest = false;
      pitEntry = m_pit->Create (interest);
      if (pitEntry != 0)
        {
          DidCreatePitEntry (inFace, interest, pitEntry);
        }
      else
        {
          FailedToCreatePitEntry (inFace, interest);
          return;
        }
    }

  bool isDuplicated = true;
  //---add the nonce of this interest to the nonce collection of the entry (if not already in it)
  if (!pitEntry->IsNonceSeen (interest->GetNonce ()))
    {
      pitEntry->AddSeenNonce (interest->GetNonce ());
      isDuplicated = false;

      //mehdi adding begin
      //if the Nonce is changed, it means that this is not a similar interest (of course only in other simulation)
      if (Parameters::other_simulation && (interest->GetACK() || interest->GetINT() || interest->GetCMD()))
    		  similarInterest = false;
      //mehdi adding end

      //update location chains of the entry if this is not a new entry
//      if (similarInterest)
//    	  pitEntry->UpdateLocatoinChainContainer(*interest->GetLocationChainContainer());
    }

  //---if the nonce is already seen, then this is a duplicate interest packet, return
  if (isDuplicated)
    {
      DidReceiveDuplicateInterest (inFace, interest, pitEntry);
      return;
    }

  //---check for cache data and satisfy the interest if data is found and return, otherwise just continue
  Ptr<Data> contentObject;
  contentObject = m_contentStore->Lookup (interest);
  if (contentObject != 0)
    {
	  //mehdi adding comment begin
	  //+1+
	  //+13+
	  //+17+
	  //mehdi adding comment end
	  //mehdi adding begin
	  //+6+
	  //if we have received an ACK for an interest whose data we already have, just return
	  if (Parameters::other_simulation && interest->GetACK())
	  {
		  Report::n_dropped_ack++;
		  return;
	  }
	  //mehdi adding end

	  FwHopCountTag hopCountTag;
      if (interest->GetPayload ()->PeekPacketTag (hopCountTag))
        {
          contentObject->GetPayload ()->AddPacketTag (hopCountTag);
        }

      pitEntry->AddIncoming (inFace/*, Seconds (1.0)*/);

      // Do data plane performance measurements
      WillSatisfyPendingInterest (0, pitEntry);

      //mehdi adding (add meta data to data packet from cache) begin
      //update the current and next location of data
      contentObject->SetSender(this->GetObject<MehdiNode>()->GetId());
	  contentObject->AddLocations(GetNodeLocations());
      //mehdi adding (add meta data to data packet from cache) end

      // Actually satisfy pending interest
      SatisfyPendingInterest (0, contentObject, pitEntry);

      //mehdi report begin
      Report::ReportPacketHappening("****INTEREST",
    		  "CACHE",
			  this->GetObject<MehdiNode>()->GetUserFriendlyName(),
			  interest->GetName().toUri(),
			  Simulator::Now().GetSeconds());
      //mehdi report end

      return;
    }

  //---not a duplicate interest and no data found in cache
  //---if the interest is similar, just check if we should suppress it (possibly not used in mehdi forwarding)
  if (similarInterest && ShouldSuppressIncomingInterest (inFace, interest, pitEntry))
    {
      pitEntry->AddIncoming (inFace/*, interest->GetInterestLifetime ()*/);
      // update PIT entry lifetime
      pitEntry->UpdateLifetime (interest->GetInterestLifetime ());

      // Suppress this interest if we're still expecting data from some other face
      NS_LOG_DEBUG ("Suppress interests");
      m_dropInterests (interest, inFace);

      DidSuppressSimilarInterest (inFace, interest, pitEntry);
      return;
    }

  if (similarInterest)
    {
      DidForwardSimilarInterest (inFace, interest, pitEntry);
    }

  //mehdi adding (add meta data to forwarding interest) begin
  interest->SetSender(this->GetObject<MehdiNode>()->GetId());
  //mehdi adding (add meta data to forwarding interest) end

  //this is a valid interest for which we have not had data
  //see what we should do with it
	if (Parameters::other_simulation)
	{
		if (interest->GetINT())
		{
			//calculate distance to the sender
			coordinate my_x, my_y;
			if (Parameters::exact_location)
			{
				my_x = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().x);
				my_y = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().y);
			}
			else
			{
				Location current_location = this->GetObject<MehdiNode>()->GetCurrentLocation(Simulator::Now().GetSeconds());
				my_x = current_location.GetX();
				my_y = current_location.GetY();
			}
			coordinate d_x = interest->GetX() - my_x;
			coordinate d_y = interest->GetY() - my_y;
			double distance = sqrt(pow(d_x, 2) + pow(d_y, 2));
			if (my_x == INVALID_COORDINATE || my_y == INVALID_COORDINATE || interest->GetX() == INVALID_COORDINATE || interest->GetY() == INVALID_COORDINATE)
				distance = INVALID_COORDINATE;
			//add self position to the packet
			//+2+
			interest->SetX(my_x);
			interest->SetY(my_y);
			//for delayed forwarding
			interest->SetDistance(distance);
			//unset INT and set ACK
			//+3+
			interest->ResetAllOtherSimulationTags();
			interest->SetACK(1);
			UniformVariable rand (0,std::numeric_limits<uint32_t>::max ());
			interest->SetNonce(rand.GetValue());
			//+4+
			double delay = 0.003 * (1 - distance / 1000);
			if (delay < 0)
				delay = 0;
			Simulator::Schedule(Seconds(delay), &MehdiForwardingBase::PropagateInterest, this, inFace, interest, pitEntry);
		}
		else if (interest->GetACK())
		{
			//id of this node
			int id = this->GetObject<MehdiNode>()->GetId();
			//find the quarter
			coordinate my_x = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().x);
			coordinate my_y = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().y);
			coordinate d_x = interest->GetX() - my_x;
			coordinate d_y = interest->GetY() - my_y;
			double distance = sqrt(pow(d_x, 2) + pow(d_y, 2));
			if (my_x == INVALID_COORDINATE || my_y == INVALID_COORDINATE || interest->GetX() == INVALID_COORDINATE || interest->GetY() == INVALID_COORDINATE)
				distance = INVALID_COORDINATE;
			//+7+
			if (d_x >= 0 && d_y >= 0)
				pitEntry->distances[0].push_back(pair<double, int>(distance, id));
			if (d_x < 0 && d_y >= 0)
				pitEntry->distances[1].push_back(pair<double, int>(distance, id));
			if (d_x < 0 && d_y < 0)
				pitEntry->distances[2].push_back(pair<double, int>(distance, id));
			if (d_x >= 0 && d_y < 0)
				pitEntry->distances[3].push_back(pair<double, int>(distance, id));
		}
		else //this is either a CMD interest for this node, or a new interest from the upper layer
		{
			coordinate my_x, my_y;
			if (Parameters::exact_location)
			{
				my_x = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().x);
				my_y = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().y);
			}
			else
			{
				Location current_location = this->GetObject<MehdiNode>()->GetCurrentLocation(Simulator::Now().GetSeconds());
				my_x = current_location.GetX();
				my_y = current_location.GetY();
			}
			interest->SetX(my_x);
			interest->SetY(my_y);
			//+14+
			interest->ResetAllOtherSimulationTags();
			interest->SetINT(1);
			UniformVariable rand (0,std::numeric_limits<uint32_t>::max ());
			interest->SetNonce(rand.GetValue());
			//+15+
			for (uint i = 0; i < 4; i++)
				pitEntry->distances[i].clear();
			//+16+
			PropagateInterest(inFace, interest, pitEntry);
			Simulator::Schedule(Seconds(0.01), &MehdiForwardingBase::SelectForwarder, this, inFace, interest, pitEntry);
		}
	}
	//not other simulation
	else
		//---propagate the interest
		PropagateInterest (inFace, interest, pitEntry);
}


void
MehdiForwardingBase::OnInterestOtherSimulation (Ptr<Face> inFace,
                                Ptr<Interest> interest)
{
	//	INT
	//		->do we have data?
	//1++			->response with data
	//		->no data?
	//2++			->add self position and distance to the sender
	//3++			->reset INT, set ACK, change Nonce
	//4++			->schedule responding time to 1/distance
	//
	//	ACK
	//		->no pit entry
	//5++			->return
	//		->data available
	//6++			->return
	//		->no data?
	//7++			->find the quarter and reset waiting for ack if exists
	//					->no waiting for ack in that quarter
	//8++					->return
	//9++			->set the selected sender
	//10++			->reset ACK, set CMD, change Nonce
	//11++			->forward interest
	//
	//	CMD
	//		->not for us
	//12++			->return
	//		->for us
	//			->do we have data
	//13++				->response with data
	//			->no data?
	//14++				->set INT and reset CMD, change Nonce
	//15++				->set waiting for ACK in pit entry
	//16++				->forward interest
	// 	new interest from upper layer
	//		->do we have data?
	//17++			->response with data
	//		->no data?
	//18++			->set INT
	//19++			->set waiting for ack in pit
	//20++			->change nonce
	//21++			->forward interest

	//mehdi report begin
	//find the user friendly name of the sender
	uint id = interest->GetSender();
	Ptr<Node> sender_node = NULL;
	NodeContainer nc = 	NodeContainer::GetGlobal ();
	  for (NodeList::Iterator i = NodeList::Begin (); i != NodeList::End (); ++i)
	    {
		  sender_node = *i;
	      if (sender_node->GetId() == id)
	    	  break;
	    }
	  if (sender_node == NULL)
		  MEHDI_ERROR_LINE("cannot find sender of interest from its id.");
	  else
	  {
		  Report::ReportPacketHappening("-->>INTEREST",
				  sender_node->GetObject<MehdiNode>()->GetUserFriendlyName(),
				  this->GetObject<ndn::mehdi::MehdiNode>()->GetUserFriendlyName(),
				  interest->GetName().toUri(),
				  Simulator::Now().GetSeconds());
	  }
	//mehdi report end

   //mehdi adding begin
	//+12+
	//if this is a CMD which is not for us just return
	if (Parameters::other_simulation && interest->GetCMD() && interest->GetCMD_SelectedSender() != this->GetObject<MehdiNode>()->GetId())
		return;
	//mehdi adding end

  NS_LOG_FUNCTION (inFace << interest->GetName ());
  m_inInterests (interest, inFace);

  Ptr<pit::Entry> pitEntry = m_pit->Lookup (*interest);
  bool similarInterest = true;
  //---if this is a new interest, a new pit entry should be made for it
  if (pitEntry == 0)
    {
	  //mehdi adding begin
	  //+5+
	  //if this is an ACK for which we are not waiting just return
	  if (Parameters::other_simulation && interest->GetACK())
		  return;
	  //mehdi adding end

	  similarInterest = false;
      pitEntry = m_pit->Create (interest);
      if (pitEntry != 0)
        {
          DidCreatePitEntry (inFace, interest, pitEntry);
        }
      else
        {
          FailedToCreatePitEntry (inFace, interest);
          return;
        }
    }

  bool isDuplicated = true;
  //---add the nonce of this interest to the nonce collection of the entry (if not already in it)
  if (!pitEntry->IsNonceSeen (interest->GetNonce ()))
    {
      pitEntry->AddSeenNonce (interest->GetNonce ());
      isDuplicated = false;

      //mehdi adding begin
      //if the Nonce is changed, it means that this is not a similar interest (of course only in other simulation)
      if (Parameters::other_simulation && (interest->GetACK() || interest->GetINT() || interest->GetCMD()))
    		  similarInterest = false;
      //mehdi adding end

      //update location chains of the entry if this is not a new entry
//      if (similarInterest)
//    	  pitEntry->UpdateLocatoinChainContainer(*interest->GetLocationChainContainer());
    }

  //---if the nonce is already seen, then this is a duplicate interest packet, return
  if (isDuplicated)
    {
      DidReceiveDuplicateInterest (inFace, interest, pitEntry);
      return;
    }

  //---check for cache data and satisfy the interest if data is found and return, otherwise just continue
  Ptr<Data> contentObject;
  contentObject = m_contentStore->Lookup (interest);
  if (contentObject != 0)
    {
	  //mehdi adding comment begin
	  //+1+
	  //+13+
	  //+17+
	  //mehdi adding comment end
	  //mehdi adding begin
	  //+6+
	  //if we have received an ACK for an interest whose data we already have, just return
	  if (Parameters::other_simulation && interest->GetACK())
		  return;
	  //mehdi adding end

	  FwHopCountTag hopCountTag;
      if (interest->GetPayload ()->PeekPacketTag (hopCountTag))
        {
          contentObject->GetPayload ()->AddPacketTag (hopCountTag);
        }

      pitEntry->AddIncoming (inFace/*, Seconds (1.0)*/);

      // Do data plane performance measurements
      WillSatisfyPendingInterest (0, pitEntry);

      //mehdi adding (add meta data to data packet from cache) begin
      //update the current and next location of data
      contentObject->SetSender(this->GetObject<MehdiNode>()->GetId());
	  contentObject->AddLocations(GetNodeLocations());
      //mehdi adding (add meta data to data packet from cache) end

      // Actually satisfy pending interest
      SatisfyPendingInterest (0, contentObject, pitEntry);

      //mehdi report begin
      Report::ReportPacketHappening("****INTEREST",
    		  "CACHE",
			  this->GetObject<MehdiNode>()->GetUserFriendlyName(),
			  interest->GetName().toUri(),
			  Simulator::Now().GetSeconds());
      //mehdi report end

      return;
    }

  //---not a duplicate interest and no data found in cache
  //---if the interest is similar, just check if we should suppress it (possibly not used in mehdi forwarding)
  if (similarInterest && ShouldSuppressIncomingInterest (inFace, interest, pitEntry))
    {
      pitEntry->AddIncoming (inFace/*, interest->GetInterestLifetime ()*/);
      // update PIT entry lifetime
      pitEntry->UpdateLifetime (interest->GetInterestLifetime ());

      // Suppress this interest if we're still expecting data from some other face
      NS_LOG_DEBUG ("Suppress interests");
      m_dropInterests (interest, inFace);

      DidSuppressSimilarInterest (inFace, interest, pitEntry);
      return;
    }

  if (similarInterest)
    {
      DidForwardSimilarInterest (inFace, interest, pitEntry);
    }

  //mehdi adding (add meta data to forwarding interest) begin
  interest->SetSender(this->GetObject<MehdiNode>()->GetId());
  //mehdi adding (add meta data to forwarding interest) end

  //this is a valid interest for which we have not had data
  //see what we should do with it
	if (Parameters::other_simulation)
	{
		if (interest->GetINT())
		{
			//calculate distance to the sender
			coordinate my_x = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().x);
			coordinate my_y = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().y);
			coordinate d_x = interest->GetX() - my_x;
			coordinate d_y = interest->GetY() - my_y;
			double distance = sqrt(pow(d_x, 2) + pow(d_y, 2));
			//add self position to the packet
			//+2+
			interest->SetX(my_x);
			interest->SetY(my_y);
			//for delayed forwarding
			interest->SetDistance(distance);
			//unset INT and set ACK
			//+3+
			interest->ResetAllOtherSimulationTags();
			interest->SetACK(1);
			UniformVariable rand (0,std::numeric_limits<uint32_t>::max ());
			interest->SetNonce(rand.GetValue());
			//+4+
			DoPropagateInterest(inFace, interest, pitEntry);
		}
		else if (interest->GetACK())
		{
			//id of this node
			int id = this->GetObject<MehdiNode>()->GetId();
			//find the quarter
			coordinate my_x = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().x);
			coordinate my_y = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().y);
			coordinate d_x = interest->GetX() - my_x;
			coordinate d_y = interest->GetY() - my_y;
			double distance = sqrt(pow(d_x, 2) + pow(d_y, 2));
			//+7+
			if (d_x > 0 && d_y > 0)
				pitEntry->distances[0].push_back(pair<double, int>(distance, id));
			if (d_x < 0 && d_y > 0)
				pitEntry->distances[1].push_back(pair<double, int>(distance, id));
			if (d_x < 0 && d_y < 0)
				pitEntry->distances[2].push_back(pair<double, int>(distance, id));
			if (d_x > 0 && d_y < 0)
				pitEntry->distances[3].push_back(pair<double, int>(distance, id));
		}
		else //this is either a CMD interest for this node, or a new interest from the upper layer
		{
			//+14+
			interest->ResetAllOtherSimulationTags();
			interest->SetINT(1);
			UniformVariable rand (0,std::numeric_limits<uint32_t>::max ());
			interest->SetNonce(rand.GetValue());
			//+15+
			for (uint i = 0; i < 4; i++)
				pitEntry->distances[i].clear();
			//+16+
			DoPropagateInterest(inFace, interest, pitEntry);
			Simulator::Schedule(Seconds(0.01), &MehdiForwardingBase::SelectForwarder, this, inFace, interest, pitEntry);
		}
	}
	//not other simulation
	else
		//---propagate the interest
		PropagateInterest (inFace, interest, pitEntry);
}


void
MehdiForwardingBase::OnData (Ptr<Face> inFace,
                            Ptr<Data> data)
{
	//mehdi report begin
	uint id = data->GetSender();
	Ptr<Node> sender_node = NULL;
	NodeContainer nc = 	NodeContainer::GetGlobal ();
	  for (NodeList::Iterator i = NodeList::Begin (); i != NodeList::End (); ++i)
	    {
		  sender_node = *i;
	      if (sender_node->GetId() == id)
	    	  break;
	    }
	  Report::ReportPacketHappening("-->>DATA",
			  sender_node->GetObject<MehdiNode>()->GetUserFriendlyName(),
			  this->GetObject<MehdiNode>()->GetUserFriendlyName(),
			  data->GetName().toUri(),
			  Simulator::Now().GetSeconds());
	//mehdi report end

  NS_LOG_FUNCTION (inFace << data->GetName ());
  m_inData (data, inFace);

  // Lookup PIT entry
  Ptr<pit::Entry> pitEntry = m_pit->Lookup (*data);
  if (pitEntry == 0)
    {
	  Ptr<fib::Entry> fibEntry = m_fib->LongestPrefixMatch(data->GetName());
	  if (fibEntry != 0)
		  fibEntry->UpdateLocatoinContainer(*(data->GetLocationContainer()), true);

      bool cached = false;

      if (m_cacheUnsolicitedData || (m_cacheUnsolicitedDataFromApps && (inFace->GetFlags () & Face::APPLICATION)))
        {
          // Optimistically add or update entry in the content store
          cached = m_contentStore->Add (data);
        }
      else
        {
          // Drop data packet if PIT entry is not found
          // (unsolicited data packets should not "poison" content store)

          //drop dulicated or not requested data packet
          m_dropData (data, inFace);
        }

      DidReceiveUnsolicitedData (inFace, data, cached);
      return;
    }
  else
    {
	  Ptr<fib::Entry> fibEntry = pitEntry->GetFibEntry();
	  if (fibEntry != 0)
		  fibEntry->UpdateLocatoinContainer(*(data->GetLocationContainer()), true);
      bool cached = m_contentStore->Add (data);
      DidReceiveSolicitedData (inFace, data, cached);
    }

  //mehdi adding (add meta data to data packet being forwarded) begin
  data->SetSender(this->GetObject<MehdiNode>()->GetId());
  data->AddLocations(GetNodeLocations());
  //mehdi adding (add meta data to data packet being forwarded) end

  while (pitEntry != 0)
    {
      // Do data plane performance measurements
      WillSatisfyPendingInterest (inFace, pitEntry);

      // Actually satisfy pending interest
      SatisfyPendingInterest (inFace, data, pitEntry);

      // Lookup another PIT entry
      pitEntry = m_pit->Lookup (*data);
    }
}

/*
 *
 */
void
MehdiForwardingBase::DidCreatePitEntry (Ptr<Face> inFace,
                                       Ptr<const Interest> interest,
                                       Ptr<pit::Entry> pitEntry)
{
	ForwardingStrategy::DidCreatePitEntry(inFace, interest, pitEntry);
//	pitEntry->UpdateLocatoinChainContainer(*(interest->GetLocationChainContainer()));
}

/*
 *
 */
void
MehdiForwardingBase::DidSuppressSimilarInterest (Ptr<Face> face,
                                                Ptr<const Interest> interest,
                                                Ptr<pit::Entry> pitEntry)
{
	ForwardingStrategy::DidSuppressSimilarInterest(face, interest, pitEntry);
//	pitEntry->UpdateLocatoinChainContainer(*(interest->GetLocationChainContainer()));
}

void
MehdiForwardingBase::DidForwardSimilarInterest (Ptr<Face> inFace,
                                               Ptr<const Interest> interest,
                                               Ptr<pit::Entry> pitEntry)
{
//	pitEntry->UpdateLocatoinChainContainer(*(interest->GetLocationChainContainer()));
}

void
MehdiForwardingBase::SatisfyPendingInterest (Ptr<Face> inFace,
                                            Ptr<const Data> data,
                                            Ptr<pit::Entry> pitEntry)
{

//mehdi commenting (enabling wireless routing of data) begin
//this part in commented out for enabling wireless routing of data packet
//  if (inFace != 0)
//    pitEntry->RemoveIncoming (inFace);
//mehdi commenting (enabling wireless routing of data) end

  //mehdi adding begin
//	if (pitEntry->GetInterest()->GetOriginalSender() == this->GetObject<MehdiNode>()->GetId())
//		Report::n_successful_requests++;
  //mehdi adding end

  //satisfy all pending incoming Interests
  BOOST_FOREACH (const pit::IncomingFace &incoming, pitEntry->GetIncoming ())
  {
	//mehdi mixed begin
	//try to stop propagating data in the requester node
	if (pitEntry->GetInterest()->GetOriginalSender() != this->GetObject<MehdiNode>()->GetId())
    {
      bool ok = incoming.m_face->SendData (data);

      DidSendOutData (inFace, incoming.m_face, data, pitEntry);
      NS_LOG_DEBUG ("Satisfy " << *incoming.m_face);
      //mehdi report begin
	  if (ok)
	  {
		  Report::ReportPacketHappening("<<--DATA",
				  this->GetObject<MehdiNode>()->GetUserFriendlyName(),
				  "PROPAGATE",
				  data->GetName().toUri(),
				  Simulator::Now().GetSeconds());

		  Report::n_data_packets++;
	  }
	  //mehdi report end
      if (!ok)
        {
          m_dropData (data, incoming.m_face);
          NS_LOG_DEBUG ("Cannot satisfy data to " << *incoming.m_face);
        }
    }

  }
  //mehdi mixed end

  // All incoming interests are satisfied. Remove them
  pitEntry->ClearIncoming ();

  // Remove all outgoing faces
  pitEntry->ClearOutgoing ();

  // Set pruning timout on PIT entry (instead of deleting the record)
  m_pit->MarkErased (pitEntry);
}

void
MehdiForwardingBase::PropagateInterest (Ptr<Face> inFace,
                                       Ptr<const Interest> interest,
                                       Ptr<pit::Entry> pitEntry)
{
	if (Parameters::smart_propagation && !Parameters::other_simulation)
	{
		elapsed_time now = Simulator::Now().GetSeconds();
		Ptr<fib::Entry> fibEntry = pitEntry->GetFibEntry();
		elapsed_time succ = fibEntry->GetLastSuccTrans();
		elapsed_time unsucc = fibEntry->GetLastUnsuccTrans();

		//if we have a successful fib entry, just continue
		if (succ + mehdi_max_time_for_accepting_succ_fib > now)
			noop;
		else if (!interest->GetForceForwarding())
		{
			//if we have an unsuccessful fib entry which is not reactivated yet
			if (unsucc > succ && unsucc + mehdi_min_time_before_reactivating_unsucc_fib > now)
				//just return
				return;

			pitEntry->GetFibEntry()->UpdateLocatoinContainer();
			loc_container container = pitEntry->GetFibEntry()->GetLocationContainer();

			//check the locations
			if (container.size() > 0)
			{
				bool is_reachable = false;
				coordinate x = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().x);
				coordinate y = (coordinate)(this->GetObject<MobilityModel>()->GetPosition().y);

				for (loc_iterator it = container.begin(); it != container.end(); it++)
					if (it->IsReachable(x, y, mehdi_reachability_limit, now))
					{
						is_reachable = true;
						break;
					}

				//not reachable and not a force forwarding packet
				if (!is_reachable)
					return;
			}
			//no location is saved so just return
			else
				return;
		}
	}

  bool isRetransmitted = m_detectRetransmissions && // a small guard
                         DetectRetransmittedInterest (inFace, interest, pitEntry);

  pitEntry->AddIncoming (inFace/*, interest->GetInterestLifetime ()*/);
  /// @todo Make lifetime per incoming interface
  pitEntry->UpdateLifetime (interest->GetInterestLifetime ());

  bool propagated = DoPropagateInterest (inFace, interest, pitEntry);

  if (!propagated && isRetransmitted) //give another chance if retransmitted
    {
      // increase max number of allowed retransmissions
      pitEntry->IncreaseAllowedRetxCount ();

      // try again
      propagated = DoPropagateInterest (inFace, interest, pitEntry);
    }

  // if (!propagated)
  //   {
  //     NS_LOG_DEBUG ("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
  //     NS_LOG_DEBUG ("+++ Not propagated ["<< interest->GetName () <<"], but number of outgoing faces: " << pitEntry->GetOutgoing ().size ());
  //     NS_LOG_DEBUG ("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
  //   }

  // ForwardingStrategy will try its best to forward packet to at least one interface.
  // If no interests was propagated, then there is not other option for forwarding or
  // ForwardingStrategy failed to find it.
  if (!propagated && pitEntry->AreAllOutgoingInVain ())
    {
      DidExhaustForwardingOptions (inFace, interest, pitEntry);
    }
	  //mehdi adding begin
  	  if (propagated)
  	  {
  		  pitEntry->GetFibEntry()->SetLastUnsuccTrans(Simulator::Now().GetSeconds());
  		  Report::ReportPacketHappening("<<--INTEREST",
  				  this->GetObject<MehdiNode>()->GetUserFriendlyName(),
				  "PROPAGATE",
				  interest->GetName().toUri(),
				  Simulator::Now().GetSeconds());
  	  }
  	  //mehdi adding end
}

bool
MehdiForwardingBase::CanSendOutInterest (Ptr<Face> inFace,
                                        Ptr<Face> outFace,
                                        Ptr<const Interest> interest,
                                        Ptr<pit::Entry> pitEntry)
{
//mehdi commenting (enabling wireless routing of interest) begin
//this part is commented out for enabling wireless routing of interest packet
//  if (outFace == inFace)
//    {
//      // NS_LOG_DEBUG ("Same as incoming");
//      return false; // same face as incoming, don't forward
//    }
//mehdi commenting (enabling wireless routing of interest) end

  pit::Entry::out_iterator outgoing =
    pitEntry->GetOutgoing ().find (outFace);

  if (outgoing != pitEntry->GetOutgoing ().end ())
    {
      if (!m_detectRetransmissions)
        return false; // suppress
      else if (outgoing->m_retxCount >= pitEntry->GetMaxRetxCount ())
        {
          // NS_LOG_DEBUG ("Already forwarded before during this retransmission cycle (" <<outgoing->m_retxCount << " >= " << pitEntry->GetMaxRetxCount () << ")");
          return false; // already forwarded before during this retransmission cycle
        }
   }

  return true;
}

void
MehdiForwardingBase::DidReceiveUnsolicitedData (Ptr<Face> inFace,
                                               Ptr<const Data> data,
                                               bool didCreateCacheEntry)
{
//	Ptr<fib::Entry> fibEntry = m_fib->Find(data->GetName());
//	if (fibEntry != 0)
//		fibEntry->UpdateLocatoinContainer(*data->GetLocationContainer());
}

void
MehdiForwardingBase::SelectForwarder(Ptr<Face> inFace,
        Ptr<Interest> interest,
        Ptr<pit::Entry> pitEntry)
{
	  for (int region = 0; region < 4; region++)
	  {
		  double max_distance = 0;
		  int selected_forwarder = -1;
		  for (uint index = 0; index < pitEntry->distances[region].size(); index++)
		  {
			  double distance = pitEntry->distances[region][index].first;
			  if (distance != INVALID_COORDINATE && distance > max_distance)
			  {
				  max_distance = distance;
				  selected_forwarder = pitEntry->distances[region][index].second;
			  }
		  }
		  if (selected_forwarder > -1 && pitEntry->distances[region].size() > 0)
			  selected_forwarder = pitEntry->distances[region][0].second;
		  if (selected_forwarder > -1)
		  {
			  interest->ResetAllOtherSimulationTags();
			  interest->SetCMD(1);
			  interest->SetCMD_SelectedSender(selected_forwarder);
			  UniformVariable rand (0,std::numeric_limits<uint32_t>::max ());
			  interest->SetNonce(rand.GetValue());
			  PropagateInterest(inFace, interest, pitEntry);
		  }
	  }
}

/*
 *
 */
std::vector<mehdi::Location>
MehdiForwardingBase::GetNodeLocations()
{
	//today location chain of the node
	std::vector<mehdi::Location> chain = this->GetObject<mehdi::MehdiNode>()->GetLocationChainForToday(Simulator::Now().GetSeconds()).GetChainVector();

//	//current location of the node
//	Vector current_position = this->GetObject<mehdi::MehdiNode>()->GetObject<MobilityModel>()->GetPosition();
//	Location current_location = Location(current_position.x, current_position.y, Simulator::Now().GetSeconds(), Simulator::Now().GetSeconds() + mehdi_validity_of_current_position);
//	chain.push_back(current_location);

	return chain;
}
} // namespace mehdi
} // namespace ndn
} // namespace ns3



