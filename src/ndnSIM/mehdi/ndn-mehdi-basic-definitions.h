/*
 * constants.h
 *
 *  Created on: Apr 29, 2016
 *      Author: mehdi
 */

#ifndef NDN_MEHDI_BASIC_DEFINITIONS_H_
#define NDN_MEHDI_BASIC_DEFINITIONS_H_

#include "stdint.h"
#include <string>
#include <vector>
#include <map>
#include <queue>

namespace ns3 {
namespace ndn {
namespace mehdi {

using namespace std;

typedef uint64_t elapsed_time;
typedef int64_t coordinate;
typedef uint64_t u_coordinate;

const coordinate INVALID_COORDINATE = INT64_MAX;
const coordinate MAX_COORDINATE = INT64_MAX -1;
const u_coordinate MAX_U_COORDINATE = UINT64_MAX - 1;
const coordinate INVALID_TIME = INT64_MAX;
const elapsed_time LAST_TIME = INT64_MAX - 1;
const elapsed_time FIRST_TIME = 0;
const string NOBODY = "nobody";

//the date 0 is the beginning of a Monday, and we want to start from a Monday 12 Jul. 2010
const elapsed_time mehdi_simulation_starting_time = 15 * 7 * 86400; //25 oct. 2010
//running the simulation for one week
const elapsed_time mehdi_simulation_duration = 7 * 86400; //31 oct. 2010, no holiday in between
const bool mehdi_compact_serializatoin = true; //uses uin32 instead of uint64 for locations

const elapsed_time mehdi_min_time_before_reactivating_unsucc_fib = 300;
const elapsed_time mehdi_max_time_for_accepting_succ_fib = 300;
const coordinate mehdi_reachability_limit = 500;
const elapsed_time mehdi_validity_time_of_predicted_next_location = 8000;

class Parameters
{
public:
	static bool other_simulation;
	static bool exact_location;
	static bool smart_propagation;
	static bool real_next_locations;

	static unsigned int n_contents;
	static string cache_size;
	static unsigned int max_non_smart_retransmit;
	static unsigned int max_smart_normal_retransmit;
	static unsigned int max_smart_force_forward_retransmit;
};

}//namespace mehdi
}//namespace ndn
}//namespace ns3

#endif /* NDN_MEHDI_BASIC_DEFINITIONS_H_ */
