/* 
 * Author: Mehdi Sepahkar
 */

#ifndef NDN_MEHDI_LOCATION_H
#define NDN_MEHDI_LOCATION_H

#include <stddef.h>
#include <stdint.h>
#include <cmath>
#include <cstdlib>
#include <map>
#include <set>
#include <vector>
#include "src/ndnSIM/mehdi/ndn-mehdi-position_interval_call.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-tools.h"
#include "ns3/buffer.h"

namespace ns3 {
namespace ndn {
namespace mehdi {

using namespace std;

//required for serializing and deserializing Location
//enum SerializedItem {X, Y, ENTERING_TIME, LEAVING_TIME};
//const SerializedItem location_64_bit_serialized_items[] = {X, Y, ENTERING_TIME, LEAVING_TIME};


//////////////////////////////
// Location class
//////////////////////////////

/*
 * a class for storing coordinates and validity times of a location.
 * it also provides some useful functions for checking reachability of the point.
 * besides it provides functions for serializing and deserializing the location in ns3.
 */
class Location
{
private:

	enum SerializedItem {FIRST_SERIALIZED_ITEM, X = FIRST_SERIALIZED_ITEM, Y, ENTERING_TIME, LEAVING_TIME, LAST_SERIALIZED_ITEM = LEAVING_TIME};
	coordinate m_x;
	coordinate m_y;
	elapsed_time m_entering_time; //in seconds (from start of simulation)
	elapsed_time m_leaving_time; //in seconds (from start of simulation)

public:
	//required for deserializing location
	Location()
	{
		m_x = INVALID_COORDINATE;
		m_y = INVALID_COORDINATE;
		m_entering_time = INVALID_TIME;
		m_leaving_time = INVALID_TIME;
	}
	Location(coordinate x, coordinate y, elapsed_time enteringTime, elapsed_time leavingTime)
	{
		m_x = x;
		m_y = y;
		m_entering_time = enteringTime;
		m_leaving_time = leavingTime;
	}

	/*
	 * serializing the location data using uint64_t elements
	 * (int64_t elements are silently converted to uint64_t elements)
	 */
	bool GetSerializedVector64(vector<uint64_t>& v) const
	{
		for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			switch(i)
			{
				case X:
					v.push_back(m_x);
					break;
				case Y:
					v.push_back(m_y);
					break;
				case ENTERING_TIME:
					v.push_back(m_entering_time);
					break;
				case LEAVING_TIME:
					v.push_back(m_leaving_time);
					break;
				default:
					MEHDI_ERROR_LINE("undefined type of element when serializing location.");
					return false;
			}
		return true;
	}

	/*
	 * deserializing the location with uint64_t elements.
	 * (int64_t elementes are silently converted from uint64_t elementes).
	 */
	bool SetWithSerializedVector64(std::vector<uint64_t> v)
	{
		if (v.size() < GetNumberOf_64_bit_SerializedItems())
		{
			MEHDI_ERROR_LINE("Location information vector does not have enough fields to be used for setting location");
			return false;
		}
		for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			switch(i)
			{
				case X:
					m_x = v[i];
					break;
				case Y:
					m_y = v[i];
					break;
				case ENTERING_TIME:
					m_entering_time = v[i];
					break;
				case LEAVING_TIME:
					m_leaving_time = v[i];
					break;
				default:
					MEHDI_ERROR_LINE("undefined field when deserializing location.");
					return false;
			}
		return true;
	}

	bool WriteToBinaryFile(ofstream& fout)
	{
		bool result = true;

		try
		{
			//write fields to file
			for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			{
				switch(i)
				{
					case X:
						if (!Tools::WriteDataToBinaryFile(m_x, fout))
							result = false;
						break;
					case Y:
						if (!Tools::WriteDataToBinaryFile(m_y, fout))
							result = false;
						break;
					case ENTERING_TIME:
						if (!Tools::WriteDataToBinaryFile(m_entering_time, fout))
							result = false;
						break;
					case LEAVING_TIME:
						if (!Tools::WriteDataToBinaryFile(m_leaving_time, fout))
							result = false;
						break;
					default:
						MEHDI_FATAL_ERROR("unrecognized serializing item in location.", -1);
						result = false;
				}
				//if problem writing to file do not continue for the next item
				if (result != true)
					break;
			}
		}
		catch(const exception& e)
		{
			MEHDI_ERROR_LINE("Error writing location data to the binary file : " << endl << e.what());
			result = false;
		}

		return result;
	}

	/*
	 * read the data of the node from binary file
	 */
	bool ReadFromBinaryFile(ifstream& fin)
	{
		//suppose success
		bool result = true;

		try
		{
			//read fields from file
			for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			{
				switch(i)
				{
					case X:
						if (!Tools::ReadDataFromBinaryFile(m_x, fin))
							result = false;
						break;
					case Y:
						if (!Tools::ReadDataFromBinaryFile(m_y, fin))
							result = false;
						break;
					case ENTERING_TIME:
						if (!Tools::ReadDataFromBinaryFile(m_entering_time, fin))
							result = false;
						break;
					case LEAVING_TIME:
						if (!Tools::ReadDataFromBinaryFile(m_leaving_time, fin))
							result = false;
						break;
					default:
						MEHDI_FATAL_ERROR("unrecognized serializing item in location.", -1);
						result = false;
				}
				//if any failure do not continue
				if (result != true)
					break;
			}

		}
		catch(const exception& e)
		{
			MEHDI_ERROR_LINE("Error reading location data from binary file: " << endl << e.what());
			result = false;
		}

		return result;
	}


	/*
	 * gets number of serialized items
	 */
	static size_t GetNumberOf_64_bit_SerializedItems()
	{
		return LAST_SERIALIZED_ITEM - FIRST_SERIALIZED_ITEM + 1;
	}

	/*
	 * gets number of serialized items
	 */
	static size_t GetNumberOf_32_bit_SerializedItems()
	{
		return LAST_SERIALIZED_ITEM - FIRST_SERIALIZED_ITEM + 1;
	}

	/*
	 * serializing the location data using uint32_t elements
	 * (int32_t elements are silently converted to uint32_t elements)
	 */
	bool GetSerializedVector32(vector<uint32_t>& v) const
	{
		for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			switch(i)
			{
				case X:
					v.push_back((uint32_t)m_x);
					break;
				case Y:
					v.push_back((uint32_t)m_y);
					break;
				case ENTERING_TIME:
					v.push_back((uint32_t)m_entering_time);
					break;
				case LEAVING_TIME:
					v.push_back((uint32_t)m_leaving_time);
					break;
				default:
					MEHDI_ERROR_LINE("undefined type of element when serializing location.");
					return false;
			}
		return true;
	}

	/*
	 * deserializing the location with uint32_t elements.
	 * (int32_t elementes are silently converted from uint32_t elementes).
	 */
	bool SetWithSerializedVector32(std::vector<uint32_t> v)
	{
		if (v.size() < GetNumberOf_32_bit_SerializedItems())
		{
			MEHDI_ERROR_LINE("Location information vector does not have enough fields to be used for setting location");
			return false;
		}
		for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			switch(i)
			{
				case X:
					m_x = v[i];
					break;
				case Y:
					m_y = v[i];
					break;
				case ENTERING_TIME:
					m_entering_time = v[i];
					break;
				case LEAVING_TIME:
					m_leaving_time = v[i];
					break;
				default:
					MEHDI_ERROR_LINE("undefined field when deserializing location.");
					return false;
			}
		return true;
	}


	/*
	 * gets the x coordinate of location
	 */
	coordinate GetX() const
	{
		return m_x;
	}

	/*
	 * gets the y coordinate of location
	 */
	coordinate GetY() const
	{
		return m_y;
	}

	/*
	 * gets the time user has entered to this location
	 */
	elapsed_time GetEnteringTime() const
	{
		return m_entering_time;
	}

	/*
	 * gets the time user has left this location
	 */
	elapsed_time GetLeavingTime() const
	{
		return m_leaving_time;
	}

	/*
	 *
	 */
	void UpdateLeavingTime(elapsed_time leaving_time)
	{
		m_leaving_time = leaving_time;
	}

	/*
	 * checks if user is in location now
	 */
	bool IsValidNow(elapsed_time now/*, bool ignoreEnteringTime*/) const
	{
		if (m_entering_time <= now && now <= m_leaving_time)
			return true;
		return false;
	}

	/*
	 * checks if this location is equal to the location sent as parameter
	 */
	bool IsEqual(Location l) const
	{
		for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			switch(i)
			{
				case X:
					if (m_x != l.GetX())
						return false;
					break;
				case Y:
					if (m_y != l.GetY())
						return false;
					break;
				case ENTERING_TIME:
					if (m_entering_time != l.GetEnteringTime())
						return false;
					break;
				case LEAVING_TIME:
					if (m_leaving_time != l.GetLeavingTime())
						return false;
					break;
				default:
					MEHDI_ERROR_LINE("undefined field in comparing locations.");
					return false;
			}
		return true;
	}

	/*
	 * location is reachable if the distance between there and location is not greater than limit
	 */
	bool IsReachable(coordinate x, coordinate y, u_coordinate limit, elapsed_time t) const
	{
		return (Position::Distance(m_x, m_y, x, y) <= limit) && (m_entering_time <= t) && (t <= m_leaving_time);
	}

	/*
	 *
	 */
	bool BelognsToThisDay(elapsed_time time) const
	{
		if (m_entering_time <= time && time <= m_leaving_time)
			return true;
		return false;
	}

	/*
	 *
	 */
	void Serialize64(Buffer::Iterator& buffer) const
	{
	  vector<uint64_t> v;
	  GetSerializedVector64(v);
	  for (vector<uint64_t>::iterator it = v.begin(); it != v.end(); it++)
		  buffer.WriteU64(*it);
	}

	/*
	 *
	 */
	void Serialize32(Buffer::Iterator& buffer) const
	{
	  vector<uint32_t> v;
	  GetSerializedVector32(v);
	  for (vector<uint32_t>::iterator it = v.begin(); it != v.end(); it++)
		  buffer.WriteU32(*it);
	}

	/*
	 *
	 */
	void Deserialize64(Buffer::Iterator& buffer)
	{
		//create vector for serialized items of location
		vector<uint64_t> v(Location::GetNumberOf_64_bit_SerializedItems());
		//read serialized items of the location
		for (uint32_t j = 0; j < mehdi::Location::GetNumberOf_64_bit_SerializedItems(); j++)
			v[j] = buffer.ReadU64();
		//set the location data using the read serialized items
		SetWithSerializedVector64(v);
	}

	/*
	 *
	 */
	void Deserialize32(Buffer::Iterator& buffer)
	{
		//create vector for serialized items of location
		vector<uint32_t> v(Location::GetNumberOf_32_bit_SerializedItems());
		//read serialized items of the location
		for (uint32_t j = 0; j < mehdi::Location::GetNumberOf_32_bit_SerializedItems(); j++)
			v[j] = buffer.ReadU32();
		//set the location data using the read serialized items
		SetWithSerializedVector32(v);
	}

}; //class Location



struct LocationCompare
{
  bool operator() (const Location& l1, const Location& l2) const
	{
		if (!l1.IsEqual(l2))
			return true;
		return false;
	}
};
typedef set<Location, LocationCompare> loc_container;
typedef loc_container::iterator loc_iterator;



class LocationChain
{
	vector<Location> m_chain;
public:
	/*
	 *
	 */
	void Clear()
	{
		m_chain.clear();
	}

	/*
	 *
	 */
	void AddLocation(Location location)
	{
		m_chain.push_back(location);
	}

	/*
	 * updates the leaving time of the last location in the chain
	 */
	void UpdateLeavingTime(elapsed_time leaving_time)
	{
		m_chain.back().UpdateLeavingTime(leaving_time);
	}

	/*
	 *
	 */
	size_t GetSize() const
	{
		return m_chain.size();
	}

	/*
	 *
	 */
	vector<Location> GetChainVector() const
	{
		return m_chain;
	}

	/*
	 *
	 */
	LocationChain GetTimeShiftedLocationChain(elapsed_time shifting_time)
	{
		LocationChain new_chain;
		for (vector<Location>::iterator it = m_chain.begin(); it != m_chain.end(); it++)
			new_chain.AddLocation(Location(it->GetX(), it->GetY(),
					it->GetEnteringTime() > shifting_time ? it->GetEnteringTime() - shifting_time : 0,
					it->GetLeavingTime() > shifting_time ? it->GetLeavingTime() - shifting_time : 0));

		return new_chain;
	}

	/*
	 *
	 */
	elapsed_time GetStartTime() const
	{
		if (m_chain.size() == 0)
			return INVALID_TIME;
		return m_chain.front().GetEnteringTime();
	}

	/*
	 *
	 */
	elapsed_time GetEndTime() const
	{
		if (m_chain.size() == 0)
			return INVALID_TIME;
		return m_chain.back().GetLeavingTime();
	}

	/*
	 *
	 */
	size_t GetSerializedSize64() const
	{
		//number of elements
		size_t size = sizeof(uint32_t);
		//elements themselves
		size += m_chain.size() * Location::GetNumberOf_64_bit_SerializedItems() * sizeof(uint64_t);

		return size;
	}

	/*
	 *
	 */
	size_t GetSerializedSize32() const
	{
		//number of elements
		size_t size = sizeof(uint32_t);
		//elements themselves
		size += m_chain.size() * Location::GetNumberOf_32_bit_SerializedItems() * sizeof(uint32_t);

		return size;
	}

	/*
	 *
	 */
	void Serialize64(Buffer::Iterator& buffer) const
	{
		//first write number of locations
		buffer.WriteU32((uint32_t)m_chain.size());
		//now serialize all locations of this chain
		for(vector<Location>::const_iterator it = m_chain.begin(); it != m_chain.end(); it++)
		{
		  vector<uint64_t> v;
		  it->GetSerializedVector64(v);
		  for (vector<uint64_t>::iterator it2 = v.begin(); it2 != v.end(); it2++)
			  buffer.WriteU64(*it2);
		}
	}

	/*
	 *
	 */
	void Serialize32(Buffer::Iterator& buffer) const
	{
		//first write number of locations
		buffer.WriteU32((uint32_t)m_chain.size());
		//now serialize all locations of this chain
		for(vector<Location>::const_iterator it = m_chain.begin(); it != m_chain.end(); it++)
		{
		  vector<uint32_t> v;
		  it->GetSerializedVector32(v);
		  for (vector<uint32_t>::iterator it2 = v.begin(); it2 != v.end(); it2++)
			  buffer.WriteU32(*it2);
		}
	}

	/*
	 *
	 */
	void Deserialize64(Buffer::Iterator& buffer)
	{
		Clear();
		//first read number of locations in this chain
		uint32_t numberOfLocations = buffer.ReadU32();
		//now read all locations of this chain
		for (uint32_t i = 0; i < numberOfLocations; i++)
		{
			//create vector for serialized items of location
			vector<uint64_t> v(Location::GetNumberOf_64_bit_SerializedItems());
			//read serialized items of the location
			for (uint32_t j = 0; j < mehdi::Location::GetNumberOf_64_bit_SerializedItems(); j++)
				v[j] = buffer.ReadU64();
			//create the location
			Location location = Location();
			//set the location data using the read serialized items
			location.SetWithSerializedVector64(v);
			//add the location to the chain of locations
			AddLocation(location);
		}
	}

	/*
	 *
	 */
	void Deserialize32(Buffer::Iterator& buffer)
	{
		//first read number of locations in this chain
		uint32_t numberOfLocations = buffer.ReadU32();
		//now read all locations of this chain
		for (uint32_t i = 0; i < numberOfLocations; i++)
		{
			//create vector for serialized items of location
			vector<uint32_t> v(Location::GetNumberOf_32_bit_SerializedItems());
			//read serialized items of the location
			for (uint32_t j = 0; j < mehdi::Location::GetNumberOf_32_bit_SerializedItems(); j++)
				v[j] = buffer.ReadU32();
			//create the location
			Location location = Location();
			//set the location data using the read serialized items
			location.SetWithSerializedVector32(v);
			//add the location to the chain of locations
			AddLocation(location);
		}
	}

	/*
	 *
	 */
	bool BelognsToThisDay(elapsed_time time) const
	{
		if (m_chain.size() > 0 && m_chain.front().GetEnteringTime() <= time && time <= m_chain.back().GetLeavingTime())
			return true;
		return false;
	}

	/*
	 *
	 */
	bool IsEqual(LocationChain other_chain)
	{
		vector<Location> v_locations = other_chain.GetChainVector();

		if (m_chain.size() != v_locations.size())
			return false;
		for (size_t i = 0; i < m_chain.size(); i++)
			if (!m_chain[i].IsEqual(v_locations[i]))
				return false;
		return true;
	}

	/*
	 *
	 */
	bool IsReachable(coordinate x, coordinate y, coordinate limit, elapsed_time now) const
	{
		for (vector<Location>::const_iterator it = m_chain.begin(); it != m_chain.end(); it++)
			if (it->IsReachable(x, y, limit, now))
				return true;
		return false;
	}

}; //class LocationChain

//////////////////////////////
// struct for comparing time of positions
// required for sorting positions
/////////////////////////////
struct LocationChainComparatorRegardingEndTime
{
    bool operator()( const LocationChain& lc1, const LocationChain& lc2 ) const
    {
    	return lc1.GetEndTime() < lc2.GetEndTime();
    }
};



////each user will have a chain of locations.
////so a set of vector of location is needed for packets. (it should be set so that repetitive chains do not appear)
////each user will add his own chain of locations for today to the set
//struct LocationChainCompare
//{
//  bool operator() (const LocationChain& chain1, const LocationChain& chain2) const
//	{
//		vector<Location> v_locations_1 = chain1.GetChainVector();
//		vector<Location> v_locations_2 = chain2.GetChainVector();
//
//		if (v_locations_1.size() != v_locations_2.size())
//			return true;
//		for (size_t i = 0; i < v_locations_1.size(); i++)
//			if (!v_locations_1[i].IsEqual(v_locations_2[i]))
//				return true;
//		return false;
//	}
//};
//typedef set<LocationChain, LocationChainCompare> loc_chain_container;
//typedef loc_chain_container::iterator loc_chain_iterator;

}  //namespace mehdi
}  //namespace ndn
} //namespace ns3

#endif // NDN_MEHDI_H
