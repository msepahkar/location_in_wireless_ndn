/*
 * ndn-mehdi-report.cc
 *
 *  Created on: May 5, 2016
 *      Author: mehdi
 */
#include "src/ndnSIM/mehdi/ndn-mehdi-report.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-tools.h"
#include "iomanip"
#include "ns3/log.h"
#include <sstream>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>

namespace ns3{
namespace ndn{
namespace mehdi{


using namespace std;

uint64_t Report::n_interest_packets;
uint64_t Report::n_data_packets;
uint64_t Report::n_requests;
uint64_t Report::n_successful_requests;
uint32_t Report::n_errors;
uint64_t Report::n_int;
uint64_t Report::n_ack;
uint64_t Report::n_cmd;
uint64_t Report::n_dropped_cmd;
uint64_t Report::n_dropped_ack;
time_t Report::starting_time;
time_t Report::ending_time;
string Report::report_file_name;
string Report::original_report_file_name;
bool Report::verbose;
bool Report::detailed_log;
bool Report::auto_name;

void
Report::Init()
{
	n_data_packets = 0;
	n_interest_packets = 0;
	n_requests = 0;
	n_successful_requests = 0;
	n_errors = 0;
	n_int = 0;
	n_ack = 0;
	n_cmd = 0;
	n_dropped_cmd = 0;
	n_dropped_ack = 0;
	::time(&starting_time);

	if (auto_name)
	{
		int i = 0;
		do{
			stringstream ss;
			ss << "reports/";
			if (Parameters::other_simulation)
			{
				ss << Parameters::other_simulation << "_" << Parameters::exact_location << "_";
			}
			ss << Parameters::max_smart_normal_retransmit << Parameters::max_smart_force_forward_retransmit << Parameters::max_non_smart_retransmit << "_";
			ss << Parameters::cache_size << "_";
			ss << Parameters::smart_propagation << "_";
			ss << Parameters::real_next_locations << "_";
			ss << ++i << ".txt";
			report_file_name = ss.str();
		}while (ifstream(report_file_name.c_str()));
	}

	original_report_file_name = report_file_name;
	report_file_name = report_file_name + "_working";
	if (ifstream(report_file_name.c_str()))
		rename(report_file_name.c_str(), (report_file_name + "_interrupted").c_str());

	//report the initializations
	ofstream log_file(report_file_name.c_str());

	log_file << "running started at " << Tools::GetDateTime() << " ..." << endl;
	cout << "running started at " << Tools::GetDateTime() << " ..." << endl;
	log_file << "other simulation: " << Parameters::other_simulation << endl;
	log_file << "exact locatoin: " << Parameters::exact_location << endl;
	log_file << "smart propagation: " << Parameters::smart_propagation << endl;
	log_file << "real next location: " << Parameters::real_next_locations << endl;
	log_file << "number of contents: " << Parameters::n_contents << endl;
	log_file << "number of cache entries: " << Parameters::cache_size << endl;
	log_file << "reachability limit: " << mehdi_reachability_limit << endl;
	log_file << "validity time of current and predicted location: " << mehdi_validity_time_of_predicted_next_location << endl;
	log_file << "maximum non-smart retransmissions: " << Parameters::max_non_smart_retransmit << endl;
	log_file << "maximum normal smart retransmissions: " << Parameters::max_smart_normal_retransmit << endl;
	log_file << "maximum force forwarding retransmissions: " << Parameters::max_smart_force_forward_retransmit << endl;
	log_file.close();
}

/*
 *
 */
void
Report::IncrementErrorCounter()
{
	n_errors++;
	if (exit_if_max_error_occurred && n_errors >= max_n_errors)
		ErrorLine("too many errors in packet sendings.", true, -1);
}

/*
 * reports a message
 */
void
Report::SimpleReport(string message)
{
	//standard output
	if (verbose)
		cout << message;

	//log file
	ofstream log_file;

	//open the log file
	log_file.open(report_file_name.c_str(), ios::app);

	log_file << message;

	log_file.close();

}

/*
 * reports a message
 */
void
Report::ReportLine(string message)
{
	//standard output
	if (verbose)
		cout << message << endl;

	//log file
	ofstream log_file;

	//open the log file
	log_file.open(report_file_name.c_str(), ios::app);

	log_file << message << endl;

	log_file.close();
}

/*
 * reports an error
 */
void
Report::ErrorLine(string message, bool should_exit, int exit_code)
{
	ReportLine("************************ ERROR ************************");
	ReportLine(message);
	ReportLine("*******************************************************");
	if (should_exit)
	{
		Finalize();
		exit(exit_code);
	}
}

/*
 *
 */
void
Report::ReportPacketHappening(string happening, string sender, string receiver, string prefix, elapsed_time time, bool ignore_counter)
{
	static uint64_t counter = 0;

	//log file
	ofstream log_file;

	//open the log file
	log_file.open(report_file_name.c_str(), ios::app);

	//log only every 1000 happenings if not detailed log
	if (++counter % 1000 == 0 || detailed_log || ignore_counter)
	{
		if (detailed_log)
		{
			//happenning and prefix
			log_file << setw(15) << left << happening << setw(12) << left << prefix;
			if (verbose)
				cout << setw(15) << left << happening << setw(12) << left << prefix;

			//sender and receiver
			log_file << setw(10) << right << sender << " -> " << setw(10) << left << receiver;
			if (verbose)
				cout << setw(10) << right << sender << " -> " << setw(10) << left << receiver;
		}

		//time
		log_file << time << ",";
		if (verbose)
			cout << "time: " << setw(12) << right << time << setw(3) << " ";

		//total number of interest packets
		log_file << Report::n_interest_packets << ",";
		if (verbose)
			cout << "interest>>>> " << setw(8) << Report::n_interest_packets << setw(3) << " ";

		//total number of data packets
		log_file << Report::n_data_packets << ",";
		if (verbose)
			cout << "data>>>> " << setw(8) << Report::n_data_packets << setw(3) << " ";

		//total requests
		log_file << n_requests << ",";
		if (verbose)
			cout << "requests:" << setw(8) << n_requests << setw(3) << " ";

		//successful requests
		log_file << n_successful_requests << ",";
		if (verbose)
			cout << "success:" << setw(8) << n_successful_requests << setw(3) << " ";

		//errors
		log_file << n_errors << ",";
		if (verbose)
			cout << "errors:" << setw(5) << n_errors << setw(3) << " ";

		//virtual memory
		log_file << Tools::VirtualMemUsage() << ",";
		if (verbose)
			cout << "vmem:" << setw(10) << fixed << setprecision(0) << Tools::VirtualMemUsage() << setw(3) << " ";

		//resident memory
		log_file << Tools::ResidentMemUsage();
		if (verbose)
			cout << "rmem:" << setw(10) << fixed << setprecision(0) << Tools::ResidentMemUsage();

		if (Parameters::other_simulation)
		{
			//INT
			log_file << "," << n_int << ",";
			if (verbose)
				cout << setw(3) << " " << "int:" << setw(8) << n_int << setw(3) << " ";
			//ACK
			log_file << n_ack << ",";
			if (verbose)
				cout << "ack:" << setw(8) << n_ack << setw(3) << " ";
			//CMD
			log_file << n_cmd << ",";
			if (verbose)
				cout << "cmd:" << setw(8) << n_cmd << setw(3) << " ";
			//dropped CMD
			log_file << n_dropped_cmd << ",";
			if (verbose)
				cout << "d_cmd:" <<  setw(8) << n_dropped_cmd << setw(3) << " ";
			//dropped ACK
			log_file << n_dropped_ack;
			if (verbose)
				cout << "d_ack:" <<  setw(8) << n_dropped_ack;
		}

		//end of line
		log_file << endl;
		if (verbose)
			cout << endl;
	}


	//close the log file
	log_file.close();
}

/*
 *
 */
void
Report::Finalize()
{
	::time(&ending_time);
	double duration = difftime(ending_time, starting_time);
	ofstream log_file(report_file_name.c_str(), ios::app);
	log_file << "running finished at " << Tools::GetDateTime() << endl;
	cout << "running finished at " << Tools::GetDateTime() << endl;
	int hours = duration / 3600;
	int minutes = (duration - hours * 3600) / 60;
	int seconds = duration - hours * 3600 - minutes * 60;
	log_file << "duration of run: " << hours << ":" << minutes << ":" << seconds << endl;
	cout << "duration of run: " << hours << ":" << minutes << ":" << seconds << endl;
	log_file.close();
	rename(report_file_name.c_str(), original_report_file_name.c_str());
}

}//namespace mehdi
}//namespace ndn
}//namespace ns3






