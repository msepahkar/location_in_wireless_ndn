/*
 * ndn-mehdi-preperation.h
 *
 *  Created on: Feb 11, 2016
 *      Author: mehdi
 */

#ifndef SRC_NDNSIM_MODEL_MEHDI_NDN_MEHDI_PREPARATION_H_
#define SRC_NDNSIM_MODEL_MEHDI_NDN_MEHDI_PREPARATION_H_

#include "src/ndnSIM/mehdi/ndn-mehdi-location.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-tools.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-centroid.h"
#include "ns3/node.h"
#include "ns3/mobility-module.h"
#include <vector>
#include <map>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <ns3/application.h>
#include <time.h>
#include <iomanip>
#include <queue>

namespace ns3 {
namespace ndn {
namespace mehdi {

using namespace std;

/////////////////////////////
// Preparation class
// static methods for preparing nodes
/////////////////////////////
class Preparation
{
public:

	/*
	 * reads movement data of all users from text files
	 * (text files are prepared from the original location file by some java program).
	 */
	static bool ReadTextData(vector<MehdiNode>& v)
	{
		//suppose success
		bool result = true;

		//read nodes from text files
		for (size_t i = 0; i < NUMBER_OF_NODES; i++)
		{
			stringstream ss_text_file_name, ss;
			//prepare file name
			ss_text_file_name << movements_dir_path << "user " << i + 1 << ".csv";
			//create a new mehdi node (Ptr<MehdiNode> should be used because otherwise ns3 dose not work!)
			Ptr<MehdiNode> p = CreateObject<MehdiNode>();
			v.push_back(*(p->GetObject<MehdiNode>()));
			//report
			MEHDI_REPORT_LINE("reading movements of user " << i + 1 << " from text file ...");
			//read the node movements from the text file
			if (!v.back().ReadMovementsFromTextFile_And_SortThem(ss_text_file_name.str()))
			{
				v.pop_back();
				result = false;
			}
			else
			{
				MEHDI_REPORT_LINE("movements of " << v.back().GetUserFriendlyName() << " read from file." << " number of movments: " << v.back().GetPositions().size());
			}
		}
		return result;
	}

	/*
	 * writes the required data of all nodes to binary files.
	 * (like movement data, poi data, movement probabilities, ...)
	 */
	static bool WriteBinaryData(vector<MehdiNode> v)
	{
		//suppose success
		bool result = true;

		for (vector<MehdiNode>::iterator it = v.begin(); it != v.end(); it++)
		{
			stringstream ss_binary_file_name, ss;
			//prepare file name
			ss_binary_file_name << movements_dir_path << it->GetUserFriendlyName() << ".bin";
			//report
			MEHDI_REPORT_LINE("writing data of " << it->GetUserFriendlyName() << " to binary file ...");

			//write the node data to binary file
			if (it->WriteToBinaryFile(ss_binary_file_name.str()))
			{
				MEHDI_REPORT_LINE("data of " << it->GetUserFriendlyName() << " written to binary file.");
			}
			else
			{
				result = false;
				MEHDI_REPORT_LINE("failed to write data of " << it->GetUserFriendlyName() << " to binary file.");
			}
		}
		return result;
	}

	/*
	 * reads data of all users from binary files.
	 */
	static bool ReadBinaryData(vector<MehdiNode>& v)
	{
		//suppose success
		bool result = true;

		//read nodes from binary file
		for (size_t i = 0; i < NUMBER_OF_NODES; i++)
		{
			stringstream ss_binary_file_name, ss;
			//prepare file name
			ss_binary_file_name << movements_dir_path << "user " << i + 1 << ".bin";
			//create a new mehdi node
			v.push_back(MehdiNode());
			//report
			MEHDI_REPORT_LINE("reading data of user " << i + 1 << " from binary file ...");
			//read the node movements from the binary file
			if (!v.back().ReadFromBinaryFile(ss_binary_file_name.str()))
			{
				MEHDI_REPORT_LINE("failed to read binary data of " << v.back().GetUserFriendlyName() << " from binary file.");
				v.pop_back();
				result = false;
			}
			else
			{
				MEHDI_REPORT_LINE("movements of " << v.back().GetUserFriendlyName() << " read from binary file.");
			}
		}
		return result;
	}

	/*
	 *
	 */
	static bool ReadText_Calculate_WriteBinary_CheckWrittenBinary()
	{
		Report::verbose = true;
		Report::auto_name = true;
		vector<MehdiNode> nodes;
		if (!ReadTextData(nodes))
			MEHDI_FATAL_ERROR("cannot read text file.", -1);
		for (vector<MehdiNode>::iterator it = nodes.begin(); it != nodes.end(); it++)
		{
			if (!it->FindChainsOfLocations())
				MEHDI_FATAL_ERROR("cannot find chains of locations.", -1);

			stringstream ss;
			ss << movements_dir_path << it->GetUserFriendlyName() << ".bin";

			if (!it->WriteToBinaryFile(ss.str()))
				MEHDI_FATAL_ERROR("cannot write the binary file." , -1);

			MEHDI_REPORT_LINE("comparing ...");
			MehdiNode node;
			if (!node.ReadFromBinaryFile(ss.str()))
				MEHDI_FATAL_ERROR("cannot read from binary file.", -1);
			if (!node.IsEqual(*it))
				MEHDI_FATAL_ERROR("read node does not match.", -1);
		}
		return true;
	}

	/*
	 * read the data from binary file and compare it to the data read
	 *  from text file with data calculated.
	 */
	static bool ReadBinary_Compare(vector<MehdiNode> v_text)
	{
		vector<MehdiNode> vb;

		//read the data from binary file
		if (!ReadBinaryData(vb))
			return false;

		//check all nodes for equality
		for (size_t i = 0; i < NUMBER_OF_NODES; i++)
			if (!v_text[i].IsEqual(vb[i]))
			{
				MEHDI_ERROR_LINE("binary data does not match with text data.");
				return false;
			}

		//everything ok
		return true;
	}

	/*
	 * reads data of all users from binary files.
	 * it only saves users whose available timings are between limits
	 */
	static bool ReadBinaryData2(vector<Ptr<MehdiNode> >& v, elapsed_time start_time_limit, elapsed_time end_time_limit)
	{
		//suppose success
		bool result = true;

		//read nodes from binary file
		for (size_t i = 0; i < NUMBER_OF_NODES; i++)
		{
			stringstream ss_binary_file_name, ss;
			//prepare file name
			ss_binary_file_name << movements_dir_path << "user " << i + 1 << ".bin";
			//create a new mehdi node
			v.push_back(CreateObject<MehdiNode>());
			//report
			MEHDI_REPORT_LINE("reading data of user " << i + 1 << " from binary file ...");
			//read the node movements from the binary file
			if (!v.back()->GetObject<MehdiNode>()->ReadFromBinaryFile(ss_binary_file_name.str()))
			{
				result = false;
				MEHDI_REPORT_LINE("failed to read binary data of " << v.back()->GetObject<MehdiNode>()->GetUserFriendlyName() << " from binary file.");
				v.pop_back();
			}
			else if (v.back()->GetObject<MehdiNode>()->GetFirstAvailableTime() > start_time_limit || v.back()->GetObject<MehdiNode>()->GetLastAvailableTime() < end_time_limit)
			{
				MEHDI_REPORT_LINE("movements of " << v.back()->GetObject<MehdiNode>()->GetUserFriendlyName() << " read from binary file, but limits are not satisfied.");
				v.pop_back();
			}
			else
			{
				MEHDI_REPORT_LINE("movements of " << v.back()->GetObject<MehdiNode>()->GetUserFriendlyName() << " read from binary file.");
			}
		}

		MEHDI_REPORT_LINE("total users in play: " << v.size());
		for (vector<Ptr<MehdiNode> >::iterator it = v.begin(); it != v.end(); it++)
		{
			Ptr<MehdiNode> n = (*it)->GetObject<MehdiNode>();

			MEHDI_REPORT(setw(15) << left << n->GetUserFriendlyName() << ", first time: ");
			MEHDI_REPORT(setw(10) << right << n->GetFirstAvailableTime());
			MEHDI_REPORT(setw(15) << left << ", last time: ");
			MEHDI_REPORT(setw(10) << right << n->GetLastAvailableTime());
			MEHDI_REPORT(setw(15) << left << ", difference: ");
			MEHDI_REPORT_LINE(setw(19) << right << n->GetLastAvailableTime() - n->GetFirstAvailableTime());
		}

		  return result;
	}

	/*
	 * finds the first and last available times in data
	 */
	static bool FindAvailableTimeRange(vector<Ptr<MehdiNode> > v,
			elapsed_time& min_first_available_time,
			elapsed_time& max_first_available_time,
			elapsed_time& min_last_available_time,
			elapsed_time& max_last_available_time)
	{
		min_first_available_time = LAST_TIME;
		max_first_available_time = 0;
		min_last_available_time = LAST_TIME;
		max_last_available_time = 0;
		elapsed_time first_available_time, last_available_time;
		for (vector<Ptr<MehdiNode> >::iterator it = v.begin(); it != v.end(); it++)
		{
			first_available_time = (*it)->GetObject<MehdiNode>()->GetFirstAvailableTime();
			if (first_available_time < min_first_available_time)
				min_first_available_time = first_available_time;
			if (first_available_time > max_first_available_time)
				max_first_available_time = first_available_time;
			last_available_time = (*it)->GetObject<MehdiNode>()->GetLastAvailableTime();
			if (last_available_time < min_last_available_time)
				min_last_available_time = last_available_time;
			if (last_available_time > max_last_available_time)
				max_last_available_time = last_available_time;
		}
		return true;
	}

	/*
	 *
	 */
	static void SetPosition1(Ptr<MehdiNode> node, uint position_index)
	{
		//calculate required parameters
		vector<Position> v_positions = node->GetObject<MehdiNode>()->GetPositions();
		Position p = v_positions[position_index];
		Vector v = Vector((double)p.GetX(), (double)p.GetY(), 0.0);

		//set current position
		node->GetObject<MobilityModel>()->SetPosition(v);

		//check if any next position is available
		//if yes schedule for next position
		if (position_index + 1 < v_positions.size())
		{
			Time schedule_time = Seconds(v_positions[position_index + 1].GetTime() - mehdi_simulation_starting_time - Simulator::Now().GetSeconds());
			Simulator::Schedule(schedule_time, &SetPosition1, node, position_index + 1);
		}
	}

	/*
	 *
	 */
	static void SetPosition2(Ptr<MehdiNode> node, coordinate x, coordinate y)
	{
		//calculate required parameters
		Vector v = Vector((double)x, (double)y, 0.0);

		//set current position
		node->GetObject<MobilityModel>()->SetPosition(v);
	}

	/*
	 *
	 */
	static bool ScheduleMovements(vector<Ptr<MehdiNode> >& nodes)
	{
		for (uint i = 0; i < nodes.size(); i++)
		{
			//get the node
			Ptr<MehdiNode> node = nodes[i]->GetObject<MehdiNode>();

			//first install mobility model
			MobilityHelper mobility;
			Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
			positionAlloc->Add (Vector (INVALID_COORDINATE, INVALID_COORDINATE - i * 1000, 0.0));
			mobility.SetPositionAllocator (positionAlloc);
			mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
			mobility.Install(node);

			//get position vector
			vector<Position> positions = node->GetPositions();

			//schedule first position after starting of simulation
			//first find the first position
			size_t first_position_index = 0;
			Position first_position = positions[first_position_index];
			while (first_position.GetTime() < mehdi_simulation_starting_time && first_position_index < positions.size())
				first_position = positions[++first_position_index];
			//now schedule the movement if first position is valid
			if (first_position.GetTime() >= mehdi_simulation_starting_time )
			{
				Simulator::Schedule(Seconds((double)first_position.GetTime() - mehdi_simulation_starting_time), &SetPosition1, node, first_position_index);
				//schedule invalidating positions 10 seconds after the last position
				Position last_position = positions[positions.size() - 1];
				Simulator::Schedule(Seconds((double)last_position.GetTime() - mehdi_simulation_starting_time + 10), &SetPosition2, node, INVALID_COORDINATE, INVALID_COORDINATE - i * 1000);
			}

		}
		return true;
	}

	/*
	 *
	 */
	static int FindNumberOfConsumersBasedOnTime(int t)
	{
		//decided on number of consumers depending on the hour of the day
		// based on the reports of the article from prof. Schindelhauer
		int number_of_consumers = 0;
		switch ((int)((Simulator::Now().GetSeconds()) / 3600) % 24)
		{
		case 0:
		case 1:
			number_of_consumers = 15;
			break;
		case 2:
		case 3:
			number_of_consumers = 10;
			break;
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
			number_of_consumers = 5;
			break;
		case 10:
		case 11:
			number_of_consumers = 10;
			break;
		case 12:
		case 13:
			number_of_consumers = 15;
			break;
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
			number_of_consumers = 20;
			break;
		case 20:
		case 21:
		case 22:
		case 23:
			number_of_consumers = 25;
			break;
		}
		return number_of_consumers;
	}

	/*
	 *
	 */
	static void CalculateRequestTimes(vector<Ptr<MehdiNode> > nodes, vector<string> prefixes)
	{
		const int interval_length = 15 * 60;
		const int average_request_interval = 30;
		map<uint, queue<pair<string, elapsed_time> > > request_times;

		//create a list of start time offsets for all requests
		queue<int> start_time_offsets;
		Tools::Randomize();
		for (uint i = 0; i < mehdi_simulation_duration / average_request_interval; i++)
			start_time_offsets.push(rand() % interval_length);

		//create a list of prefix indexes which will be requested
		queue<int> prefix_indexes;
		Tools::Randomize();
		for (uint i = 0; i < mehdi_simulation_duration / average_request_interval; i++)
			prefix_indexes.push(Tools::Rand_PowerLaw(0, prefixes.size()));

		//now create a list of "which user asks for what content at what time"
		Tools::Randomize();
		MEHDI_REPORT_LINE("creating request times ...");
		for (elapsed_time t = 0; t < mehdi_simulation_duration; t += interval_length)
		{
			int number_of_consumers = FindNumberOfConsumersBasedOnTime(t);
			for (int i = 0; i < number_of_consumers; i++)
			{
				int consumer_index = Tools::Rand_PowerLaw(0, nodes.size());
				if (prefix_indexes.size() < 1)
					MEHDI_FATAL_ERROR("not enough number of prefix indexes.", -1);
				string prefix = prefixes[prefix_indexes.front()];
				prefix_indexes.pop();
				if (prefix_indexes.size() < 1)
					MEHDI_FATAL_ERROR("not enough number of start time offsets.", -1);
				request_times[consumer_index].push(pair<string, elapsed_time>(prefix, t + start_time_offsets.front()));
				start_time_offsets.pop();
			}
		}

		int total_requests = 0;
		for (map<uint, queue<pair<string, elapsed_time> > >::iterator it = request_times.begin(); it != request_times.end(); it++)
		{
			nodes[it->first]->request_times = it->second;
			total_requests += it->second.size();
		}

		MEHDI_REPORT_LINE("total requests: " << total_requests);
		MEHDI_REPORT_LINE("average request per user: " << (total_requests / request_times.size()));


	}

	/*
	 *
	 */
	static bool PrepareWireless(NodeContainer& nodes)
	{
		std::string phyMode ("DsssRate1Mbps");
//		bool verbose = false;

		// disable fragmentation for frames below 2200 bytes
//		Config::SetDefault ("ns3::WifiRemoteStationManager::FragmentationThreshold", StringValue ("2200"));
		// turn off RTS/CTS for frames below 2200 bytes
//		Config::SetDefault ("ns3::WifiRemoteStationManager::RtsCtsThreshold", StringValue ("2200"));
		// Fix non-unicast data rate to be the same as that of unicast
		Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode", StringValue (phyMode));

		// The below set of helpers will help us to put together the wifi NICs we want
		WifiHelper wifi;
//		if (verbose)
//		{
//		  wifi.EnableLogComponents ();  // Turn on all Wifi logging
//		}
		wifi.SetStandard (WIFI_PHY_STANDARD_80211g);

		YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
		// This is one parameter that matters when using FixedRssLossModel
		// set it to zero; otherwise, gain will be added
//		wifiPhy.Set ("RxGain", DoubleValue (0) );
		// ns-3 supports RadioTap and Prism tracing extensions for 802.11b
//		wifiPhy.SetPcapDataLinkType (YansWifiPhyHelper::DLT_IEEE802_11_RADIO);

		YansWifiChannelHelper wifiChannel;
		wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
//		wifiChannel.SetPropagationDelay ("ns3::RandomPropagationDelayModel");
		// The below FixedRssLossModel will cause the rss to be fixed regardless
		// of the distance between the two stations, and the transmit power
		wifiChannel.AddPropagationLoss ("ns3::LogDistancePropagationLossModel", "Exponent", DoubleValue (3.0));
//		wifiChannel.AddPropagationLoss ("ns3::RandomPropagationLossModel");
		wifiPhy.SetChannel (wifiChannel.Create ());

		// Add a non-QoS upper mac, and disable rate control
		NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
		wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
									"DataMode",StringValue (phyMode),
									"ControlMode",StringValue (phyMode));
		// Set it to adhoc mode
		wifiMac.SetType ("ns3::AdhocWifiMac");
		NetDeviceContainer devices = wifi.Install (wifiPhy, wifiMac, nodes);

		return true;
	}

	/*
	 *
	 */
	static bool SimpleExampleOld()
	{
		std::string prefix("/prefix");
		NodeContainer nodes;
		vector<Ptr<MehdiNode> > mehdiNodes;

		if (!Preparation::ReadBinaryData2(mehdiNodes, 10*1000*1000, 20*1000*1000))
			exit(-1);

		Ptr<MehdiNode> consumer = mehdiNodes[10];//CreateObject<MehdiNode> ("node 0", "consumer");
		Ptr<MehdiNode> router1 = mehdiNodes[11]; //CreateObject<MehdiNode> ("node 1", "router 1");
		Ptr<MehdiNode> router2 = mehdiNodes[12]; //CreateObject<MehdiNode> ("node 2", "router 2");
		Ptr<MehdiNode> router3 = mehdiNodes[13]; //CreateObject<MehdiNode> ("node 3", "router 3");
		Ptr<MehdiNode> producer = mehdiNodes[14]; //CreateObject<MehdiNode> ("node 4", "producer");
		nodes.Add(consumer);
		nodes.Add(router1);
		nodes.Add(router2);
		nodes.Add(router3);
		nodes.Add(producer);

		ndn::mehdi::Preparation::PrepareWireless(nodes);

		MobilityHelper mobility;
		Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
		positionAlloc->Add (Vector (0.0, 0.0, 0.0)); //consumer
		mobility.SetPositionAllocator (positionAlloc);
		mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
		mobility.Install(consumer);

		positionAlloc = CreateObject<ListPositionAllocator> ();
		positionAlloc->Add (Vector (100.0, 0.0, 0.0)); //router 1
		mobility.SetPositionAllocator (positionAlloc);
		mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
		mobility.Install(router1);

		positionAlloc = CreateObject<ListPositionAllocator> ();
		positionAlloc->Add (Vector (200.0, 0.0, 0.0)); //router 2
		mobility.SetPositionAllocator (positionAlloc);
		mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
		mobility.Install(router2);

		positionAlloc = CreateObject<ListPositionAllocator> ();
		positionAlloc->Add (Vector (300.0, 0.0, 0.0)); //router 3
		mobility.SetPositionAllocator (positionAlloc);
		mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
		mobility.Install(router3);

		positionAlloc = CreateObject<ListPositionAllocator> ();
		positionAlloc->Add (Vector (400.0, 0.0, 0.0)); //producer
		mobility.SetPositionAllocator (positionAlloc);
		mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
		mobility.Install(producer);

		// Install NDN stack on all nodes
		ndn::StackHelper ndnHelper;
		//flooding
		ndnHelper.SetForwardingStrategy("ns3::ndn::mehdi::MehdiForwarding");
		ndnHelper.InstallAll ();

		// Installing applications

		// Consumer
		ndn::AppHelper consumerHelper ("ns3::ndn::mehdi::MehdiConsumer");
		// Consumer will request /prefix/0, /prefix/1, ...
		consumerHelper.SetPrefix (prefix);
		//  consumerHelper.SetAttribute ("Frequency", StringValue ("1")); // 1 interests a second
		consumerHelper.Install (consumer); // first node
		//Simulator::Schedule(Seconds(1), &f, consumer);

		// Producer
		ndn::AppHelper producerHelper ("ns3::ndn::Producer");
		// Producer will reply to all requests starting with /prefix
		producerHelper.SetPrefix (prefix);
		producerHelper.SetAttribute ("PayloadSize", StringValue("1024"));
		producerHelper.Install (producer); // last node

		//mehdi - manual routing

		//  //get channel of the device
		//  Ptr<Channel> ch = routerDevice->GetChannel();
		Ptr<NetDevice> device;
		Ptr<ns3::ndn::Face> face;
		Ptr<ndn::Fib> fib;
		Ptr<ndn::fib::Entry> fibEntry;
		//consumer
		device = consumer->GetDevice(0);
		face = consumer->GetObject<ndn::L3Protocol>()->GetFaceByNetDevice(device);
		fib = consumer->GetObject<ndn::Fib> ();
		fibEntry = fib->Add (prefix, face, 0);
		//router1
		device = router1->GetDevice(0);
		face = router1->GetObject<ndn::L3Protocol>()->GetFaceByNetDevice(device);
		fib = router1->GetObject<ndn::Fib> ();
		fibEntry = fib->Add (prefix, face, 0);
		//router2
		device = router2->GetDevice(0);
		face = router2->GetObject<ndn::L3Protocol>()->GetFaceByNetDevice(device);
		fib = router2->GetObject<ndn::Fib> ();
		fibEntry = fib->Add (prefix, face, 0);
		//router3
		device = router3->GetDevice(0);
		face = router3->GetObject<ndn::L3Protocol>()->GetFaceByNetDevice(device);
		fib = router3->GetObject<ndn::Fib> ();
		fibEntry = fib->Add (prefix, face, 0);
		//producer
		device = producer->GetDevice(0);
		face = producer->GetObject<ndn::L3Protocol>()->GetFaceByNetDevice(device);
		fib = producer->GetObject<ndn::Fib> ();
		fibEntry = fib->Add (prefix, face, 0);
		//

		//  LogComponentEnable("ndn.Consumer", LOG_LEVEL_INFO);
		//  LogComponentEnable("ndn.Producer", LOG_LEVEL_INFO);

		//Preparation::ScheduleMovements(mehdiNodes);

		Simulator::Stop (Seconds (3));

		Simulator::Run ();
		Simulator::Destroy ();

		return true;
	}

	/*
	 *
	 */
	static bool SimpleExample()
	{
		std::string prefix("/prefix");
		NodeContainer nodes;
		vector<Ptr<MehdiNode> > mehdiNodes;

		if (!Preparation::ReadBinaryData2(mehdiNodes, 10*1000*1000, 20*1000*1000))
			exit(-1);

		if (!Preparation::ScheduleMovements(mehdiNodes))
			exit(-1);


		for (vector<Ptr<MehdiNode> >::iterator it = mehdiNodes.begin(); it != mehdiNodes.end(); it++)
			nodes.Add(*it);

		ndn::mehdi::Preparation::PrepareWireless(nodes);

		// Install NDN stack on all nodes
		ndn::StackHelper ndnHelper;
		//flooding
		ndnHelper.SetForwardingStrategy("ns3::ndn::mehdi::MehdiForwarding");
		ndnHelper.InstallAll ();

		// Installing applications

		// Consumer
		ndn::AppHelper consumerHelper ("ns3::ndn::mehdi::MehdiConsumer");
		// Consumer will request /prefix/0, /prefix/1, ...
		consumerHelper.SetPrefix (prefix);
		//  consumerHelper.SetAttribute ("Frequency", StringValue ("1")); // 1 interests a second
		consumerHelper.Install (mehdiNodes[0]); // first node

		// Producer
		ndn::AppHelper producerHelper ("ns3::ndn::Producer");
		// Producer will reply to all requests starting with /prefix
		producerHelper.SetPrefix (prefix);
		producerHelper.SetAttribute ("PayloadSize", StringValue("1024"));
		producerHelper.Install (mehdiNodes[1]); // last node

		//mehdi - manual routing

		//  //get channel of the device
		//  Ptr<Channel> ch = routerDevice->GetChannel();
		Ptr<NetDevice> device;
		Ptr<ns3::ndn::Face> face;
		Ptr<ndn::Fib> fib;
		Ptr<ndn::fib::Entry> fibEntry;

		for (vector<Ptr<MehdiNode> >::iterator it = mehdiNodes.begin(); it != mehdiNodes.end(); it++)
		{
			device = (*it)->GetDevice(0);
			face = (*it)->GetObject<ndn::L3Protocol>()->GetFaceByNetDevice(device);
			fib = (*it)->GetObject<ndn::Fib> ();
			fibEntry = fib->Add (prefix, face, 0);
		}

		Simulator::Stop (Seconds (20*1000*1000));

		Simulator::Run ();
		Simulator::Destroy ();

		return true;
	}

	/*
	 *
	 */
	static bool TestLocationChain()
	{
		NodeContainer nodes;
		vector<MehdiNode> mehdiNodes;

		if (!Preparation::ReadBinaryData(mehdiNodes))
			exit(-1);
//		int j = 0;
		for (vector<MehdiNode>::iterator it = mehdiNodes.begin(); it != mehdiNodes.end(); it++)
		{
//			if (j++ == 0 || j == 1 || j == 2)
//				continue;
			it->FindChainsOfLocations();
			cout << it->GetUserFriendlyName() << ", " << it->GetLocationChains().size() << " location chains." << endl;
			uint avg = 0;
			uint min = 10000000;
			uint max = 0;
			for (uint i = 0; i < it->GetLocationChains().size(); i++)
			{
				size_t size = it->GetLocationChains()[i].GetSize();
//				cout << "chain size: " << size << endl;
				avg += size;
				if (min > size)
					min = size;
				if (max < size)
					max = size;
			}

//			for (vector<LocationChain>::iterator it2 = it->GetLocationChains().begin(); it2 != it->GetLocationChains().end(); it2++)
//				it2->GetChain();
//				avg += it2->GetChain().size();
			avg /= it->GetLocationChains().size();
			cout << "average size of chain: " << avg << ", min: " << min << ", max: " << max << endl;
//			break;
		}

//		MehdiNode node = mehdiNodes[1];
//		cout << "printing location changes of " << node.GetUserFriendlyName() << " with " << node.GetPOIs().size() << " POIs ..." << endl;
//		int i = 0;
//		for (elapsed_time t = 0 * 1000 * 1000; t < 30 * 1000 * 1000; t++)
//		{
//			if ( t % (3600 * 24) == 0)
//			{
////				cout << "getting chain for day number " << t / (3600 * 24) << "..." << endl;
//				vector<Location> v;
//				node.GetLocationChainForToday(v, t);
//				cout << "day number " << ++i << "(" << t / (3600 * 24) + 1 << ")" << ", " << v.size() << " locations:" << endl;
////				for (vector<Location>::iterator it = v.begin(); it != v.end(); it++)
////				{
////					cout << "(" << it->GetX() << "," << it->GetY() << ")";
////					if (it + 1 != v.end())
////						cout << "->";
////				}
////				cout << endl;
//
//			}
//		}
		return true;
	}

	/*
	 *
	 */
	static bool TestLocationChainTransfer()
	{
		std::string prefix("/prefix");
		NodeContainer nodes;
		vector<Ptr<MehdiNode> > mehdiNodes;

		if (!Preparation::ReadBinaryData2(mehdiNodes, 10*1000*1000, 20*1000*1000))
			exit(-1);

		for (uint i = 0; i < mehdiNodes.size(); i++)
		{
			//get the node
			Ptr<MehdiNode> node = mehdiNodes[i]->GetObject<MehdiNode>();

			//first install mobility model
			MobilityHelper mobility;
			Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
			positionAlloc->Add (Vector (INVALID_COORDINATE, INVALID_COORDINATE - i * 1000, 0.0));
			mobility.SetPositionAllocator (positionAlloc);
			mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
			mobility.Install(node);
		}

		for (vector<Ptr<MehdiNode> >::iterator it = mehdiNodes.begin(); it != mehdiNodes.end(); it++)
			nodes.Add(*it);

		ndn::mehdi::Preparation::PrepareWireless(nodes);

		// Install NDN stack on all nodes
		ndn::StackHelper ndnHelper;
		//flooding
		ndnHelper.SetForwardingStrategy("ns3::ndn::mehdi::MehdiForwarding");
		ndnHelper.InstallAll ();

		// Installing applications

		// Consumer
		Ptr<MehdiNode> consumer = mehdiNodes[0];
		ndn::AppHelper consumerHelper ("ns3::ndn::mehdi::MehdiConsumer");
		// Consumer will request /prefix/0, /prefix/1, ...
		consumerHelper.SetPrefix (prefix);
		//  consumerHelper.SetAttribute ("Frequency", StringValue ("1")); // 1 interests a second
		consumerHelper.Install (consumer); // first node
		consumer->GetObject<MobilityModel>()->SetPosition(Vector(0.0, 0.0, 0.0));

		// Producer
		Ptr<MehdiNode> producer = mehdiNodes[1];
		ndn::AppHelper producerHelper ("ns3::ndn::Producer");
		// Producer will reply to all requests starting with /prefix
		producerHelper.SetPrefix (prefix);
		producerHelper.SetAttribute ("PayloadSize", StringValue("1024"));
		producerHelper.Install (producer); // last node
		producer->GetObject<MobilityModel>()->SetPosition(Vector(100.0, 0.0, 0.0));

		//mehdi - manual routing

		//  //get channel of the device
		//  Ptr<Channel> ch = routerDevice->GetChannel();
		Ptr<NetDevice> device;
		Ptr<ns3::ndn::Face> face;
		Ptr<ndn::Fib> fib;
		Ptr<ndn::fib::Entry> fibEntry;

		for (vector<Ptr<MehdiNode> >::iterator it = mehdiNodes.begin(); it != mehdiNodes.end(); it++)
		{
			device = (*it)->GetDevice(0);
			face = (*it)->GetObject<ndn::L3Protocol>()->GetFaceByNetDevice(device);
			fib = (*it)->GetObject<ndn::Fib> ();
			fibEntry = fib->Add (prefix, face, 0);
		}

		Simulator::Stop (Seconds (20*1000*1000));

		Simulator::Run ();
		Simulator::Destroy ();

		return true;
	}

	/*
	 *
	 */
	static bool Start(int argc, char *argv[])
	{
		NodeContainer nodes;
		vector<Ptr<MehdiNode> > mehdiNodes;

		CommandLine cmd;

		cmd.AddValue("verbose", "log to console", Report::verbose);
		cmd.AddValue("detailed", "create detailed log", Report::detailed_log);

		cmd.AddValue("other", "other simulation", Parameters::other_simulation);
		cmd.AddValue("exact", "exact location", Parameters::exact_location);
		cmd.AddValue("s", "smart propagation", Parameters::smart_propagation);
		cmd.AddValue("rn", "real next locations" , Parameters::real_next_locations);
		cmd.AddValue("r", "name of report file", Report::report_file_name);
		cmd.AddValue("autoname", "makes report file name itself", Report::auto_name );

		cmd.AddValue("nc", "number of contents", Parameters::n_contents);
		cmd.AddValue("sc", "size of cache", Parameters::cache_size);
		//retransmissions
		cmd.AddValue("nsr", "number of retransmissions in non-smart mode", Parameters::max_non_smart_retransmit);
		cmd.AddValue("snr", "number of retransmissions in normal smart mode", Parameters::max_smart_normal_retransmit);
		cmd.AddValue("sfr", "number of retransmissions in force forwarding mode", Parameters::max_smart_force_forward_retransmit);

		cmd.Parse(argc, argv);

		if (Parameters::other_simulation)
			Parameters::smart_propagation = false;

		if (Report::report_file_name == "" && !Report::auto_name)
		{
			cout << "please specify the report file." << endl;
			exit(-1);
		}

		//initialize the report
		Report::Init();

		//read the users who have data between simulation starting time and simulation stopping time
		if (!Preparation::ReadBinaryData2(mehdiNodes, mehdi_simulation_starting_time, mehdi_simulation_starting_time + mehdi_simulation_duration))
			exit(-1);

		MEHDI_REPORT_LINE("number of users: " << mehdiNodes.size());

		//install mobility model
		ScheduleMovements(mehdiNodes);

		//add the prepared mehdi nodes to the container
		for (vector<Ptr<MehdiNode> >::iterator it = mehdiNodes.begin(); it != mehdiNodes.end(); it++)
			nodes.Add(*it);

		//prepare wireless
		ndn::mehdi::Preparation::PrepareWireless(nodes);

		// Install NDN stack on all nodes
		ndn::StackHelper ndnHelper;

		//set cache size
		ndnHelper.SetContentStore("ns3::ndn::cs::Lru", "MaxSize", Parameters::cache_size);
//		ndnHelper.SetContentStore("ns3::ndn::cs::Nocache");

		//install forwarding
		//basic forwarding (flooding)
		ndnHelper.SetForwardingStrategy("ns3::ndn::mehdi::MehdiForwardingBase");
		ndnHelper.InstallAll ();

		// Installing applications

		//contents
		vector<std::string> prefixes;
		//initialize
		for (uint i = 0; i < Parameters::n_contents; i++)
		{
			stringstream ss;
			ss << "/prefix" << i + 1;
			prefixes.push_back(ss.str());
		}

		// producers

		/* initialize random seed: */
		Tools::Randomize();

		//select random producers using power law distribution
		//and install producer app on producers
		map<string,uint> producer_id_of_prefix;

		ndn::AppHelper producerHelper ("ns3::ndn::Producer");
		producerHelper.SetAttribute ("PayloadSize", StringValue("1"));

		for (uint i = 0; i < prefixes.size(); i++ )
		{
			// Producer will reply to all requests starting with this prefix
			producerHelper.SetPrefix (prefixes[i]);
			//select a random producer
			uint producer_id = Tools::Rand_PowerLaw(0, mehdiNodes.size());
			//register the id of producer for this prefix
			producer_id_of_prefix[prefixes[i]] = producer_id;
			//install producer app
			producerHelper.Install (mehdiNodes[producer_id]);
		}

		//consumers
		CalculateRequestTimes(mehdiNodes, prefixes);
		ndn::AppHelper consumerHelper ("ns3::ndn::mehdi::MehdiConsumer");
		for (uint i = 0; i < nodes.size(); i++)
			consumerHelper.Install(mehdiNodes[i]);

		//mehdi - manual routing

		//  //get channel of the device
		//  Ptr<Channel> ch = routerDevice->GetChannel();
		Ptr<NetDevice> device;
		Ptr<ns3::ndn::Face> face;
		Ptr<ndn::Fib> fib;
		Ptr<ndn::fib::Entry> fibEntry;

		for (uint i = 0; i < mehdiNodes.size(); i++)
		{
			device = mehdiNodes[i]->GetDevice(0);
			face = mehdiNodes[i]->GetObject<ndn::L3Protocol>()->GetFaceByNetDevice(device);
			fib = mehdiNodes[i]->GetObject<ndn::Fib> ();
			//add routing for all prefixes
			for (uint j = 0; j < prefixes.size(); j++)
				//except in the producer of the prefix
				if (i != producer_id_of_prefix[prefixes[j]])
					fibEntry = fib->Add (prefixes[j], face, 0);
		}

		Simulator::Stop (Seconds (mehdi_simulation_duration));

		try
		{
			Simulator::Run ();

			//final report
			Report::ReportPacketHappening("finished", "", "", "", Simulator::Now().GetSeconds(), true);

			Simulator::Destroy ();
		}
		catch(exception& e)
		{
			MEHDI_REPORT_LINE("exception occurred while running: " << e.what());
		}

		Report::Finalize();

		return true;
	}

};//class Preparation

}//namespace mehdi
}//namespace ndn
}//namespace ns3



#endif /* SRC_NDNSIM_MODEL_MEHDI_NDN_MEHDI_PREPARATION_H_ */
