/*
 * ndn-mehdi-report.h
 *
 *  Created on: May 5, 2016
 *      Author: mehdi
 */

#ifndef NDN_MEHDI_REPORT_H_
#define NDN_MEHDI_REPORT_H_

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include "src/ndnSIM/mehdi/ndn-mehdi-basic-definitions.h"

namespace ns3{
namespace ndn{
namespace mehdi{

#define MEHDI_REPORT(message)				\
	do										\
	{										\
		std::stringstream ss;				\
		ss << message;						\
		ndn::mehdi::Report::SimpleReport(ss.str());\
	}										\
	while (false)

#define MEHDI_REPORT_LINE(message)				\
	do										\
	{										\
		std::stringstream ss;				\
		ss << message;						\
		ndn::mehdi::Report::ReportLine(ss.str());\
	}										\
	while (false)

#define MEHDI_ERROR_LINE(message)				\
	do										\
	{										\
		std::stringstream ss;				\
		ss << message;						\
		ndn::mehdi::Report::ErrorLine(ss.str());	\
	}										\
	while (false)

#define MEHDI_FATAL_ERROR(message, code)				\
	do													\
	{													\
		std::stringstream ss;							\
		ss << message;									\
		ndn::mehdi::Report::ErrorLine(ss.str(), true, code);	\
	}													\
	while (false)

using namespace std;

class Report
{
	static uint32_t n_errors;
	const static int max_n_errors = 100000;
	const static bool exit_if_max_error_occurred = true;
public:

	static uint64_t n_interest_packets;
	static uint64_t n_data_packets;
	static uint64_t n_requests;
	static uint64_t n_successful_requests;
	static uint64_t n_int;
	static uint64_t n_ack;
	static uint64_t n_cmd;
	static uint64_t n_dropped_cmd;
	static uint64_t n_dropped_ack;
	static string report_file_name;
	static string original_report_file_name;
	static time_t starting_time;
	static time_t ending_time;
	static bool verbose;
	static bool detailed_log;
	static bool auto_name;


	/*
	 *
	 */
	static void Init();

	/*
	 *
	 */
	static void IncrementErrorCounter();

	/*
	 * reports a message
	 */
	static void SimpleReport(string message);

	/*
	 * reports a message
	 */
	static void ReportLine(string message);

	/*
	 * reports an error
	 */
	static void ErrorLine(string message, bool should_exit = false, int exit_code = -1);

	/*
	 *
	 */
	static void ReportPacketHappening(string happening, string sender, string receiver, string prefix, elapsed_time time, bool ignore_counter = false);

	/*
	 *
	 */
	static void Finalize();
};//class Report

}//namespace mehdi
}//namespace ndn
}//namespace ns3

#endif /* NDN_MEHDI_REPORT_H_ */
