/*
 * ndn-mehdi-basic-definitions.cc
 *
 *  Created on: May 12, 2016
 *      Author: mehdi
 */

#include "src/ndnSIM/mehdi/ndn-mehdi-basic-definitions.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-centroid.h"

namespace ns3 {
namespace ndn {
namespace mehdi {

using namespace std;

bool Parameters::other_simulation = 0;
bool Parameters::exact_location = 0;
bool Parameters::smart_propagation = 1;
bool Parameters::real_next_locations = 0;

unsigned int Parameters::n_contents = 1000;

string Parameters::cache_size = "30";

unsigned int Parameters::max_non_smart_retransmit = 1;
unsigned int Parameters::max_smart_normal_retransmit = 1;
unsigned int Parameters::max_smart_force_forward_retransmit = 1;

}//namespace mehdi
}//namespace ndn
}//namespace ns3



