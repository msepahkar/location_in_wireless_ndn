/*
 * nsn-mehdi-tools.h
 *
 *  Created on: Jan 29, 2016
 *      Author: mehdi
 */

#ifndef NDN_MEHDI_TOOLS_H_
#define NDN_MEHDI_TOOLS_H_

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <unistd.h>
#include <ctime>
#include <ios>
#include <string>
#include <math.h>
#include "src/ndnSIM/mehdi/ndn-mehdi-report.h"

namespace ns3{
namespace ndn{
namespace mehdi{

#define noop

using namespace std;

class Tools{

public:

	//////////////////////////////////////////////////////////////////////////////
	//
	// process_mem_usage(double &, double &) - takes two doubles by reference,
	// attempts to read the system-dependent data for a process' virtual memory
	// size and resident set size, and return the results in KB.
	//
	// On failure, returns 0.0, 0.0

	static double VirtualMemUsage()
	{
	   using std::ios_base;
	   using std::ifstream;
	   using std::string;

	   // 'file' stat seems to give the most reliable results
	   //
	   ifstream stat_stream("/proc/self/stat",ios_base::in);

	   // dummy vars for leading entries in stat that we don't care about
	   //
	   string pid, comm, state, ppid, pgrp, session, tty_nr;
	   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
	   string utime, stime, cutime, cstime, priority, nice;
	   string O, itrealvalue, starttime;

	   // the two fields we want
	   //
	   unsigned long vsize;
	   long rss;

	   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
	               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
	               >> utime >> stime >> cutime >> cstime >> priority >> nice
	               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

	   stat_stream.close();

	   return vsize / 1024.0;
	}

	/*
	 *
	 */
	static double ResidentMemUsage()
	{
	   using std::ios_base;
	   using std::ifstream;
	   using std::string;

	   // 'file' stat seems to give the most reliable results
	   //
	   ifstream stat_stream("/proc/self/stat",ios_base::in);

	   // dummy vars for leading entries in stat that we don't care about
	   //
	   string pid, comm, state, ppid, pgrp, session, tty_nr;
	   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
	   string utime, stime, cutime, cstime, priority, nice;
	   string O, itrealvalue, starttime;

	   // the two fields we want
	   //
	   unsigned long vsize;
	   long rss;

	   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
	               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
	               >> utime >> stime >> cutime >> cstime >> priority >> nice
	               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

	   stat_stream.close();

	   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages

	   return rss * page_size_kb;
	}

	/*
	 * randomizing the seed using time and process id
	 */
	static void Randomize()
	{
		long s, seed, pid, seconds;

		pid = getpid();
		s = ::time ( &seconds ); /* get CPU seconds since 01/01/1970 */
		seed = abs(((s*181)*((pid-83)*359))%104729);
		srand(seed);
	}

	/*
	 *
	 */
	static int Rand_PowerLaw(int x0, int x1)
	{
		double n = 2.1;
		double y = ((double) rand() / (RAND_MAX));
		double d1 = (pow(x1,n+1) - pow(x0,n+1))*y;
		double d2 = pow(x0,n+1);
		return (int)pow(d1 + d2, 1/(n+1));
	}

	/*
	 *
	 */
	static string GetDateTime()
	{
		time_t t = ::time(0);   // get time now
		string weekdays[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

		struct tm * now = localtime( & t );

		stringstream ss;
		ss << weekdays[now->tm_wday] << ", "
			<< now->tm_year + 1900 << '-'
			<< now->tm_mon + 1 << '-'
			<< now->tm_mday << ", "
			<< now->tm_hour << ':'
			<< now->tm_min << ':'
			<< now->tm_sec;

		return ss.str();
	}

	/*
	 * removes the firest occurance of sub_s from main_s
	 * note that it modifies the main_s
	 */
	static bool RemoveSubstring(string& main_s, string sub_s)
	{
		//find the first occurance of sub_s in main_s
		size_t index = main_s.find(sub_s,0);

		//if not found
		if (index == string::npos)
			return false;

		//remove it if found
		main_s.replace(index, sub_s.size(), "");

		//found and removed
		return true;
	}

	/*
	 * writing a data of type T to the binary file
	 * out_stream should be of reference type because buffers will be modified
	 */
	template<typename T>
	static bool WriteDataToBinaryFile(const T data, ofstream& out_stream)
	{
		try
		{
			out_stream.write((char *)&data, sizeof(T));
		}
		catch(exception& e)
		{
			MEHDI_ERROR_LINE("Error in writing data to binary file." << endl << e.what());
			return false;
		}
		return true;
	}

	/*
	 * reading a data of type T from a binary file
	 * in_stream should be of reference type because buffers will be modified.
	 * obviously data should be of type reference too.
	 */
	template<typename T>
	static bool ReadDataFromBinaryFile(T& data, ifstream& in_stream)
	{
		try
		{
			in_stream.read((char *)&data, sizeof(T));
		}
		catch(exception& e)
		{
			MEHDI_ERROR_LINE("Error in reading data from binary file." << endl << e.what());
			return false;
		}
		return true;
	}

	/*
	 * writing a string to a binary file
	 * it makes use of the WriteVectorToBinaryFile function
	 */
	static bool WriteStringToBinaryFile(const string s, ofstream& out_stream)
	{
		if (!WriteVectorToBinaryFile(vector<char>(s.begin(), s.end()), out_stream))
			return false;
		return true;
	}

	/*
	 * reads a string from a binary file.
	 * it makes use of ReadVectorFromBinaryFile function.
	 * obviously string parameter should be of type reference.
	 */
	static bool ReadStringFromBinaryFile(string& s, ifstream& in_stream)
	{
		//empty the string
		s = "";
		//the string will be read as a vector of char
		vector<char>v;
		if(!ReadVectorFromBinaryFile(v, in_stream))
			return false;
		//convert vector of char to string
		s = string(v.begin(), v.end());

		//all done
		return true;
	}

	/*
	 * writes a vector of type T to the binary file
	 * it first writes the size of the vector to the file so that
	 *  at the time of reading we can conclude how many items are
	 *  written in the file.
	 *
	 * IMPORTANT NOTE:
	 *  if type T is class, the type of its members should be of simple types,
	 *  i.e. no vector or other complex types are allowed.
	 *  complex types should be written and read separately.
	 */
	template<typename T>
	static bool WriteVectorToBinaryFile(const vector<T> v, ofstream& out_stream)
	{
		try
		{
			size_t size = v.size();
			//first write the size of vector
			out_stream.write((char *)&size, sizeof(size_t));
			//now write the items one by one
			for (typename vector<T>::const_iterator it = v.begin(); it != v.end(); it++)
				out_stream.write((char *)&(*it), sizeof(T));
		}
		catch(exception& e)
		{
			MEHDI_ERROR_LINE("Error in writing vector to binary file." << endl << e.what());
			return false;
		}

		//all done
		return true;
	}

	/*
	 * reads a vector from the binary file.
	 * first the number of items written in the file is read.
	 * note that the vector parameter should be of reference type.
	 *
	 * IMPORTANT NOTE:
	 *  if type T is class, the type of its members should be of simple types,
	 *  i.e. no vector or other complex types are allowed.
	 *  complex types should be written and read separately.
	 */
	template<typename T>
	static bool ReadVectorFromBinaryFile(vector<T>& v, ifstream& in_stream)
	{
		try
		{
			size_t size;
			//first read the number of items
			in_stream.read((char *)&size, sizeof(size_t));
			//empty the vector
			v.clear();
			//now read the elements one by one
			for (size_t i = 0; i < size; i++)
			{
				//first push an empty item to the vector
				v.push_back(T());
				//now fill the empty element with the read data
				in_stream.read((char *)&(v.back()), sizeof(T));
			}
		}
		catch(exception& e)
		{
			MEHDI_ERROR_LINE("Error in writing vector to binary file." << endl << e.what());
			return false;
		}

		//all done
		return true;
	}

//
//	/*
//	 * writes a map of type T1 and T2 to a binary file.
//	 */
//	template<typename T1, typename T2>
//	static bool WriteMapToBinaryFile(const map<T1, T2> m, ofstream& out_stream)
//	{
//		try
//		{
//			size_t size = m.size();
//			//first write the size of map
//			out_stream.write((char *)&size, sizeof(size_t));
//			//now write items one by one
//			for (typename map<T1, T2>::iterator it = m.begin(); it != m.end(); it++)
//			{
//				//first the key
//				out_stream.write((char *)&(it->first), sizeof(T1));
//				//then the data
//				out_stream.write((char *)&(it->second), sizeof(T2));
//			}
//		}
//		catch(exception& e)
//		{
//			MEHDI_ERROR("Error in writing map to binary file." << endl << e.what());
//			return false;
//		}
//
//		//all done
//		return true;
//	}
//
//	/*
//	 * reads a map of type T1 and T2 from a binary file
//	 * obviously the map parameter should be of reference type
//	 */
//	template<typename T1, typename T2>
//	static bool ReadMapFromBinaryFile(map<T1, T2>& m, ifstream& in_stream)
//	{
//		try
//		{
//			size_t size;
//			//first read the number of items
//			in_stream.read((char *)&size, sizeof(size_t));
//			//empty the map
//			m.clear();
//			T1 key;
//			T2 value;
//			//read items one by one
//			for (size_t i = 0; i < size; i++)
//			{
//				//first the key
//				in_stream.read((char *)&key, sizeof(T1));
//				//then the value
//				in_stream.read((char *)&value, sizeof(T2));
//				//add the pair to the map
//				m[key] = value;
//			}
//		}
//		catch(exception& e)
//		{
//			MEHDI_ERROR("Error in writing vector to binary file." << endl << e.what());
//			return false;
//		}
//
//		//all done
//		return true;
//	}
//
};//class Tools
}//namespace mehdi
}//namespace ndn
}//namespace ns3




#endif /* SRC_NDNSIM_MODEL_MEHDI_NSN_MEHDI_TOOLS_H_ */
