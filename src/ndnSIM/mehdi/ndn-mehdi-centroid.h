/*
 * ndn-mehdi-centroid.h
 *
 *  Created on: Jan 19, 2016
 *      Author: mehdi
 */

#ifndef NDN_MEHDI_CENTROID_H_
#define NDN_MEHDI_CENTROID_H_

#include <string>
#include <algorithm>
#include <stdint.h>
#include <iostream>
#include "src/ndnSIM/mehdi/ndn-mehdi-tools.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-position_interval_call.h"

namespace ns3 {
namespace ndn {
namespace mehdi {

using namespace std;

const static coordinate POI_merging_threshold = 30;

//used for reading centroids from binary file
const string READING_FROM_BINARY_FILE = "reading from binary file";
//used for finding POIs
const string HYPOTHETICAL_CENTORID_NAME = "hypothetical centroid";
const string EMPTY_CENTROID_NAME = "empty centroid";
const string INVALID_CENTROID_NAME = "invalid centroid";

enum CentroidType {
	POI, ZOI, ROI
};

//////////////////////////////
// class Centroid
/////////////////////////////
class Centroid {

public:
bool operator< (const Centroid& c2) const
{
    return !IsEqual(c2);
}


private:
	string m_name;
	int64_t m_sum_of_x;
	int64_t m_sum_of_y;
	size_t m_number_of_members;
	uint m_radius;
	vector<Position> m_positions;	//positions belonging to this centroid
	vector<Interval> m_intervals;//intervals in which the user has been in this centroid
	vector<Call> m_calls;//calls happended while the user has been in this centroid
//	map<string, uint32_t> m_totalPredictions_markovWithCalls;//the key element is the callee name
//	map<string, uint32_t> m_truePredictions_markovWithCalls;//the key element is the callee name
//	map<pair<CentroidType, string>, uint32_t> m_totalPredictions_XOIchains;	//the key is the pair of centroid type and callee name
//	map<pair<CentroidType, string>, uint32_t> m_truePredictions_XOIchains;//the key is the pair of centroid type and callee name

	enum SerializingItems{NAME, SUM_OF_X, SUM_OF_Y, NUMBER_OF_MEMBERS, RADIUS, POSITIONS, INTERVALS, CALLS};
	vector<SerializingItems> m_serializing_items;

public:
	uint32_t totalPredictions;
	uint32_t truePredictions;
	map<Centroid, uint> number_of_times_occured_as_destination;

public:
	Centroid(string name, uint32_t radius) {
		m_name = name;

		//we have positions and we can find sum of x and sum of y and number of members from that array
		//but for performance we keep these data in separate variables so that we do not have to update them
		//every time we want to use them
		//it should be noticed that m_positions array should be ONLY internally updated because its change will affect all of these variables

		m_sum_of_x = 0;
		m_sum_of_y = 0;
		m_number_of_members = 0;

		m_radius = radius;

		totalPredictions = 0;
		truePredictions = 0;

		m_serializing_items.push_back(NAME);
		m_serializing_items.push_back(SUM_OF_X);
		m_serializing_items.push_back(SUM_OF_Y);
		m_serializing_items.push_back(NUMBER_OF_MEMBERS);
		m_serializing_items.push_back(RADIUS);
		m_serializing_items.push_back(POSITIONS);
		m_serializing_items.push_back(INTERVALS);
		m_serializing_items.push_back(CALLS);
	}

	/*
	 *
	 */
	static Centroid FindCentroidOfPosition(vector<Centroid> centroids, Position position)
	{
		for (vector<Centroid>::iterator it = centroids.begin(); it != centroids.end(); it++)
			if (it->IsPositionInside(position))
				return *it;
		return Centroid(INVALID_CENTROID_NAME, 0);
	}

	/*
	 *
	 */
	static vector<Centroid>::iterator FindCentroidOfPositionIterator(vector<Centroid>& centroids, Position position)
	{
		for (vector<Centroid>::iterator it = centroids.begin(); it != centroids.end(); it++)
			if (it->IsPositionInside(position))
				return it;
		return centroids.end();
	}

	/*
	 * find pOIs using the passed positions vector.
	 * pOIs are places of interest before merging them.
	 */
	static vector<Centroid> Find_pOIs(vector<Position> positions, u_coordinate pOI_distance_threshold /*50*/, elapsed_time pOI_time_threshold /*600 sec*/)
	{
		vector<Centroid> pOIs;

		//any positions available?
		if (positions.size() > 0)
		{
			//create a hypothetical pOI with the first position
			Centroid pOI = Centroid(EMPTY_CENTROID_NAME, pOI_distance_threshold);
			pOI.AddPosition(positions[0]);

			//we have not found a true pOI yet
			bool in_pOI = false;
			//zero seconds spent in pOI so far
			elapsed_time time_in_pOI = 0;

			//go over all positions (beginning from the second one)
			for (size_t i = 1; i < positions.size(); i++)
			{
				//pick a position
				Position new_position = positions[i];
				//is it near enough to the previous position in the pOI
				if (Position::Distance(pOI.GetLastAddedPosition().GetX(), pOI.GetLastAddedPosition().GetY(), new_position.GetX(), new_position.GetY()) <= pOI_distance_threshold)
				{
					//increase the time in pOI
					time_in_pOI += new_position.GetTime() - pOI.GetLastAddedPosition().GetTime();
					//add the new position to the pOI
					pOI.AddPosition(new_position);
					//if the pOI is not real and the duration of pOI is long enough make it real
					if (!in_pOI && time_in_pOI >= pOI_time_threshold)
					{
						pOIs.push_back(pOI);
						in_pOI = true;
					}
				}
				//far position
				else
				{
					//end of real pOI
					in_pOI = false;
					//create another hypothetical pOI
					pOI = Centroid(EMPTY_CENTROID_NAME, pOI_distance_threshold);
					pOI.AddPosition(new_position);
				}
			}
			//name the found pOIs
			uint pOI_number = 0;
			for (vector<Centroid>::iterator it = pOIs.begin(); it != pOIs.end(); it++)
			{
				stringstream ss;
				ss << "pOI " << ++pOI_number;
				it->SetName(ss.str());
//				cout << it->GetName() << " number of members: " << it->GetNumberOfMembers() << ", pOI radius: " << it->GetRadius() << endl;
			}
		}
		return pOIs;
	}

	/*
	 *
	 */
	static void MergeCentroids(vector<Centroid>& centroids, coordinate merging_threshold)
	{
		size_t i = 0;
		//because merged POIs will be deleted from array we cannot use the usual for loop
		size_t n = centroids.size();
		if (n == 0)
			return;
		//while not the one before last reached
		while (i < n - 1)
		{
			//start from the next centroid
			size_t j = i + 1;
			//while not end of array reached
			while (j < n)
			{
				//how much far are the centers?
				coordinate distance = Position::Distance(centroids[i].GetX(), centroids[i].GetY(), centroids[j].GetX(), centroids[j].GetY());
				//either if the two POIs near enough or if they have intersection?
				if (distance <= merging_threshold)// || distance < centroids[i].GetRadius() + centroids[j].GetRadius())
				{
					//merge them
					centroids[i].MergeWithCentroid(centroids[j]);
					//delete the second one
					centroids.erase(centroids.begin() + j);
					//decrease the length of the array
					n -= 1;
				}
				else
					//next second centroid
					j += 1;
			}
			//next first centroid
			i += 1;
		}
	}

	/*
	 *
	 */
	static vector<Centroid> Find_POIs(vector<vector<Centroid> > v_pOIs, vector<vector<Position> > v_positions)
	{
		//flatten the array of pOIs into POIs
		vector<Centroid> POIs;
		for (vector<vector<Centroid> >::iterator it = v_pOIs.begin(); it != v_pOIs.end(); it++)
			POIs.insert(POIs.end(), it->begin(), it->end());

		//flatten the array of positions
		vector<Position> positions;
		for (vector<vector<Position> >::iterator it = v_positions.begin(); it != v_positions.end(); it++)
			positions.insert(positions.end(), it->begin(), it->end());

		//merge POIs as much as possible
		while (true)
		{
			//keep the number of POIs before merge
			size_t number_of_POIs_before_merge = POIs.size();
			//merge POIs
			MergeCentroids(POIs, POI_merging_threshold);
			//no more merging? break the loop
			if (number_of_POIs_before_merge == POIs.size())
				break;
		}

		//update positions and name the POIs
		uint POI_number = 0;

		for (vector<Centroid>::iterator it = POIs.begin(); it != POIs.end(); it++)
		{
//			//update positions
//			if not sourcePOI.updatePositions(positions, Assets.POINumberOfMembersThreshold):
//				del POIs[i]
//				continue
			//set the name
			POI_number += 1;
			stringstream ss;
			ss << "POI " << POI_number;
			it->SetName(ss.str());
//			cout << "*" << it->GetName() << " number of members: " << it->GetNumberOfMembers() << ", radius: " << it->GetRadius() << endl;
		}

		//initialize the destinations
		vector<Centroid> destination_POIs = POIs;
		for ( vector<Centroid>::iterator it = POIs.begin(); it != POIs.end(); it++)
		{
			it->number_of_times_occured_as_destination.clear();
			for (vector<Centroid>::iterator it2 = destination_POIs.begin(); it2 != destination_POIs.end(); it2++)
				it->number_of_times_occured_as_destination[*it2] = 0;
		}

		//find first position present in some POI
		uint first_position_index = -1;
		vector<Centroid>::iterator first_POI = POIs.end();
		for (uint i = 0; i < positions.size(); i++)
		{
			first_POI = Centroid::FindCentroidOfPositionIterator(POIs, positions[i]);
			if (first_POI != POIs.end())
			{
				first_position_index = i;
				break;
			}
		}
		//continue with other positions
		for (uint i = first_position_index + 1; i < positions.size(); i++)
		{
			vector<Centroid>::iterator next_POI = Centroid::FindCentroidOfPositionIterator(POIs, positions[i]);
			if (next_POI != POIs.end() && next_POI != first_POI)
			{
				first_POI->number_of_times_occured_as_destination[*next_POI]++;
				first_POI = next_POI;
			}
		}

		return POIs;
	}

	/*
	 *
	 */
	Position GetLastAddedPosition()
	{
		return m_positions.back();
	}

	/*
	 * sets the name of centroid
	 */
	void SetName(string name)
	{
		m_name = name;
	}

	/*
	 * add a position to the centroid
	 */
	bool AddPosition(Position position)
	{
		//check the validity of position
		if (position.GetX() == INVALID_COORDINATE || position.GetY() == INVALID_COORDINATE || position.GetTime() == INVALID_TIME)
		{
			MEHDI_ERROR_LINE("invalid position for adding to the centroid.");
			return false;
		}
		//add the position and update the required parameters
		m_positions.push_back(position);
		m_sum_of_x += position.GetX();
		m_sum_of_y += position.GetY();
		m_number_of_members += 1;

		//now time to update intervals

		//find the most proper interval and extend its times if required
		bool intervalFound = false;
		//sort intervals based on entering time in reverse
		sort(m_intervals.begin(), m_intervals.end(), Interval::ReverseCompare());

		//traverse reversely sorted intervals but always keep the previous element too
		vector<Interval>::iterator previous_it, it;
		for (previous_it = it = m_intervals.begin(); it != m_intervals.end(); previous_it = it, it++)
			//find the first interval whose entering time is not later than the position time
			if (it->GetEnteringTime() <= position.GetTime()) {
				//now check:
				//if position time also is less than or equal to the leaving time of the
				//  interval then position belongs to this interval and nothing else
				//  is needed to be done.
				//otherwise if the position time is bigger than the leaving time of the interval
				//  extend either the leaving time of this interval or the entering time of
				//  the previous interval. (whichever whose difference with the position
				//  time is less)
				if (position.GetTime() > it->GetLeavingTime())
				{
					//if there is either no previous interval or this interval is better to be extended
					if ((previous_it == it) || ((position.GetTime() - it->GetLeavingTime())	<= (previous_it->GetEnteringTime() - position.GetTime())))
						//extend this interval
						it->SetLeavingTime(position.GetTime());
					else
						// extend the previous interval
						previous_it->SetEnteringTime(position.GetTime());
				}
				//TODO: extending one interval may need merging two adjacent intervals
				//in any of these cases the interval is found
				intervalFound = true;
				break;
			}
		//if no interval is found, a new one should be created and added to intervals vector
		if (!intervalFound)
			m_intervals.push_back(Interval(position.GetTime(), position.GetTime())); //entering and leaving times are the same
		return true;
	}

	/*
	 * gets the name of the centroid
	 */
	string GetName() const
	{
		return m_name;
	}

	/*
	 * get x coordinate of the center of the centroid
	 */
	coordinate GetX() const
	{
		//no member?
		if (m_number_of_members == 0)
		{
			//no center
			MEHDI_ERROR_LINE("zero number of members in centroid while trying to get the x of centroid.");
			return INVALID_COORDINATE;
		}
		//center's x is average of all members' x
		return m_sum_of_x / (int64_t)m_number_of_members;
	}

	/*
	 * get y coordinate of the center of the centroid
	 */
	coordinate GetY() const
	{
		//no member?
		if (m_number_of_members == 0)
		{
			//no center
			MEHDI_ERROR_LINE("zero number of members in centroid while trying to get the y of centroid.");
			return INVALID_COORDINATE;
		}
		//center's y is the average of all member's y
		return m_sum_of_y / (int64_t)m_number_of_members;
	}

	/*
	 * gets the radius of the centroid
	 */
	uint32_t GetRadius() const
	{
		return m_radius;
	}

	/*
	 * gets sum of x of centroid
	 */
	int64_t GetSumOfX() const
	{
		return m_sum_of_x;
	}

	/*
	 * gets som of y of centroid
	 */
	int64_t GetSumOfY() const
	{
		return m_sum_of_y;
	}

	/*
	 * gets number of members
	 */
	uint32_t GetNumberOfMembers() const
	{
		return m_number_of_members;
	}

	/*
	 * makes a copy of position vector and returns it.
	 * making copy is not required but it is just done for more confidence
	 */
	vector<Position> GetPositions() const
	{
		vector<Position> v = m_positions;
		return v;
	}

	/*
	 * makes a copy of intervals vector and returns it.
	 * making copy is not required but it is just done for more confidence
	 */
	vector<Interval> GetIntervals() const
	{
		vector<Interval> v = m_intervals;
		return v;
	}

	/*
	 * clears all calls
	 */
	void ResetCalls()
	{
		m_calls.clear();
	}

	/*
	 * adds a call to the call vector
	 */
	void AddCall(Call call)
	{
		m_calls.push_back(call);
	}

	/*
	 * makes a copy of calls vector and returns it
	 * making copy is not required but just it is done for more confidence
	 */
	vector<Call> GetCalls() const
	{
		vector<Call> v = m_calls;
		return v;
	}

	/*
	 * entering time to the centroid (if "now" is in centroid)
	 */
	elapsed_time GetEnteringTime(elapsed_time now)
	{
		//check all intervals
		for (vector<Interval>::iterator it = m_intervals.begin(); it != m_intervals.end(); it++)
			//is "now" in any interval?
			if (it->IncludesTime(now))
				//return the entering time of that interval
				return it->GetEnteringTime();
		//return an invalid time
		return LAST_TIME;
	}

	/*
	 * leaving time from the centroid (if "now" is in centroid)
	 */
	elapsed_time GetLeavingTime(elapsed_time now)
	{
		//check all intervals
		for (vector<Interval>::iterator it = m_intervals.begin(); it != m_intervals.end(); it++)
			//does any interval include "now"?
			if (it->IncludesTime(now))
				//return leaving time of that interval
				return it->GetLeavingTime();
		//return an invalid time
		return FIRST_TIME;
	}

	/*
	 * accuracy of pure markov prediction
	 */
	double Accuracy_PureMarkov() {
		if (totalPredictions == 0)
			return 0;
		return truePredictions / totalPredictions;
	}

//	/*
//	 * accuracy of markov with calls
//	 */
//	double Accuracy_MarkovWithCalls(string callee) {
//		//callee not in the map?
//		if (m_totalPredictions_markovWithCalls.find(callee)
//				== m_totalPredictions_markovWithCalls.end())
//			return 0;
//		//no predictions made?
//		if (m_totalPredictions_markovWithCalls[callee] == 0)
//			return 0;
//		return m_truePredictions_markovWithCalls[callee]
//				/ m_totalPredictions_markovWithCalls[callee];
//	}
//
//	/*
//	 * accuracy of XOI chains
//	 */
//	double Accuracy_XOI_chains(CentroidType type, string callee) {
//		pair<CentroidType, string> key(type, callee);
//		//callee not in map?
//		if (m_totalPredictions_XOIchains.find(key)
//				== m_totalPredictions_XOIchains.end())
//			return 0;
//		//no prediction made?
//		if (m_totalPredictions_XOIchains[key] == 0)
//			return 0;
//		return m_truePredictions_XOIchains[key]
//				/ m_totalPredictions_XOIchains[key];
//	}
//

	/*
	 * maximim duration of stay in the centroid
	 */
	elapsed_time MaximumDuration()
	{
		elapsed_time maximumDuration = 0;

		//find the maximum duration
		for (vector<Interval>::iterator it = m_intervals.begin(); it != m_intervals.end(); it++)
			if (it->Duration() > maximumDuration)
				maximumDuration = it->Duration();

		return maximumDuration;
	}

	/*
	 * check if the time "t" is in any interval of this centroid
	 */
	bool IncludesTime(elapsed_time t)
	{
		//check all intervals
		for (vector<Interval>::iterator it = m_intervals.begin(); it != m_intervals.end(); it++)
			if (it->IncludesTime(t))
				return true;
		return false;
	}

	/*
	 * checks if the two times "t1" and "t2" are in the same interval
	 */
	bool HasInSameInterval(elapsed_time t1, elapsed_time t2)
	{
		//check all intervals
		for (vector<Interval>::iterator it = m_intervals.begin(); it != m_intervals.end(); it++)
			if (it->IncludesTime(t1) && it->IncludesTime(t2))
				return true;
		return false;
	}

	/*
	 * check if the passed position is inside this centroid
	 */
	bool IsPositionInside(Position position)
	{
		if (m_number_of_members > 0)
			if (Position::Distance(this->GetX(), this->GetY(), position.GetX(), position.GetY()) <= m_radius)
				return true;
		return false;
	}

	/*
	 * like KVM this function should be repeated until no further changes are made.
	 * returns false if the number of positions which are still in this centroid are less than threshold
	 */
	bool UpdatePositions(vector<Position> positions, uint32_t threshold)
	{
		//new parameters for positions
		vector<Position> newPositions;
		uint64_t new_sum_of_x = 0;
		uint64_t new_sum_of_y = 0;
		//check all positions to see if they belong to this centroid
		for (vector<Position>::iterator it = positions.begin(); it != positions.end(); it++)
		{
			//if yes, add the poisition information to the new parameters
			if (this->IsPositionInside(*it))
			{
				newPositions.push_back(*it);
				new_sum_of_x += it->GetX();
				new_sum_of_y += it->GetY();
			}
		}
		//update the position parameters
		m_number_of_members = newPositions.size();
		m_sum_of_x = new_sum_of_x;
		m_sum_of_y = new_sum_of_y;
		//not enough number of positions available?
		if (newPositions.size() < threshold)
			//this centroid is not worth keeping
			return false;
		return true;
	}

	/*
	 * merges this centroid with the passed centroid
	 */
	void MergeWithCentroid(Centroid& otherCentroid)
	{
		//add sum of x
		m_sum_of_x += otherCentroid.GetSumOfX();
		//add sum of y
		m_sum_of_y += otherCentroid.GetSumOfY();
		//add number of members
		m_number_of_members += otherCentroid.GetNumberOfMembers();
		//BEGIN: new radius
		//distance between two centers
		u_coordinate distance = Position::Distance(this->GetX(), this->GetY(), otherCentroid.GetX(), otherCentroid.GetY());
		//the smaller radius
		uint32_t minRadius = min(m_radius, otherCentroid.GetRadius());
		//the bigger radius
		uint32_t maxRadius = max(m_radius, otherCentroid.GetRadius());
		//are the centers outside each other?
		if (distance >= maxRadius)
			//add two radiuses and the distance to find the new diameter
			m_radius = (m_radius + otherCentroid.GetRadius() + distance) / 2;
		//if one circle is totally inside the other one?
		else if (distance + minRadius <= maxRadius)
			//the new radius is the bigger radius
			m_radius = maxRadius;
		//circles have intersection and one center is inside the other circle
		else
			//just add the smaller radius to the distance between two centers
			m_radius = distance + minRadius;
		//END: new radius

		//add intervals, calls, and positions
		vector<Interval> other_intervals = otherCentroid.GetIntervals();
		m_intervals.insert(m_intervals.end(), other_intervals.begin(), other_intervals.end());
		vector<Position> other_positions = otherCentroid.GetPositions();
		m_positions.insert(m_positions.end(), other_positions.begin(), other_positions.end());
		vector<Call> other_calls = otherCentroid.GetCalls();
		m_calls.insert(m_calls.end(), other_calls.begin(), other_calls.end());
	}


	/*
	 *
	 */
	vector<Centroid> NextProbableCentroids()
	{
		//find the maximum repetition time of a centroid as destination
		uint maxRepetition = 0;
		for (map<Centroid, uint32_t>::iterator it = number_of_times_occured_as_destination.begin(); it != number_of_times_occured_as_destination.end();	it++)
			if (it->second > maxRepetition)
				maxRepetition = it->second;
		//find all centroids with maximum repetition number
		vector<Centroid> v;
		if (maxRepetition > 0)
			//append all centroids whose repetition is equal to maximum repeatition
			for (map<Centroid, uint32_t>::iterator it = number_of_times_occured_as_destination.begin(); it != number_of_times_occured_as_destination.end(); it++)
				if (it->second >= maxRepetition)
					v.push_back(it->first);
		return v;
	}

	/*
	 *
	 */
	Centroid NextProbableCentroid()
	{
		//suppose there is no next probable centroid
		Centroid nextCentroid = Centroid(EMPTY_CENTROID_NAME, 0);
		//find a list of the next probable centoids using pure markov method
		vector<Centroid> nextProbableCentroids = this->NextProbableCentroids();
		//any next probable cenroid found?
		if (nextProbableCentroids.size() > 0)
		{
			//select the one with maximum accuracy till now
			double maximumAccuracy = 0;
			for (vector<Centroid>::iterator it = nextProbableCentroids.begin(); it != nextProbableCentroids.end(); it++)
				if (it->Accuracy_PureMarkov() >= maximumAccuracy)
				{
					//update th emaximum accuracy and next probable centroid
					maximumAccuracy = it->Accuracy_PureMarkov();
					nextCentroid = *it;
				}
		}
		return nextCentroid;
	}

//	/*
//	 *
//	 */
//	vector<Centroid> NextProbableCentroids_AfterCall(string callee) {
//		//find the maximum repetition time of a centroid as destination
//		uint maxRepetition = 0;
//		for (map<pair<Centroid, string>, uint32_t>::iterator it =
//				m_numberOfTimes__occuredAsDestination__afterCall.begin();
//				it != m_numberOfTimes__occuredAsDestination__afterCall.end();
//				it++)
//			//only consider the destination centroids which are after the call
//			if (it->first.second == callee)
//				if (it->second > maxRepetition)
//					maxRepetition = it->second;
//		//find all centroids with maximum repetition number
//		vector<Centroid> v;
//		if (maxRepetition > 0)
//			for (map<pair<Centroid, string>, uint32_t>::iterator it =
//					m_numberOfTimes__occuredAsDestination__afterCall.begin();
//					it != m_numberOfTimes__occuredAsDestination__afterCall.end();
//					it++)
//				//only consider destination centroids which are after the call
//				if (it->first.second == callee)
//					//append all centroids whose repetition is equal to maximum repetition
//					if (it->second >= maxRepetition)
//						v.push_back(it->first.first);
//		return v;
//	}
//

//	/*
//	 *
//	 */
//	Centroid *NextProbableCentroid_AfterCall(string callee) {
//		//suppose there is no next probable centroid
//		Centroid *nextCentroid = NULL;
//		//find a list of the next probable centoids using pure markov method
//		vector<Centroid> nextCentroids = NextProbableCentroids_AfterCall(
//				callee);
//		//any next probable cenroid found?
//		if (nextCentroids.size() > 0) {
//			//select the one with maximum accuracy till now
//			double maximumAccuracy = 0;
//			for (vector<Centroid>::iterator it = nextCentroids.begin();
//					it != nextCentroids.end(); it++)
//				if (it->Accuracy_MarkovWithCalls(callee) >= maximumAccuracy) {
//					//update th emaximum accuracy and next probable centroid
//					maximumAccuracy = it->Accuracy_MarkovWithCalls(callee);
//					nextCentroid = &(*it);
//				}
//		}
//		return nextCentroid;
//	}
//
//	/*
//	 *
//	 */
//	Centroid *NextProbableCentroid__XOI_Chains(string callee,
//			map<string, vector<vector<Centroid> > > self_poi_chains_after_call,
//			map<pair<CentroidType, string>, vector<vector<Centroid> > > partners_XOI_chains_after_call,
//			CentroidType XOI_type) {
//		//suppose there is no next probable POI
//		Centroid *next_probable_centroid = NULL;
//
//		//check if XOI chain for callee name is in partners XOI chains after call
//		//meaning: check if we have received required XOI chains from the callee
//		if (partners_XOI_chains_after_call.find(make_pair(XOI_type, callee))
//				!= partners_XOI_chains_after_call.end()) {
//			//create next centroid list considering only markov
//			vector<Centroid> next_centroids =
//					NextProbableCentroids_PureMarkov();
//			//get the last chain of self POI chains related to callee
//			vector<Centroid> self_chain =
//					self_poi_chains_after_call[callee].back();
//			//get the last chain of XOI chains related to callee
//			vector<Centroid> partner_chain =
//					partners_XOI_chains_after_call[make_pair(XOI_type, callee)].back();
//			//both chains have elements?
//			if (self_chain.size() > 0 && partner_chain.size() > 0) {
//				//find the co_location of partners by finding the two centroids from the two chains
//				// which have the minimum distance with each other
//				coordinate min_distance = MAX_COORDINATE;
//				Centroid *co_location = NULL;
//				for (vector<Centroid>::iterator it = self_chain.begin();
//						it != self_chain.end(); it++)
//					for (vector<Centroid>::iterator it2 = partner_chain.begin();
//							it2 != partner_chain.end(); it2++) {
//						coordinate distance = Position::Distance(it->GetX(),
//								it->GetY(), it2->GetX(), it2->GetY());
//						if (distance < min_distance) {
//							min_distance = distance;
//							co_location = &(*it);
//						}
//					}
//				//co-location found? (it will be found if both chains are non-empty, so maybe this condition is useless)
//				if (co_location != NULL) {
//					//find the minimum distance between centroids in the next centroids list with co-location centroid
//					min_distance = MAX_COORDINATE;
//					for (vector<Centroid>::iterator it = next_centroids.begin();
//							it != next_centroids.end(); it++) {
//						coordinate distance = Position::Distance(it->GetX(),
//								it->GetY(), co_location->GetX(),
//								co_location->GetY());
//						if (distance < min_distance)
//							min_distance = distance;
//					}
//					//delete the centroids in the next centroid list which have more distance than the minimum
//					for (vector<Centroid>::iterator it = next_centroids.begin(); it != next_centroids.end();)
//						if (Position::FartherThan(it->GetX(), it->GetY(), co_location->GetX(), co_location->GetY(), min_distance))
//							next_centroids.erase(it);
//						else
//							it++;
//				}
//			}
//			//any next centroid in the list?
//			if (next_centroids.size() > 0) {
//				//find the POI which has had the maximum accuracy till now
//				double maximum_accuracy = 0;
//				for (vector<Centroid>::iterator it = next_centroids.begin();
//						it != next_centroids.end(); it++) {
//					double accuracy = it->Accuracy_XOI_chains(XOI_type, callee);
//					if (accuracy >= maximum_accuracy) {
//						maximum_accuracy = accuracy;
//						next_probable_centroid = &(*it);
//					}
//				}
//			}
//
//		}
//
//		return next_probable_centroid;
//	}
//
	/*
	 * writes the data of the node to binary file
	 */
	bool WriteToBinaryFile(ofstream& fout)
	{
		//suppose success
		bool result = true;

		try
		{
			//write fields to file
			for (vector<SerializingItems>::iterator it = m_serializing_items.begin(); it != m_serializing_items.end(); it++)
			{
				switch(*it)
				{
					case NAME:
						if (!Tools::WriteStringToBinaryFile(m_name, fout))
							result = false;
						break;
					case SUM_OF_X:
						if (!Tools::WriteDataToBinaryFile(m_sum_of_x, fout))
							result = false;
						break;
					case SUM_OF_Y:
						if (!Tools::WriteDataToBinaryFile(m_sum_of_y, fout))
							result = false;
						break;
					case NUMBER_OF_MEMBERS:
						if (!Tools::WriteDataToBinaryFile(m_number_of_members, fout))
							result = false;
						break;
					case RADIUS:
						if (!Tools::WriteDataToBinaryFile(m_radius, fout))
							result = false;
						break;
					case POSITIONS:
						if (!Tools::WriteVectorToBinaryFile(m_positions, fout))
							result = false;
						break;
					case INTERVALS:
						if (!Tools::WriteVectorToBinaryFile(m_intervals, fout))
							result = false;
						break;
					case CALLS:
						if (!Tools::WriteVectorToBinaryFile(m_calls, fout))
							result = false;
						break;
				}
				//any error? do not continue
				if (result != true)
					break;
			}
		}
		catch(const exception& e)
		{
			MEHDI_ERROR_LINE("Error writing centroid data to the binary file." << endl << e.what());
			result = false;
		}

		return result;
	}

	/*
	 * reads the data of the node from binary file
	 */
	bool ReadFromBinaryFile(ifstream& fin)
	{
		//suppose success
		bool result = true;

		try
		{
			//read fields from file
			for (vector<SerializingItems>::iterator it = m_serializing_items.begin(); it != m_serializing_items.end(); it++)
			{
				switch(*it)
				{
					case NAME:
						if (!Tools::ReadStringFromBinaryFile(m_name, fin))
							result = false;
						break;
					case SUM_OF_X:
						if (!Tools::ReadDataFromBinaryFile(m_sum_of_x, fin))
							result = false;
						break;
					case SUM_OF_Y:
						if (!Tools::ReadDataFromBinaryFile(m_sum_of_y, fin))
							result = false;
						break;
					case NUMBER_OF_MEMBERS:
						if (!Tools::ReadDataFromBinaryFile(m_number_of_members, fin))
							result = false;
						break;
					case RADIUS:
						if (!Tools::ReadDataFromBinaryFile(m_radius, fin))
							result = false;
						break;
					case POSITIONS:
						if (!Tools::ReadVectorFromBinaryFile(m_positions,fin))
							result = false;
						break;
					case INTERVALS:
						if (!Tools::ReadVectorFromBinaryFile(m_intervals,fin))
							result = false;
						break;
					case CALLS:
						if (!Tools::ReadVectorFromBinaryFile(m_calls,fin))
							result = false;
						break;
				}
				//any error? do not continue
				if (result != true)
					break;
			}

		}
		catch(const exception& e)
		{
			MEHDI_ERROR_LINE("Error reading centroid data from binary file." << endl << e.what());
			result = false;
		}

		//check the validity of positions
		for (vector<Position>::iterator it = m_positions.begin(); it != m_positions.end(); it++)
			if (it->GetX() == INVALID_COORDINATE || it->GetY() == INVALID_COORDINATE || it->GetTime() == INVALID_TIME)
			{
				MEHDI_ERROR_LINE("invalid position read from binary file.");
				result = false;
			}

		return result;
	}

	/*
	 * checks if this centroid is equal to the passed centroid
	 */
	bool IsEqual(Centroid c) const
	{
		//name
		if (m_name != c.GetName())
			return false;
		//sum of x
		if (m_sum_of_x != c.GetSumOfX())
			return false;
		//sum of y
		if (m_sum_of_y != c.GetSumOfY())
			return false;
		//number of members
		if (m_number_of_members != c.GetNumberOfMembers())
			return false;
		//radius
		if (m_radius != c.GetRadius())
			return false;
		//positions
		vector<Position> v_positions = c.GetPositions();
		if (m_positions.size() != v_positions.size())
			return false;
		for (size_t i = 0; i < v_positions.size(); i++)
			if (!m_positions[i].IsEqual(v_positions[i]))
				return false;
		//intervals
		vector<Interval> v_intervals = c.GetIntervals();
		if (m_intervals.size() != v_intervals.size())
			return false;
		for (size_t i = 0; i < v_intervals.size(); i++)
			if (!m_intervals[i].IsEqual(v_intervals[i]))
				return false;
		//calls
		vector<Call> v_calls = c.GetCalls();
		if (m_calls.size() != v_calls.size())
			return false;
		for (size_t i = 0; i < v_calls.size(); i++)
			if (!m_calls[i].IsEqual(v_calls[i]))
				return false;
		//all match
		return true;
	}

};//class centroid



} //namespace mehdi
} //namespace ndn
} //namespace ns3

#endif /* NDN_MEHDI_CENTROID_H_ */
