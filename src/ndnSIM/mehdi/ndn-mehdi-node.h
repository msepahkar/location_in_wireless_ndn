//
/*
 to be done:

 1- extreme coordinates should be assigned to nodes for which we do not have valid location data.
 (with enough distance between themselves so that they do not hear each other's voice)
 (for example on the top y axis with 1000000 meters distance between nodes)
 (before their joining the play and after their leaving the play).
 in this way there is no need to enter the nodes into play and make the leave the play.
 this will be done automatically as no signal will reach them and their signal will reach no one.

 2- all times in the movement files should be converted to their distance from the beginning of simulation.
 the starting time of simulation will be just before the earliest time available in movement files.

 3- maybe for optimization it will be good to check the validity of location data before sending interest packet

 4- a thread should run periodically and check all nodes for their being in the play and record number of nodes which are in the play
 along with other useful information like active consumers and active producers

 */
//
/*
 * Author: Mehdi Sepahkar
 */

#ifndef NDN_MEHDI_NODE_H
#define NDN_MEHDI_NODE_H

#include "src/ndnSIM/mehdi/ndn-mehdi-location.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-tools.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-centroid.h"
#include "ns3/node.h"
#include "ns3/mobility-module.h"
#include <vector>
#include <map>
#include <algorithm>
#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <ns3/application.h>

namespace ns3 {
namespace ndn {
namespace mehdi {

using namespace std;


/////////////////////////////
//some constants
/////////////////////////////

const coordinate pOI_distance_threshold = 20;
const elapsed_time pOI_time_threshold = 60 * 10;
const Position INVALID_POSITION = Position(INVALID_COORDINATE, INVALID_COORDINATE, INVALID_TIME, false, false, false);
const Location INVALID_LOCATION = Location(INVALID_COORDINATE, INVALID_COORDINATE, INVALID_TIME, INVALID_TIME);


// range of values which distance threshold of POIs can have
const static coordinate POI_distance_threshold_range[] = {10, 20, 40}; //meters
// range of values which time threshold for making a POI valid can have (from high to low)
const static elapsed_time POI_time_threshold_range[] = {60 * 60,
												45 * 60,
												30 * 60,
												15 * 60,
												10 * 60,
												5 * 60};  // seconds
// range of distance threshold between two centroids for marging them
const static coordinate POI_merging_threshold_range[] = {50, 80, 120}; //meters
const static coordinate ZOI_merging_threshold_range[] = {200, 300, 500}; //meters
const static coordinate ROI_merging_threshold_range[] = {1000, 1500, 2000}; //meters
// minimum and maximum number of acceptable POIs
const static size_t n_POI_lower_limit = 3;
const static size_t n_POI_upper_limit = 50;
// ratio of ZOIs to POIs and ROIs to ZOIs
const static double ZOI_to_POI_ratio = 2 / 3;
const static double ROI_to_ZOI_ratio = 2 / 3;
// minimum number of members in a centroid to make it valid
const static size_t POI_number_of_members_threshold = 10;
const static size_t ZOI_number_of_members_threshold = 20;
const static size_t ROI_number_of_members_threshold = 30;
//path for movement files
const string movements_dir_path = "user locations/movements/";
//number of mehdi nodes
const size_t NUMBER_OF_NODES = 130;
//name of empty node
const string EMPTY_NODE_NAME = "empty node";

/////////////////////////////
// MehdiNode class
//a new type of node
/////////////////////////////
class MehdiNode: public Node {

	enum SerializingItem{
		FIRST_SERIALIZED_ITEM,
		ORIGINAL_NAME = FIRST_SERIALIZED_ITEM,
		USER_FRIENDLY_NAME,
		FIRST_AVAILABLE_TIME,
		LAST_AVAILABLE_TIME,
		POI_DISTANCE_THRESHOLD,
		POI_MERGING_THRESHOLD,
		ZOI_MERGING_THRESHOLD,
		ROI_MERGING_THRESHOLD,
		POI_TIME_THRESHOLD,
		POSITIONS,
		CALLS,
		POIS,
		ZOIS,
		ROIS,
		LOCATION_CHAINS,
		PREDICTED_NEXT_LOCATION_CHAINS,
		LAST_SERIALIZED_ITEM = PREDICTED_NEXT_LOCATION_CHAINS
	};
	string m_original_name;		//original name in the data set
	string m_user_friendly_name;	//some user friendly name like user1, user2, ...
	elapsed_time m_first_available_time;
	elapsed_time m_last_available_time;
	uint m_current_loc_chain_index;
	uint m_POI_distance_threshold_index;
	uint m_POI_merging_threshold_index;
	uint m_ZOI_merging_threshold_index;
	uint m_ROI_merging_threshold_index;
	uint m_POI_time_threshold_index;
	vector<Position> m_positions;
	vector<LocationChain> m_location_chains;	//collection of chains of traversed locations in each day
	vector<LocationChain> m_predicted_next_location_chains;	//locations predicted as the next location
	vector<Centroid> m_POIs;
	vector<Centroid> m_ZOIs;
	vector<Centroid> m_ROIs;
	vector<Call> m_calls;

public:
	queue<pair<string, elapsed_time> > request_times;

public:
	MehdiNode()
	{
//		m_current_time = INVALID_TIME;
//		m_current_location = new Location();
//		m_today_locations = new loc_container();
		m_original_name = "";
		m_user_friendly_name = EMPTY_NODE_NAME;
		m_first_available_time = INVALID_TIME;
		m_last_available_time = INVALID_TIME;
		m_current_loc_chain_index = 0;
		m_POI_distance_threshold_index = 0;
		m_POI_merging_threshold_index = 0;
		m_ZOI_merging_threshold_index = 0;
		m_ROI_merging_threshold_index = 0;
		m_POI_time_threshold_index = 0;
	}
	/*
	 * returns user friendly name of the node
	 * (user friendly name should be like 'user 1', 'user 2', ...)
	 */
	string GetUserFriendlyName() const
	{
		return m_user_friendly_name;
	}

	/*
	 * returns original name of the node
	 * (like fa-35-01, ...)
	 */
	string GetOriginalName() const
	{
		return m_original_name;
	}

	/*
	 * gets the first available time
	 */
	elapsed_time GetFirstAvailableTime() const
	{
		return m_first_available_time;
	}

	/*
	 * gets the last available time
	 */
	elapsed_time GetLastAvailableTime() const
	{
		return m_last_available_time;
	}

	/*
	 * creates a copy of position vector and returns it
	 *  creating a copy is not actually needed but just done
	 *  for extra confidence
	 */
	vector<Position> GetPositions() const
	{
		vector<Position> v = m_positions;
		return v;
	}

	/*
	 * creates a copy of call vector and returns it
	 *  creating a copy is not actually needed.
	 *  just for extra confidence.
	 */
	vector<Call> GetCalls() const
	{
		vector<Call> v = m_calls;
		return v;
	}

	/*
	 * creates a copy of POIs and returns it.
	 *  creating a copy is not required.
	 *  just for extra confidence
	 */
	vector<Centroid> GetPOIs() const
	{
		vector<Centroid> v = m_POIs;
		return v;
	}

	/*
	 * creates a copy of ZOIs and returns it.
	 *  creating a copy is not required.
	 *  just for extra confidence.
	 */
	vector<Centroid> GetZOIs() const
	{
		vector<Centroid> v = m_ZOIs;
		return v;
	}

	/*
	 * creates a copy of ROIs and returns it.
	 *  creating a copy is not requried.
	 *  just for extra confidence.
	 */
	vector<Centroid> GetROIs() const
	{
		vector<Centroid> v = m_ROIs;
		return v;
	}

	/*
	 * read movement data from text file
	 */
	bool ReadMovementsFromTextFile_And_SortThem(string fileFullName)
	{
		bool result = true;
		//open the text file for reading
		filebuf f_b;
		if (!f_b.open(fileFullName.c_str(), ios::in))
		{
		  MEHDI_ERROR_LINE("error opening the user 1 file");
		  //file has not been opened so no need to close it.
		  return false;
		}
		istream i_s(&f_b);
		//for all lines in the file
		for (string line; getline(i_s, line);)
		{
			stringstream movement_stream(line);
			vector<string> movement_vector;
			string movement_element;

			//separate and save elements in each line (and delete the '"' character)
			while(getline(movement_stream, movement_element, ','))
			{
				movement_element.erase(remove( movement_element.begin(), movement_element.end(), '\"' ), movement_element.end());
				movement_vector.push_back(movement_element);
			}

			//convert text to proper data
			coordinate x, y;
			elapsed_time t;
			bool is_weekday, is_day_switched, is_week_switched;
			istringstream ss;

			//rank index (according to number of positions) is 0, but it is not needed
			const int user_friendly_name_index = 1;
			const string user_friendly_field_pretext = "user_friendly_name:";
			const int original_name_index = 2;
			const string original_name_field_pretext = "original_name:";
			//date time index is 3, but it is not needed because we have t field
			const int x_index = 4;
			const string x_field_pretext = "x:";
			const int y_index = 5;
			const string y_field_pretext = "y:";
			const int t_index = 6;
			const string time_field_pretext = "elapsed_seconds:";
			const int is_weekday_index = 7;
			const string is_weekday_field_pretext = "is_weekday:";
			const int is_day_switched_index = 8;
			const string is_day_switched_field_pretext = "is_day_switched:";
			const int is_week_switched_index = 9;
			const string is_week_switched_field_pretext = "is_week_switched:";

			//user friendly name
			if (!Tools::RemoveSubstring(movement_vector[user_friendly_name_index], user_friendly_field_pretext))
			{
				MEHDI_ERROR_LINE("incorrect user friendly name field.");
				return false;
			}
			if (m_user_friendly_name == EMPTY_NODE_NAME)
				m_user_friendly_name = movement_vector[user_friendly_name_index];
			else if (m_user_friendly_name != movement_vector[user_friendly_name_index])
			{
				MEHDI_ERROR_LINE("user friendly name does not match.");
				result = false;
				//an error has occurred. so it is better not to continue.
				break;
			}

			//original name
			if (!Tools::RemoveSubstring(movement_vector[original_name_index], original_name_field_pretext))
			{
				MEHDI_ERROR_LINE("incorrect original name field.");
			}
			if (m_original_name.empty())
				m_original_name = movement_vector[2];
			else if (m_original_name != movement_vector[2])
			{
				MEHDI_ERROR_LINE("original name does not match.");
				result = false;
				//an error has occurred. so it is better not to continue.
				break;
			}

			//x
			if (!Tools::RemoveSubstring(movement_vector[x_index], x_field_pretext))
			{
				MEHDI_ERROR_LINE("incorrect x field.");
				result = false;
				//an error has occurred. so it is better not to continue.
				break;
			}
			ss.str("");
			ss.clear();
			ss.str(movement_vector[x_index]);
			ss >> x;

			//y
			if (!Tools::RemoveSubstring(movement_vector[y_index], y_field_pretext))
			{
				MEHDI_ERROR_LINE("incorrect y field.");
				result = false;
				//an error has occurred. so it is better not to continue.
				break;
			}
			ss.str("");
			ss.clear();
			ss.str(movement_vector[5]);
			ss >> y;

			//time
			if (!Tools::RemoveSubstring(movement_vector[t_index], time_field_pretext))
			{
				MEHDI_ERROR_LINE("incorrect elapsed seconds field.");
				result = false;
				//an error has occurred. so it is better not to continue.
				break;
			}
			ss.str("");
			ss.clear();
			ss.str(movement_vector[t_index]);
			ss >> t;

			//is weekday
			if (!Tools::RemoveSubstring(movement_vector[is_weekday_index], is_weekday_field_pretext))
			{
				MEHDI_ERROR_LINE("incorrect is weekday field.");
				result = false;
				//an error has occurred. so it is better not to continue.
				break;
			}
			ss.str("");
			ss.clear();
			ss.str(movement_vector[is_weekday_index]);
			ss >> is_weekday;

			//is day switched
			if (!Tools::RemoveSubstring(movement_vector[is_day_switched_index], is_day_switched_field_pretext))
			{
				MEHDI_ERROR_LINE("incorrect is day switched field.");
				result = false;
				//an error has occurred. so it is better not to continue.
				break;
			}
			ss.str("");
			ss.clear();
			ss.str(movement_vector[is_day_switched_index]);
			ss >> is_day_switched;

			//is week switched
			if (!Tools::RemoveSubstring(movement_vector[is_week_switched_index], is_week_switched_field_pretext))
			{
				MEHDI_ERROR_LINE("incorrect is week switched field.");
				result = false;
				//an error has occurred. so it is better not to continue.
				break;
			}
			ss.str("");
			ss.clear();
			ss.str(movement_vector[is_week_switched_index]);
			ss >> is_week_switched;

			if (!AddPosition(x, y, t, is_weekday, is_day_switched, is_week_switched))
			{
				MEHDI_ERROR_LINE("problem adding position read from text file to the node.");
				result = false;
				//an error has occurred. so it is better not to continue.
				break;
			}
		}
		f_b.close();

		//sort positions according to time
		sort( m_positions.begin( ), m_positions.end( ), PositionComparatorRegardingTime() );

		m_first_available_time = m_positions.front().GetTime();
		m_last_available_time = m_positions.back().GetTime();

		return result;
	}

	/*
	 * write the data of the node to binary file
	 */
	bool WriteToBinaryFile(string fileFullName)
	{
		ofstream fout;
		bool result = true;

		try
		{
			//open the file for writing
			fout.open(fileFullName.c_str(), ios::out | ios::binary);

			//write fields to file
//			for (vector<SerializingItems>::iterator it = m_serializing_items.begin(); it != m_serializing_items.end(); it++)
			for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			{
				switch(i)
				{
					case ORIGINAL_NAME:
						if (!Tools::WriteStringToBinaryFile(m_original_name, fout))
							result = false;
						break;
					case USER_FRIENDLY_NAME:
						if (!Tools::WriteStringToBinaryFile(m_user_friendly_name, fout))
							result = false;
						break;
					case FIRST_AVAILABLE_TIME:
						if (!Tools::WriteDataToBinaryFile(m_first_available_time, fout))
							result = false;
						break;
					case LAST_AVAILABLE_TIME:
						if (!Tools::WriteDataToBinaryFile(m_last_available_time, fout))
							result = false;
						break;
					case POI_DISTANCE_THRESHOLD:
						if (!Tools::WriteDataToBinaryFile(Get_POI_DistanceThreshold(), fout))
							result = false;
						break;
					case POI_MERGING_THRESHOLD:
						if (!Tools::WriteDataToBinaryFile(Get_POI_MergingThreshold(), fout))
							result = false;
						break;
					case ZOI_MERGING_THRESHOLD:
						if (!Tools::WriteDataToBinaryFile(Get_ZOI_MergingThreshold(), fout))
							result = false;
						break;
					case ROI_MERGING_THRESHOLD:
						if (!Tools::WriteDataToBinaryFile(Get_ROI_MergingThreshold(), fout))
							result = false;
						break;
					case POI_TIME_THRESHOLD:
						if (!Tools::WriteDataToBinaryFile(Get_POI_TimeThreshold(), fout))
							result = false;
						break;
					case POSITIONS:
						if (!Tools::WriteVectorToBinaryFile(m_positions, fout))
							result = false;
						break;
					case CALLS:
						if (!Tools::WriteVectorToBinaryFile(m_calls, fout))
							result = false;
						break;
					case POIS:
						//first write number of POIs
						if (Tools::WriteDataToBinaryFile(m_POIs.size(), fout))
						{
							//now write POIs one by one
							for (vector<Centroid>::iterator it = m_POIs.begin(); it != m_POIs.end(); it++)
								if (!it->WriteToBinaryFile(fout))
								{
									//if error in writing a POI do not continue
									result = false;
									break;
								}
						}
						else
							//cannot write number of POIs
							result = false;
						break;
					case ZOIS:
						//first write number of ZOIs
						if (Tools::WriteDataToBinaryFile(m_ZOIs.size(), fout))
						{
							//now write ZOIs one by one
							for (vector<Centroid>::iterator it = m_ZOIs.begin(); it != m_ZOIs.end(); it++)
								if (!it->WriteToBinaryFile(fout))
								{
									//problem in writing ZOI
									result = false;
									break;
								}
						}
						//cannot write number of ZOIs
						else
							result = false;
						break;
					case ROIS:
						//first write number of ROIs
						if (Tools::WriteDataToBinaryFile(m_ROIs.size(), fout))
						{
							//now write ROIs one by one
							for (vector<Centroid>::iterator it = m_ROIs.begin(); it != m_ROIs.end(); it++)
								if (!it->WriteToBinaryFile(fout))
								{
									//problem writing ROI
									result = false;
									break;
								}
						}
						//cannot write number of ROIs
						else
							result = false;
						break;
					case LOCATION_CHAINS:
						//first write number of chains
						if (Tools::WriteDataToBinaryFile(m_location_chains.size(), fout))
						{
							//now write chains one by one
							for (vector<LocationChain>::iterator it = m_location_chains.begin(); it != m_location_chains.end(); it++)
							{
								vector<Location> chain = it->GetChainVector();
								//first write the size of the chain
								if (Tools::WriteDataToBinaryFile(chain.size(), fout))
								{
									for (vector<Location>::iterator it = chain.begin(); it != chain.end(); it++)
										if (!it->WriteToBinaryFile(fout))
										{
											result = false;
											break;
										}
								}
								else
								{
									result = false;
									break;
								}
							}
						}
						//cannot write number of chains
						else
							result = false;
						break;
					case PREDICTED_NEXT_LOCATION_CHAINS:
						//first write number of chains
						if (Tools::WriteDataToBinaryFile(m_predicted_next_location_chains.size(), fout))
						{
							//now write chains one by one
							for (vector<LocationChain>::iterator it = m_predicted_next_location_chains.begin(); it != m_predicted_next_location_chains.end(); it++)
							{
								vector<Location> chain = it->GetChainVector();
								//first write the size of the chain
								if (Tools::WriteDataToBinaryFile(chain.size(), fout))
								{
									for (vector<Location>::iterator it = chain.begin(); it != chain.end(); it++)
										if (!it->WriteToBinaryFile(fout))
										{
											result = false;
											break;
										}
								}
								else
								{
									result = false;
									break;
								}
							}
						}
						//cannot write number of chains
						else
							result = false;
						break;
					default:
						MEHDI_FATAL_ERROR("unrecognized serializing item in node.", -1);
						result = false;
				}
				//if problem writing to file do not continue for the next item
				if (result != true)
					break;
			}
		}
		catch(const exception& e)
		{
			stringstream ss;
			ss << "Error writing node data to the binary file : " << fileFullName << endl << e.what();
			MEHDI_ERROR_LINE(ss.str());
			result = false;
		}

		//writing done
		fout.close();

		return result;
	}

	/*
	 * read the data of the node from binary file
	 */
	bool ReadFromBinaryFile(string fileFullName)
	{
		//input stream
		ifstream fin;

		//suppose success
		bool result = true;

		//required parameters
		coordinate coordinate_threshold;
		elapsed_time time_threshold;
		size_t n;

		try
		{
			//open the file for reading
			fin.open(fileFullName.c_str(), ios::in | ios::binary);

			//read fields from file
			for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			{
				switch(i)
				{
					case ORIGINAL_NAME:
						if (!Tools::ReadStringFromBinaryFile(m_original_name, fin))
							result = false;
						break;
					case USER_FRIENDLY_NAME:
						if (!Tools::ReadStringFromBinaryFile(m_user_friendly_name, fin))
							result = false;
						break;
					case FIRST_AVAILABLE_TIME:
						if (!Tools::ReadDataFromBinaryFile(m_first_available_time, fin))
							result = false;
						break;
					case LAST_AVAILABLE_TIME:
						if (!Tools::ReadDataFromBinaryFile(m_last_available_time, fin))
							result = false;
						break;
					case POI_DISTANCE_THRESHOLD:
						//first suppose failure
						result = false;
						//read the threshold
						if (Tools::ReadDataFromBinaryFile(coordinate_threshold, fin))
							//set the threshold
							if (Set_POI_DistanceThreshold(coordinate_threshold))
								//success
								result = true;
						break;
					case POI_MERGING_THRESHOLD:
						//first suppose failure
						result = false;
						//read the threshold
						if (Tools::ReadDataFromBinaryFile(coordinate_threshold, fin))
							//set the threshold
							if (Set_POI_MergingThreshold(coordinate_threshold))
								//success
								result = true;
						break;
					case ZOI_MERGING_THRESHOLD:
						//first suppose failure
						result = false;
						//read the threshold
						if (Tools::ReadDataFromBinaryFile(coordinate_threshold, fin))
							//set the threshold
							if (Set_ZOI_MergingThreshold(coordinate_threshold))
								//success
								result = true;
						break;
					case ROI_MERGING_THRESHOLD:
						//first suppose failure
						result = false;
						//read the threshold
						if (Tools::ReadDataFromBinaryFile(coordinate_threshold, fin))
							//set the threshold
							if (Set_ROI_MergingThreshold(coordinate_threshold))
								//success
								result = true;
						break;
					case POI_TIME_THRESHOLD:
						//first suppose failure
						result = false;
						//read the threshold
						if (Tools::ReadDataFromBinaryFile(time_threshold, fin))
							//set the threshold
							if (Set_POI_TimeThreshold(time_threshold))
								//success
								result = true;
						break;
					case POSITIONS:
						if (!Tools::ReadVectorFromBinaryFile(m_positions,fin))
							result = false;
						break;
					case CALLS:
						if (!Tools::ReadVectorFromBinaryFile(m_calls,fin))
							result = false;
						break;
					case POIS:
						//erase previous POIs
						m_POIs.clear();
						//first read number of POIs
						if (!Tools::ReadDataFromBinaryFile(n, fin))
							result = false;
						//read POIs one by one
						for (size_t i = 0; i < n; i++)
						{
							//create an empty POI
							m_POIs.push_back(Centroid(READING_FROM_BINARY_FILE, 0));
							//read its data from the file
							if (!m_POIs.back().ReadFromBinaryFile(fin))
							{
								//failure
								result = false;
								//remove the added empty centroid
								m_POIs.pop_back();
								//do not continue
								break;
							}
						}
						break;
					case ZOIS:
						//erase previous ZOIs
						m_ZOIs.clear();
						//read number of ZOIs
						if (!Tools::ReadDataFromBinaryFile(n, fin))
							result = false;
						//read ZOIs one by one
						for (size_t i = 0; i < n; i++)
						{
							//first create an empty ZOI
							m_ZOIs.push_back(Centroid(READING_FROM_BINARY_FILE, 0));
							//then fill its data from the file
							if (!m_ZOIs.back().ReadFromBinaryFile(fin))
							{
								//failure
								result = false;
								//erase the empty ZOI
								m_ZOIs.pop_back();
								//do not continue
								break;
							}
						}
						break;
					case ROIS:
						//erase previous ROIs
						m_ROIs.clear();
						//read number of ROIs
						if (!Tools::ReadDataFromBinaryFile(n, fin))
							result = false;
						//read ROIs one by one
						for (size_t i = 0; i < n; i++)
						{
							//first create an empty ROI
							m_ROIs.push_back(Centroid(READING_FROM_BINARY_FILE, 0));
							//now read its data from the file
							if (!m_ROIs.back().ReadFromBinaryFile(fin))
							{
								//failure
								result = false;
								//erase the empty ROI
								m_ROIs.pop_back();
								//do not continue
								break;
							}
						}
						break;
					case LOCATION_CHAINS:
						//clear the location chains first
						m_location_chains.clear();
						//now read number of chains
						if (Tools::ReadDataFromBinaryFile(n, fin))
						{
							//now read chains one by one
							for (size_t i = 0; i < n; i++)
							{
								//push an empty chain
								m_location_chains.push_back(LocationChain());

								//now read its locations
								vector<LocationChain> chain;
								size_t chain_size;

								//first read the size of locations in chain
								if (Tools::ReadDataFromBinaryFile(chain_size, fin))
								{
									//now read locations one by one
									for (size_t j = 0; j < chain_size; j++)
									{
										Location location;
										if (!location.ReadFromBinaryFile(fin))
										{
											result = false;
											break;
										}
										m_location_chains.back().AddLocation(location);
									}
								}
								else
								{
									result = false;
									break;
								}
							}
						}
						//cannot write number of chains
						else
							result = false;
						break;
					case PREDICTED_NEXT_LOCATION_CHAINS:
						//clear the location chains first
						m_predicted_next_location_chains.clear();
						//now read number of chains
						if (Tools::ReadDataFromBinaryFile(n, fin))
						{
							//now read chains one by one
							for (size_t i = 0; i < n; i++)
							{
								//push an empty chain
								m_predicted_next_location_chains.push_back(LocationChain());

								//now read its locations
								vector<LocationChain> chain;
								size_t chain_size;

								//first read the size of locations in chain
								if (Tools::ReadDataFromBinaryFile(chain_size, fin))
								{
									//now read locations one by one
									for (size_t j = 0; j < chain_size; j++)
									{
										Location location;
										if (!location.ReadFromBinaryFile(fin))
										{
											result = false;
											break;
										}
										m_predicted_next_location_chains.back().AddLocation(location);
									}
								}
								else
								{
									result = false;
									break;
								}
							}
						}
						//cannot write number of chains
						else
							result = false;
						break;
					default:
						MEHDI_FATAL_ERROR("unrecognized serializing item in node.", -1);
						result = false;
				}
				//if any failure do not continue
				if (result != true)
					break;
			}

		}
		catch(const exception& e)
		{
			stringstream ss;
			ss << "Error reading node data from binary file: " << fileFullName << endl << e.what();
			MEHDI_ERROR_LINE(ss.str());
			result = false;
		}

		//reading done
		fin.close();

		//check for validity of positons read
		for (vector<Position>::iterator it = m_positions.begin(); it != m_positions.end(); it++)
			if (it->GetX() == INVALID_COORDINATE || it->GetY() == INVALID_COORDINATE || it->GetTime() == INVALID_TIME)
			{
				MEHDI_ERROR_LINE("invalid position read from binary file.");
				result = false;
			}

		return result;
	}

	/*
	 * checks if this mehdi node is equal to the passed node
	 */
	bool IsEqual(MehdiNode n)
	{
		vector<Call> v_calls;
		vector<Centroid> v_POIs, v_ZOIs, v_ROIs;
		vector<LocationChain> v_location_chains;
		vector<LocationChain> v_predicted_next_location_chains;

		for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			switch(i)
			{
				//user friendly name
				case USER_FRIENDLY_NAME:
					if (m_user_friendly_name != n.GetUserFriendlyName())
						return false;
					break;
				//original name
				case ORIGINAL_NAME:
					if (m_original_name != n.GetOriginalName())
						return false;
					break;
				case FIRST_AVAILABLE_TIME:
					if (m_first_available_time != n.GetFirstAvailableTime())
						return false;
					break;
				case LAST_AVAILABLE_TIME:
					if (m_last_available_time != n.GetLastAvailableTime())
						return false;
					break;
				case POI_DISTANCE_THRESHOLD:
					if (Get_POI_DistanceThreshold() != n.Get_POI_DistanceThreshold())
						return false;
					break;
				case POI_MERGING_THRESHOLD:
					if (Get_POI_MergingThreshold() != n.Get_POI_MergingThreshold())
						return false;
					break;
				case ZOI_MERGING_THRESHOLD:
					if (Get_ZOI_MergingThreshold() != n.Get_ZOI_MergingThreshold())
						return false;
					break;
				case ROI_MERGING_THRESHOLD:
					if (Get_ROI_MergingThreshold() != n.Get_ROI_MergingThreshold())
						return false;
					break;
				case POI_TIME_THRESHOLD:
					if (Get_POI_TimeThreshold() != n.Get_POI_TimeThreshold())
						return false;
					break;
				//positions
				case POSITIONS:
					if (m_positions.size() != n.GetPositions().size())
						return false;
					for (size_t i = 0; i < m_positions.size(); i++)
						if (!m_positions[i].IsEqual(n.GetPositions()[i]))
							return false;
					break;
				//calls
				case CALLS:
					v_calls = n.GetCalls();
					if (m_calls.size() != v_calls.size())
						return false;
					for (size_t i = 0; i < m_calls.size(); i++)
						if (!m_calls[i].IsEqual(v_calls[i]))
							return false;
					break;
				//POIs
				case POIS:
					v_POIs = n.GetPOIs();
					if (m_POIs.size() != v_POIs.size())
						return false;
					for (size_t i = 0; i < m_POIs.size(); i++)
						if (!m_POIs[i].IsEqual(v_POIs[i]))
							return false;
					break;
				//ZOIs
				case ZOIS:
					v_ZOIs = n.GetZOIs();
					if (m_ZOIs.size() != v_ZOIs.size())
						return false;
					for (size_t i = 0; i < m_ZOIs.size(); i++)
						if (!m_ZOIs[i].IsEqual(v_ZOIs[i]))
							return false;
					break;
				//ROIs
				case ROIS:
					v_ROIs = n.GetROIs();
					if (m_ROIs.size() != v_ROIs.size())
						return false;
					for (size_t i = 0; i < m_ROIs.size(); i++)
						if (!m_ROIs[i].IsEqual(v_ROIs[i]))
							return false;
					break;
				//location chains
				case LOCATION_CHAINS:
					v_location_chains = n.GetLocationChains();
					if (m_location_chains.size() != v_location_chains.size())
						return false;
					for (size_t i = 0; i < m_location_chains.size(); i++)
						if (!m_location_chains[i].IsEqual(v_location_chains[i]))
							return false;
					break;
				//predicted next location chains
				case PREDICTED_NEXT_LOCATION_CHAINS:
					v_predicted_next_location_chains = n.GetPredictedNextLocationChains();
					if (m_predicted_next_location_chains.size() != v_predicted_next_location_chains.size())
						return false;
					for (size_t i = 0; i < m_predicted_next_location_chains.size(); i++)
						if (!m_predicted_next_location_chains[i].IsEqual(v_predicted_next_location_chains[i]))
							return false;
					break;
				default:
					MEHDI_ERROR_LINE("undefined field type in comparing mehdi nodes.");
					return false;
			}

		//temp begin
		cout << m_user_friendly_name << " matches." << endl;
		//temp end

		return true;
	}

	/*
	 * checks if this mehdi node is equal to the passed node
	 */
	bool IsEqual2(Ptr<MehdiNode> n)
	{
		vector<Call> v_calls;
		vector<Centroid> v_POIs, v_ZOIs, v_ROIs;
		vector<LocationChain> v_location_chains;
		vector<LocationChain> v_predicted_next_location_chains;

		for (uint i = FIRST_SERIALIZED_ITEM; i <= LAST_SERIALIZED_ITEM; i++)
			switch(i)
			{
				//user friendly name
				case USER_FRIENDLY_NAME:
					if (m_user_friendly_name != n->GetObject<MehdiNode>()->GetUserFriendlyName())
						return false;
					break;
				//original name
				case ORIGINAL_NAME:
					if (m_original_name != n->GetObject<MehdiNode>()->GetOriginalName())
						return false;
					break;
				//first available time
				case FIRST_AVAILABLE_TIME:
					if (m_first_available_time != n->GetFirstAvailableTime())
						return false;
					break;
				//last available time
				case LAST_AVAILABLE_TIME:
					if (m_last_available_time != n->GetLastAvailableTime())
						return false;
					break;
				//poi distance threshold
				case POI_DISTANCE_THRESHOLD:
					if (Get_POI_DistanceThreshold() != n->GetObject<MehdiNode>()->Get_POI_DistanceThreshold())
						return false;
					break;
				//poi merging threshold
				case POI_MERGING_THRESHOLD:
					if (Get_POI_MergingThreshold() != n->GetObject<MehdiNode>()->Get_POI_MergingThreshold())
						return false;
					break;
				//zoi merging threshold
				case ZOI_MERGING_THRESHOLD:
					if (Get_ZOI_MergingThreshold() != n->GetObject<MehdiNode>()->Get_ZOI_MergingThreshold())
						return false;
					break;
				//roi merging threshold
				case ROI_MERGING_THRESHOLD:
					if (Get_ROI_MergingThreshold() != n->GetObject<MehdiNode>()->Get_ROI_MergingThreshold())
						return false;
					break;
				//poi time threshold
				case POI_TIME_THRESHOLD:
					if (Get_POI_TimeThreshold() != n->GetObject<MehdiNode>()->Get_POI_TimeThreshold())
						return false;
					break;
				//positions
				case POSITIONS:
					if (m_positions.size() != n->GetObject<MehdiNode>()->GetPositions().size())
						return false;
					for (size_t i = 0; i < m_positions.size(); i++)
						if (!m_positions[i].IsEqual(n->GetObject<MehdiNode>()->GetPositions()[i]))
							return false;
					break;
				//calls
				case CALLS:
					v_calls = n->GetObject<MehdiNode>()->GetCalls();
					if (m_calls.size() != v_calls.size())
						return false;
					for (size_t i = 0; i < m_calls.size(); i++)
						if (!m_calls[i].IsEqual(v_calls[i]))
							return false;
					break;
				//POIs
				case POIS:
					v_POIs = n->GetObject<MehdiNode>()->GetPOIs();
					if (m_POIs.size() != v_POIs.size())
						return false;
					for (size_t i = 0; i < m_POIs.size(); i++)
						if (!m_POIs[i].IsEqual(v_POIs[i]))
							return false;
					break;
				//ZOIs
				case ZOIS:
					v_ZOIs = n->GetObject<MehdiNode>()->GetZOIs();
					if (m_ZOIs.size() != v_ZOIs.size())
						return false;
					for (size_t i = 0; i < m_ZOIs.size(); i++)
						if (!m_ZOIs[i].IsEqual(v_ZOIs[i]))
							return false;
					break;
				//ROIs
				case ROIS:
					v_ROIs = n->GetObject<MehdiNode>()->GetROIs();
					if (m_ROIs.size() != v_ROIs.size())
						return false;
					for (size_t i = 0; i < m_ROIs.size(); i++)
						if (!m_ROIs[i].IsEqual(v_ROIs[i]))
							return false;
					break;
				//location chains
				case LOCATION_CHAINS:
					v_location_chains = n->GetObject<MehdiNode>()->GetLocationChains();
					if (m_location_chains.size() != v_location_chains.size())
						return false;
					for (size_t i = 0; i < m_location_chains.size(); i++)
						if (!m_location_chains[i].IsEqual(v_location_chains[i]))
							return false;
					break;
				//location chains
				case PREDICTED_NEXT_LOCATION_CHAINS:
					v_predicted_next_location_chains = n->GetObject<MehdiNode>()->GetPredictedNextLocationChains();
					if (m_predicted_next_location_chains.size() != v_predicted_next_location_chains.size())
						return false;
					for (size_t i = 0; i < m_predicted_next_location_chains.size(); i++)
						if (!m_predicted_next_location_chains[i].IsEqual(v_predicted_next_location_chains[i]))
							return false;
					break;
				default:
					MEHDI_ERROR_LINE("undefined field in comparing mehdi nodes.");
					return false;
			}

		//temp begin
		cout << m_user_friendly_name << " matches." << endl;
		//temp end

		return true;
	}

	/*
	 * add a new position to the node
	 */
	bool AddPosition(coordinate x, coordinate y, elapsed_time time, bool is_weekday, bool is_day_switched, bool is_week_switched)
	{
		//check the validity of positon
		if (x == INVALID_COORDINATE || y == INVALID_COORDINATE || time == INVALID_TIME)
		{
			MEHDI_ERROR_LINE("adding invalid position to the node.");
			return false;
		}
		this->m_positions.push_back(Position(x, y, time, is_weekday, is_day_switched, is_week_switched));
		return true;
	}

	/*
	 * calculate the ZOIs of the node
	 */
	bool FindZOIs()
	{
        // merge POIs as much as possible to form the ZOIs and if the number of ZOIs and POIs are not that much different
        // change the merging threshold and do the merging again from the beginning

		bool result = false;

        Reset_ZOI_MergingThreshold();

        // at first ZOIs are just POIs
        m_ZOIs = m_POIs;

        //merging outer loop
        while (true)
        {
            // merge ZOIs as much as possible with the current merging threshold
            //merging inner loop
            while (true)
            {
                // keep the number of ZOIs before merge
                size_t numberOfZOIsBeforeMerge = m_ZOIs.size();
                // merge ZOIs
                MergeCentroids(m_ZOIs, Get_ZOI_MergingThreshold());
                // no more merging? break the inner loop
                if (numberOfZOIsBeforeMerge == m_ZOIs.size())
                    break;
            }
			// check if the ZOIs are too much
			if (m_ZOIs.size() > ZOI_to_POI_ratio * m_POIs.size())
			{
				// change the merging threshold (if possible)
				// and restore the backup version of ZOIs and do the merging process again
				if (UseNext_ZOI_MergingThreshold())
					m_ZOIs = m_POIs;
				// otherwise nothing could be done, give up and break the outer loop
				else
				{
					result = false;
					break;
				}
			}
			// not too much ZOIs. everything is ok. so no need for more merging
			// successful merging. break the outer loop.
			else
			{
				result = true;
				break;
			}
        }

        //do some polishing on found ZOIs
        uint zoiNumber = 0;
        for (size_t i = 0; i < m_ZOIs.size(); i++)
        {
            //update positions
            if (!m_ZOIs[i].UpdatePositions(m_positions, ZOI_number_of_members_threshold))
            {
            	//erase worthless ZOIs (ZOIs with members less than threshold)
            	m_ZOIs.erase(m_ZOIs.begin() + i);
            	continue;
            }
			//set the number
			zoiNumber += 1;
			//set the name
			ostringstream name;
			name << "zoi " << zoiNumber;
			m_ZOIs[i].SetName(name.str());
			//add calls
			m_ZOIs[i].ResetCalls();
			for (vector<Call>::iterator it = m_calls.begin(); it != m_calls.end(); it++)
				if (m_ZOIs[i].IncludesTime(it->GetTime()))
					m_ZOIs[i].AddCall(*it);
        }

        //BEGIN: report
        u_coordinate avg_ZOI_radius = 0;
        u_coordinate min_ZOI_radius = MAX_COORDINATE;
        u_coordinate max_ZOI_radius = 0;
        //find min, max, and avg radius
        for (vector<Centroid>::iterator it = m_ZOIs.begin(); it != m_ZOIs.end(); it++)
        {
        	avg_ZOI_radius += it->GetRadius();
        	if (it->GetRadius() < min_ZOI_radius)
        		min_ZOI_radius = it->GetRadius();
        	if (it->GetRadius() > max_ZOI_radius)
        		max_ZOI_radius = it->GetRadius();
        }
        //update avg radius
        if (m_ZOIs.size() > 0)
        	avg_ZOI_radius /= m_ZOIs.size();
        //print the report
        MEHDI_REPORT_LINE(m_user_friendly_name << ": " << m_ZOIs.size() << " ZOIs, ZOI merging threshold: " << Get_ZOI_MergingThreshold() << ", average radius: " << avg_ZOI_radius);
        //END: report

        return result;
	}

	/*
	 * calculate the ROIs of the node
	 */
	bool FindROIs()
	{
		bool result = false;

        // merge ZOIs as much as possible to form the ROIs and if the number of ROIs and ZOIs are not that much different
        // change the merging threshold and do the merging again from the beginning
        this->Reset_ROI_MergingThreshold();

        // at first ROIs are just ZOIs
        m_ROIs = m_ZOIs;

        //merging outer loop
        while (true)
        {
            // merge ROIs as much as possible with the current merging threshold
        	//merging inner loop
            while (true)
            {
                // keep the number of ROIs before merge
                size_t numberOfROIsBeforeMerge = m_ROIs.size();
                // merge ROIs
                MergeCentroids(m_ROIs, Get_ROI_MergingThreshold());
                // no more merging? break the inner loop
                if (numberOfROIsBeforeMerge == m_ROIs.size())
                    break;
            }
			// check if the ROIs are too much
			if (m_ROIs.size() > ROI_to_ZOI_ratio * m_ZOIs.size())
			{
				// change the merging threshold (if possible)
				// and restore the backup version of ROIs and do the merging process again
				if (UseNext_ROI_MergingThreshold())
					m_ROIs = m_ZOIs;
				// otherwise nothing could be done, give up and break the outer loop
				else
				{
					result = false;
					break;
				}
			}
			// not too much ROIs. everything is ok. so no need for more merging
			// successful merging. break the outer loop
			else
			{
				result = true;
				break;
			}
        }

        //do some polishing on the found ROIs
        uint roiNumber = 0;
        for (size_t i = 0; i < m_ROIs.size(); i++)
        {
            //update positions
            if (!m_ROIs[i].UpdatePositions(m_positions, ROI_number_of_members_threshold))
            {
            	//erase worthless ROIs. (ROIs whose number of members is less than threshold)
            	m_ROIs.erase(m_ROIs.begin() + i);
            	//no more polishing on the erased ROI
            	continue;
            }
            //set the number
            roiNumber += 1;
            //set the name
			ostringstream name;
			name << "roi " << roiNumber;
            m_ROIs[i].SetName(name.str());
            //add calls
            m_ROIs[i].ResetCalls();
			for (vector<Call>::iterator it = m_calls.begin(); it != m_calls.end(); it++)
				if (m_ROIs[i].IncludesTime(it->GetTime()))
					m_ROIs[i].AddCall(*it);
        }

		//BEGIN: report
		u_coordinate avg_ROI_radius = 0;
		u_coordinate min_ROI_radius = MAX_COORDINATE;
		u_coordinate max_ROI_radius = 0;
		//find min, max, and avg radius
		for (vector<Centroid>::iterator it = m_ROIs.begin(); it != m_ROIs.end(); it++)
		{
			avg_ROI_radius += it->GetRadius();
			if (it->GetRadius() < min_ROI_radius)
				min_ROI_radius = it->GetRadius();
			if (it->GetRadius() > max_ROI_radius)
				max_ROI_radius = it->GetRadius();
		}
		//update avg redius
		if (m_ROIs.size() > 0)
			avg_ROI_radius /= m_ROIs.size();
		//pring the report
		MEHDI_REPORT_LINE(m_user_friendly_name << ": " << m_ROIs.size() << " ROIs, ROI merging threshold: " << Get_ROI_MergingThreshold() << ", average radius: " << avg_ROI_radius);
		//END: report

		return result;
	}

	/*
	 * find chains of locations
	 */
	bool FindChainsOfLocations()
	{
		vector<Position> today_positions;		//array for positions of one day
		vector<vector<Position> > weekday_positions;	//array of arrays for positions of all kept weekdays (positions of each weekday will be in a sub-array)
		vector<vector<Position> > weekend_positions;	//array of arrays for positions of all kept weekends (positions of each weekend will be in a sub-array)
		vector<vector<Centroid> > weekday_pOIs;		//array of arrays for pOIs of all kept weekdays (pOIs for each weekday will be in a sub-array)
		vector<vector<Centroid> > weekend_pOIs;		//array of arrays for pOIs of all kept weekends (pOIs for each weekend will be in a sub-array)
		vector<Centroid> weekday_POIs;		//array for POIs of weekdays (no sub-array)
		vector<Centroid> weekend_POIs;		//array for POIs of weekends (no sub-array)
		LocationChain today_chain;
		LocationChain today_predicted_next_location_chain;

		m_location_chains.clear();
		m_predicted_next_location_chains.clear();

		//previousPosition will be set to current position when manipulating current position is done (at the end of the loop)
		Position previous_position = INVALID_POSITION;

		//previousPOI will be set to invalid centroid when day is switched so that prediction for the first POI of a day will not be performed
		Centroid previous_POI = Centroid(INVALID_CENTROID_NAME, 0);

		//traversing positions

		//mehdi temp begin
		MEHDI_REPORT_LINE(m_user_friendly_name << ", finding chains of locations ...");
		//mehdi temp end

		//in reality, this loop continues forever for updating pOIs and POIs for weekdays and weekends
		for (vector<Position>::iterator it = m_positions.begin(); it != m_positions.end() - 1; it++)
		{
			//for clarity
			Position current_position = *it;
			Position next_position = *(it + 1);


			//if previous position is not valid it means that this is the beginning. so this part will not be executed
			if (!previous_position.IsEqual(INVALID_POSITION))
			// only the previous position (at the bottom part) and current position (when returning to for loop) will be updated
			{
				//if no day switch only check for POI change
				if (!current_position.IsDaySwitched())
				{
					//still in the same day; so just add the position to the list of today positions
					today_positions.push_back(current_position);

					//initialize current POI
					Centroid current_POI = Centroid(INVALID_CENTROID_NAME, 0);

					//find the current POI
					//weekday
					if (previous_position.IsWeekday() && weekday_POIs.size() > 0)
						current_POI = Centroid::FindCentroidOfPosition(weekday_POIs, current_position);
					//weekend
					if (previous_position.IsWeekend() && weekend_POIs.size() > 0)
						current_POI = Centroid::FindCentroidOfPosition(weekend_POIs, current_position);

					//if current POI is found
					if (!current_POI.IsEqual(Centroid(INVALID_CENTROID_NAME,0)))
					{
						//current POI is a new POI?
						if (!current_POI.IsEqual(previous_POI))
						{
							//add it to the chain
							today_chain.AddLocation(Location(current_POI.GetX(), current_POI.GetY(), current_position.GetTime(), next_position.GetTime()));
							//predict the next location and add it to the prediction chain
							Location predicted_next_location = INVALID_LOCATION;
							Centroid predicted_next_POI = current_POI.NextProbableCentroid();
							if (predicted_next_POI.GetName() != EMPTY_CENTROID_NAME)
								predicted_next_location = Location(predicted_next_POI.GetX(), predicted_next_POI.GetY(), current_position.GetTime(), current_position.GetTime() + current_position.GetTime() + mehdi_validity_time_of_predicted_next_location);
							today_predicted_next_location_chain.AddLocation(predicted_next_location);
							//update the previous POI
							previous_POI = current_POI;
						}
						//it is the same as previous POI
						else
						{
							//update the leaving time of the locations
							today_chain.UpdateLeavingTime(current_position.GetTime());
							today_predicted_next_location_chain.UpdateLeavingTime(current_position.GetTime() + mehdi_validity_time_of_predicted_next_location);
						}
					}
				}
				//another day has begun so calculations of pOIs will be done for positions until previous position
				//then a new array will be created for today positions which contains the current position
				//also if week is switched too, calculations of POIs will be done both for weekdays and weekends
				//the today chain of locations will be added to the total chain too
				else
				{
					//add today chain to the total chain
					m_location_chains.push_back(today_chain);
					m_predicted_next_location_chains.push_back(today_predicted_next_location_chain);
					//prepare for updating accuracy of prediction
					vector<Location> real_locations = today_chain.GetChainVector();
					vector<Location> predicted_locations = today_predicted_next_location_chain.GetChainVector();
					//clear the chain for the next day
					today_chain.Clear();
					today_predicted_next_location_chain.Clear();

					//pOI calculation

					//weekdays?
					if (previous_position.IsWeekday())
					{
						//add today positions to the array of weekday positions
						weekday_positions.push_back(today_positions);
						//daily pOIs should be calculated on weekdays
						weekday_pOIs.push_back(Centroid::Find_pOIs(today_positions, pOI_distance_threshold, pOI_time_threshold));
						//TODO: different numbers should be tested and one selected
						//only 5 weeks are kept
						while (weekday_pOIs.size() > 5 * 5)
						{
							weekday_pOIs.erase(weekday_pOIs.begin());
							weekday_positions.erase(weekday_positions.begin());
						}
						//update accuracy of prediction
						for (uint i = 0; i < predicted_locations.size(); i++)
						{
							if (i + 1 < real_locations.size())
							{
								vector<Centroid>::iterator it;
								//find the POI of the current real location
								for (it = weekday_POIs.begin(); it != weekday_POIs.end(); it++)
									if (it->GetX() == real_locations[i].GetX() && it->GetY() == real_locations[i].GetY())
										break;
								//if POI found
								if (it != weekday_POIs.end())
								{
									it->totalPredictions++;
									//check the validity of prediction
									if (predicted_locations[i].GetX() == real_locations[i + 1].GetX() && predicted_locations[i].GetY() == real_locations[i + 1].GetY())
										it->truePredictions++;
								}
							}
						}
					}
					//weekends?
					else
					{
						//add today positions to the array of weekend positions
						weekend_positions.push_back(today_positions);
						//weekend pOIs should be calculated on weekends
						weekend_pOIs.push_back(Centroid::Find_pOIs(today_positions, pOI_distance_threshold, pOI_time_threshold));
						//TODO: different numbers should be tested and one selected
						//data is kept for only 5 weekends (5 times 2 weekend days)
						while (weekend_pOIs.size() > 5 * 2)
						{
							weekend_pOIs.erase(weekend_pOIs.begin());
							weekend_positions.erase(weekend_positions.begin());
						}
						//update accuracy of prediction
						for (uint i = 0; i < predicted_locations.size(); i++)
						{
							if (i + 1 < real_locations.size())
							{
								vector<Centroid>::iterator it;
								//find the POI of the current real location
								for (it = weekend_POIs.begin(); it != weekend_POIs.end(); it++)
									if (it->GetX() == real_locations[i].GetX() && it->GetY() == real_locations[i].GetY())
										break;
								//if POI found
								if (it != weekend_POIs.end())
								{
									it->totalPredictions++;
									//check the validity of prediction
									if (predicted_locations[i].GetX() == real_locations[i + 1].GetX() && predicted_locations[i].GetY() == real_locations[i + 1].GetY())
										it->truePredictions++;
								}
							}
						}
					}

					//a new day has begun
					today_positions.clear();
					today_positions.push_back(current_position);

					//it is the beginning of the day so there is no previous POI
					// (so the prediction for the first POI will not be made in the prediction part)
					previous_POI = Centroid(INVALID_CENTROID_NAME, 0);

					//POI calculation

					//also calculate POIs if week is switched

					if (current_position.IsWeekSwitched())
					{
						//weekday POIs
						weekday_POIs = Centroid::Find_POIs(weekday_pOIs, weekday_positions);
						//weekend POIs
						weekend_POIs = Centroid::Find_POIs(weekend_pOIs, weekend_positions);
					}
				}
			}
			//go for the next position
			previous_position = current_position;
		}
		return true;
	}

	/*
	 *
	 */
	vector<LocationChain> GetLocationChains() const
	{
		return m_location_chains;
	}

	/*
	 *
	 */
	vector<LocationChain> GetPredictedNextLocationChains() const
	{
		return m_predicted_next_location_chains;
	}

	/*
	 *
	 */
	LocationChain GetLocationChainForToday(elapsed_time simulation_now)
	{
		elapsed_time real_now = simulation_now + mehdi_simulation_starting_time;
		int index = -1;
		//first option is the current chain
		if (m_location_chains[m_current_loc_chain_index].BelognsToThisDay(real_now))
			index = m_current_loc_chain_index;
		//second option is the next chain
		else if (m_current_loc_chain_index + 1 < m_location_chains.size() && m_location_chains[m_current_loc_chain_index + 1].BelognsToThisDay(real_now))
				index = ++m_current_loc_chain_index;
		//we have to search for the chain
		else
		{
			LocationChain chain = LocationChain();
			chain.AddLocation(Location(0,0,real_now,real_now));
			vector<LocationChain>::iterator it = lower_bound(m_location_chains.begin(), m_location_chains.end(), chain, LocationChainComparatorRegardingEndTime());
			if (it != m_location_chains.end() && it->GetStartTime() <= real_now)
			{
				m_current_loc_chain_index = distance(m_location_chains.begin(), it);
				index = m_current_loc_chain_index;
			}
		}
		if (index >= 0)
		{
			//real data
			if (Parameters::real_next_locations)
				return m_location_chains[m_current_loc_chain_index].GetTimeShiftedLocationChain(mehdi_simulation_starting_time);
			//prediction data
			else
			{
				LocationChain chain = LocationChain();
				vector<Location> today_chain = m_location_chains[m_current_loc_chain_index].GetChainVector();
				vector<Location> today_predicted_next_location_chain = m_predicted_next_location_chains[m_current_loc_chain_index].GetChainVector();

				//find current location in the today chain
				uint index;
				for (index = 0; index < today_chain.size(); index++)
					if (today_chain[index].GetEnteringTime() <= real_now && real_now <= today_chain[index].GetLeavingTime())
						break;
				if (index < today_chain.size())
				{
					//add current location
					chain.AddLocation(today_chain[index]);
					//add the predicted next location if it is valid
					if (today_predicted_next_location_chain[index].GetEnteringTime() != INVALID_TIME)
						chain.AddLocation(today_predicted_next_location_chain[index]);
					return chain.GetTimeShiftedLocationChain(mehdi_simulation_starting_time);
				}
			}
		}
		//sorry! no chain found
		return LocationChain();
	}

	/*
	 *
	 */
	Location GetCurrentLocation(elapsed_time simulation_now)
	{
		vector<Location>today_chain = GetLocationChainForToday(simulation_now).GetChainVector();

		for (vector<Location>::iterator it = today_chain.begin(); it != today_chain.end(); it++)
			if (it->GetEnteringTime() <= simulation_now && simulation_now <= it->GetLeavingTime())
				return *it;
		return INVALID_LOCATION;
	}

private:
	bool BelongsToAnyPOI(const Position& position)
	{
		for (vector<Centroid>::iterator it = m_POIs.begin(); it != m_POIs.end(); it++)
			if (it->IsPositionInside(position))
				return true;
		return false;
	}

	bool FindPositionPOI(Centroid &poi, const Position& position)
	{
		for (size_t i = 0; i < m_POIs.size(); i++)
			if (m_POIs[i].IsPositionInside(position))
			{
				poi = m_POIs[i];
				return true;
			}
		poi = Centroid(EMPTY_CENTROID_NAME, 0);
		return false;
	}

	Location CreateLocation(Centroid POI, elapsed_time t)
	{
		return Location(POI.GetX(), POI.GetY(), POI.GetEnteringTime(t), POI.GetLeavingTime(t));
	}

	/*
	 * sets the original name
	 */
	bool SetOriginalName(string name)
	{
		m_original_name = name;
		return true;
	}

	/*
	 * sets the user friendly name
	 */
	bool SetUserFriendlyName(string name)
	{
		m_user_friendly_name = name;
		return true;
	}

	/*
	 * sets the position vector
	 */
	bool SetPositions(vector<Position> v)
	{
		m_positions = v;
		return true;
	}

	/*
	 * sets the call vector
	 */
	bool SetCalls(vector<Call> v)
	{
		m_calls = v;
		return true;
	}

	/*
	 * sets the POI distance threshold
	 */
	bool Set_POI_DistanceThreshold(coordinate threshold)
	{
		//find size of range array
		uint size = sizeof(POI_distance_threshold_range) / sizeof(POI_distance_threshold_range[0]);
		//find the threshold index in the range array
		for (uint i = 0; i < size; i++)
			if (POI_distance_threshold_range[i] == threshold)
			{
				//set the index
				m_POI_distance_threshold_index = i;
				return true;
			}
		//the passed threshold could not be found in the range array
		return false;
	}

	/*
	 * sets the merging threshold for POIs
	 */
	bool Set_POI_MergingThreshold(coordinate threshold)
	{
		//find number of available thresholds
		uint size = sizeof(POI_merging_threshold_range) / sizeof(POI_merging_threshold_range[0]);
		//look for the requested threshold in the available thresholds
		for (uint i = 0; i < size; i++)
			if (POI_merging_threshold_range[i] == threshold)
			{
				//threshold found
				m_POI_merging_threshold_index = i;
				return true;
			}
		//requested threshold is not in the available thresholds list
		return false;
	}

	/*
	 * set the merging threshold for ZOIs
	 */
	bool Set_ZOI_MergingThreshold(coordinate threshold)
	{
		//find number of available thresholds
		uint size = sizeof(ZOI_merging_threshold_range) / sizeof(ZOI_merging_threshold_range[0]);
		//look for the requested threshold in the available thresholds
		for (uint i = 0; i < size; i++)
			if (ZOI_merging_threshold_range[i] == threshold)
			{
				//found
				m_ZOI_merging_threshold_index = i;
				return true;
			}
		//the requested threshold is not in the available thresholds
		return false;
	}

	/*
	 * set the merging threshold for ROIs
	 */
	bool Set_ROI_MergingThreshold(coordinate threshold)
	{
		//find number of available thresholds
		uint size = sizeof(ROI_merging_threshold_range) / sizeof(ROI_merging_threshold_range[0]);
		//look for reqested threshold in the available thresholds
		for (uint i = 0; i < size; i++)
			if (ROI_merging_threshold_range[i] == threshold)
			{
				//found
				m_ROI_merging_threshold_index = i;
				return true;
			}
		//the requested threshold is not in the avialable thresholds list
		return false;
	}

	/*
	 * set the time threshold for POIs
	 */
	bool Set_POI_TimeThreshold(elapsed_time threshold)
	{
		//find the size of available thresholds
		uint size = sizeof(POI_time_threshold_range) / sizeof(POI_time_threshold_range[0]);
		//look for the requested threshold in the available threshold list
		for (uint i = 0; i < size; i++)
			if (POI_time_threshold_range[i] == threshold)
			{
				//found
				m_POI_time_threshold_index = i;
				return true;
			}
		//the requested threshold is not in the available threshold list
		return false;
	}

	/*
	 * returns the current distance threshold which absorbs positions
	 */
	coordinate Get_POI_DistanceThreshold() const
	{
		return POI_distance_threshold_range[m_POI_distance_threshold_index];
	}

	/*
	 * returns the current time threshold which makes the POI valid
	 */
	elapsed_time Get_POI_TimeThreshold() const
	{
		return POI_time_threshold_range[m_POI_time_threshold_index];
	}

	/*
	 * returns the current merging threshold for merging POIs
	 */
	coordinate Get_POI_MergingThreshold() const
	{
		return POI_merging_threshold_range[m_POI_merging_threshold_index];
	}

	/*
	 * returns the current merging threshold for merging ZOIs
	 */
	coordinate Get_ZOI_MergingThreshold() const
	{
		return ZOI_merging_threshold_range[m_ZOI_merging_threshold_index];
	}

	/*
	 * returns the current merging threshold for merging ROIs
	 */
	coordinate Get_ROI_MergingThreshold() const
	{
		return ROI_merging_threshold_range[m_ROI_merging_threshold_index];
	}

	/*
	 * updates the distance and time thresholds
	 */
	bool UseNextDistanceTimeThreshold()
	{
		// if not already the last distance threshold use the next one
		if (m_POI_distance_threshold_index < ((sizeof(POI_distance_threshold_range) / sizeof(POI_distance_threshold_range[0])) - 1))
		{
			m_POI_distance_threshold_index += 1;
			return true;
		}
		// if already the last distance threshold, if not the last date threshold go to the next
		//  date threshold and start distance threshold from the beginning again
		else if (m_POI_time_threshold_index < ((sizeof(POI_time_threshold_range) / sizeof(POI_time_threshold_range[0])) - 1))
		{
			m_POI_time_threshold_index += 1;
			m_POI_distance_threshold_index = 0;
			return true;
		}
		// sorry, this is the end and nothing could be done
		return false;
	}

	/*
	 * updates the POI merging threshold
	 */
	bool UseNext_POI_MergingThreshold()
	{
		//any more threshold is avaiable?
		if (m_POI_merging_threshold_index < ((sizeof(POI_merging_threshold_range) / sizeof(POI_merging_threshold_range[0])) - 1))
		{
			//yes, choose next threshold
			m_POI_merging_threshold_index += 1;
			return true;
		}
		//sorry, no more threshold is available
		return false;
	}

	/*
	 * updates the ZOI merging threshold
	 */
	bool UseNext_ZOI_MergingThreshold()
	{
		//any more threshold is available?
		if (m_ZOI_merging_threshold_index < ((sizeof(ZOI_merging_threshold_range) / sizeof(ZOI_merging_threshold_range[0])) - 1))
		{
			//yes, use the next threshold
			m_ZOI_merging_threshold_index += 1;
			return true;
		}
		//sorry, no more threshold is available
		return false;
	}

	/*
	 * updates the ROI merging threshold
	 */
	bool UseNext_ROI_MergingThreshold()
	{
		//any more threshold is available?
		if (m_ROI_merging_threshold_index < ((sizeof(ROI_merging_threshold_range) / sizeof(ROI_merging_threshold_range[0])) - 1))
		{
			//yes, use the next threshold
			m_ROI_merging_threshold_index += 1;
			return true;
		}
		//sorry, no more threshold is available
		return false;
	}

	/*
	 * resets the POI merging threshold
	 */
	void Reset_POI_MergingThreshold()
	{
		m_POI_merging_threshold_index = 0;
	}

	/*
	 * resets the ZOI merging threshold
	 */
	void Reset_ZOI_MergingThreshold()
	{
		m_ZOI_merging_threshold_index = 0;
	}

	/*
	 * resets the ROI merging threshold
	 */
	void Reset_ROI_MergingThreshold()
	{
		m_ROI_merging_threshold_index = 0;
	}

	/*
	 * resets all thresholds
	 */
	void ResetAllThresholds()
	{
		m_POI_distance_threshold_index = 0;
		m_POI_time_threshold_index = 0;
		Reset_POI_MergingThreshold();
		Reset_ZOI_MergingThreshold();
		Reset_ROI_MergingThreshold();
	}

	/*
	 * merges centroids which are closer than the threshold
	 * the calling function should repeat calling this function until no more merging is possible
	 */
	void MergeCentroids(vector<Centroid>& centroids, u_coordinate mergingThreshold)
	{
        int i = 0;
        // because merged POIs will be deleted from array we cannot use the usual 'for' loop
        int n = centroids.size();
        // while not the centroid before last is reached
        while ( i < n - 1 )
        {
            // start from the next centroid
            int j = i + 1;
            // while not end of array reached
            while ( j < n )
            {
                //how much far are the centers?
                u_coordinate distance = Position::Distance(centroids[i].GetX(), centroids[i].GetY(), centroids[j].GetX(), centroids[j].GetY());
                // either if the two POIs near enough or if they have intersection
                if ( distance <= mergingThreshold || distance < centroids[i].GetRadius() + centroids[j].GetRadius() )
                {
                    // merge them
                    centroids[i].MergeWithCentroid(centroids[j]);
//                    //add the positions which are in between
//                     for position in self.positions:
//                         if not position in centroids[i]._positions and centroids[i].includesPosition(position):
//                             centroids[i].addPosition(position)
                    // delete the second one
                    centroids.erase(centroids.begin() + j);
                    // decrease the length of the array
                    n -= 1;
                }
                else
					// next second centroid
					j += 1;
            }
            // next first centroid
            i += 1;
        }
	}

}; //class MehdiNode

}  //namespace mehdi
}  //namespace ndn
} //namespace ns3

#endif
