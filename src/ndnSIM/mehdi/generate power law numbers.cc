#include <iomanip>
#include <random>
#include <map>
#include <iostream>
#include <math.h>
#include <fstream>

using namespace std;

int x0 = 1;
int x1 = 101;
double n = 2.1;

int power_law(int x0, int x1, double n)
{
//	double y = ((double) rand() / (RAND_MAX));
//	double d1 = (pow(x1,n+1) - pow(x0,n+1))*y;
//	double d2 = pow(x0,n+1);
//	return (int)pow(d1 + d2, 1/(n+1));
}

void fill_array()
{
	map<int, int> result_set;

	for (int i =0; i < 100000; ++i)
		++result_set[power_law(x0,x1,n)]; 

	for (auto& v : result_set) 
	{
		cout << v.first << " -> " << string (v.second / 10, '.') << endl;
		if (v.second == 0)
		  break;
	}
}

void fill_file()
{
	ofstream fout;
	fout.open("power_law.txt");
	for (int i =0; i < 100000; ++i)
	{
		int number = power_law(x0,x1,n);
		fout << number << endl;
	}
	fout.close();
}

int
main (int argc, char *argv[])
{
	fill_array();
	return 0;
}
