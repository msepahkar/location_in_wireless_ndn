/*
 * ndn-mehdi-position_interval_call.h
 *
 *  Created on: Feb 2, 2016
 *      Author: mehdi
 */

#ifndef NDN_MEHDI_POSITION_INTERVAL_CALL_H_
#define NDN_MEHDI_POSITION_INTERVAL_CALL_H_

#include <stdint.h>
#include <string>

#include "src/ndnSIM/mehdi/ndn-mehdi-basic-definitions.h"
#include "src/ndnSIM/mehdi/ndn-mehdi-tools.h"

namespace ns3 {
namespace ndn {
namespace mehdi {

using namespace std;

//for read/write of binary files
//enum data_type_enum {STRING, INT32_T, UINT, SIZE_T, V_POSITIONS, V_INTERVALS, V_CALLS};

//////////////////////////////
// class Position
/////////////////////////////
class Position
{

private:
	coordinate m_x;
	coordinate m_y;
	//time in which this position is recorded
	elapsed_time m_time;
	//shows if this position belongs to a weekday
	bool m_is_weekday;
	//shows if this position is the first one in a new day
	bool m_is_day_switched;
	//shows if this position is the first one in a new week
	bool m_is_week_switched;

public:
	Position(coordinate x, coordinate y, elapsed_time t, bool is_weekday, bool is_day_switched, bool is_week_switched)
	{
		m_x = x;
		m_y = y;
		m_time = t;
		m_is_weekday = is_weekday;
		m_is_day_switched = is_day_switched;
		m_is_week_switched = is_week_switched;
	}
	//is required for reading vector of positions from binary file
	Position()
	{
		m_x = INVALID_COORDINATE;
		m_y = INVALID_COORDINATE;
		m_time = INVALID_TIME;
		m_is_weekday = false;
		m_is_day_switched = false;
		m_is_week_switched = false;
	}

	/*
	 * calculates distance between two points.
	 * the order of parameters should be noted when using this function
	 */
	static u_coordinate Distance(coordinate x1, coordinate y1, coordinate x2,	coordinate y2)
	{
		//check for invalid coordinates
		if (x1 == INVALID_COORDINATE || y1 == INVALID_COORDINATE || x2 == INVALID_COORDINATE || y2 == INVALID_COORDINATE)
		{
			MEHDI_ERROR_LINE("invalid coordinate in distance function of position class.");
			return INVALID_COORDINATE;
		}
		//calculate the distance
		coordinate xdiff = abs(x2 - x1);
		coordinate ydiff = abs(y2 - y1);
		return (coordinate) sqrt((xdiff << 1) + (ydiff << 1));
	}

	/*
	 * checks if two points are farther from each other than the limit
	 * it makes use of the distance function
	 */
	static bool FartherThan(coordinate x1, coordinate y1, coordinate x2, coordinate y2, u_coordinate limit)
	{
		if (Distance(x1, y1, x2, y2) > limit)
			return true;
		return false;
	}

	/*
	 * gets the x coordinate of the position
	 */
	coordinate GetX() const
	{
		return m_x;
	}

	/*
	 * gets the y coordinate of the position
	 */
	coordinate GetY() const
	{
		return m_y;
	}

	/*
	 * gets the time at which this position is recorded.
	 */
	elapsed_time GetTime() const
	{
		return m_time;
	}

	/*
	 * sets the time at which this position is recorded.
	 */
	bool SetTime(elapsed_time t)
	{
		m_time = t;
		return true;
	}

	/*
	 * returns true if presence in this position has been in weekday
	 */
	bool IsWeekday() const
	{
		return m_is_weekday;
	}

	/*
	 * returns true if presence in this position has been in weekend
	 */
	bool IsWeekend() const
	{
		return !m_is_weekday;
	}

	/*
	 * returns true if this is the first position recorded in a new day
	 */
	bool IsDaySwitched() const
	{
		return m_is_day_switched;
	}

	/*
	 * returns true if this is the first position recorded in a new week
	 */
	bool IsWeekSwitched() const
	{
		return m_is_week_switched;
	}

	/*
	 * checks if this position is equal to the position passed to it
	 * it compares all of the required fields
	 */
	bool IsEqual(Position p) const
	{
		//x
		if (m_x != p.GetX())
			return false;
		//y
		if (m_y != p.GetY())
			return false;
		//time
		if (m_time != p.GetTime())
			return false;
		//weekday
		if (m_is_weekday != p.IsWeekday())
			return false;
		//day switched
		if (m_is_day_switched != p.IsDaySwitched())
			return false;
		//week switched
		if (m_is_week_switched != p.IsWeekSwitched())
			return false;

		//all match
		return true;
	}
}; //class Position

//////////////////////////////
// struct for comparing time of positions
// required for sorting positions
/////////////////////////////
struct PositionComparatorRegardingTime
{
    bool operator()( const Position& p1, const Position& p2 ) const {
    	return p1.GetTime() < p2.GetTime();
    }
};

//////////////////////////////
// class Interval
/////////////////////////////
class Interval
{

private:
	//first recorded time of the interval
	elapsed_time m_first_time_after_enter;
	//last recorded time of the interval
	elapsed_time m_last_time_before_leave;

public:
	Interval(elapsed_time first_time_after_enter, elapsed_time last_time_before_leave)
	{
		m_first_time_after_enter = first_time_after_enter;
		m_last_time_before_leave = last_time_before_leave;
	}
	//required for reading vector of intervals from binary file
	Interval()
	{
		m_first_time_after_enter = INVALID_TIME;
		m_last_time_before_leave = INVALID_TIME;
	}

	/*
	 * duration of the interval
	 */
	elapsed_time Duration() const{
		//check if timing is ok
		if (m_last_time_before_leave <= m_first_time_after_enter)
			return 0;
		return m_last_time_before_leave - m_first_time_after_enter;
	}

	/*
	 * returns the first recorded time of the interval
	 */
	elapsed_time GetEnteringTime() const
	{
		return m_first_time_after_enter;
	}

	/*
	 * returns the last recorded time of the interval
	 */
	elapsed_time GetLeavingTime() const
	{
		return m_last_time_before_leave;
	}

	/*
	 * checks if the time 't' is in this duration
	 */
	bool IncludesTime(elapsed_time t) const
	{
		if ( m_first_time_after_enter <= t && t <= m_last_time_before_leave )
			return true;
		return false;
	}

	/*
	 * sets the first recorded time of the interval
	 */
	void SetEnteringTime(elapsed_time enteringTime)
	{
		m_first_time_after_enter = enteringTime;
	}

	/*
	 * sets the last recorded time of the interval
	 */
	void SetLeavingTime(elapsed_time leavingTime) {
		m_last_time_before_leave = leavingTime;
	}

	/*
	 * checks if this interval is equal to the one passed to it
	 */
	bool IsEqual(Interval i) const
	{
		if (m_first_time_after_enter != i.GetEnteringTime())
			return false;
		if (m_last_time_before_leave != i.GetLeavingTime())
			return false;

		//all match
		return true;
	}

	/*
	 * comparison of intervals based on entering time
	 * required for sort
	 */
	bool operator<(Interval interval)
	{
		return m_first_time_after_enter < interval.GetEnteringTime();
	}

	/*
	 * comparison of intervals based on entering time in reverse order
	 * required for reverse sort
	 */
	struct ReverseCompare {
		bool operator()(const Interval& i1, const Interval& i2) const
		{
			return i1.GetEnteringTime() > i2.GetEnteringTime();
		}
	};
}; //class Interval

//////////////////////////////
// class Call
/////////////////////////////
class Call {

private:
	//name of the partner of call
	string m_name;
	//time of call
	elapsed_time m_time;

public:
	Call(string name, uint32_t time) {
		m_name = name;
		m_time = time;
	}
	//is required for reading vector of calls from binary file
	Call()
	{
		m_name = NOBODY;
		m_time = INVALID_TIME;
	}

	/*
	 * gets the name of the partner
	 */
	string GetName() const
	{
		return m_name;
	}

	/*
	 * gets the time of call
	 */
	elapsed_time GetTime() const
	{
		return m_time;
	}

	/*
	 * checks if this call is equal to the one passed to it
	 */
	bool IsEqual(Call c) const
	{
		//name
		if (m_name != c.GetName())
			return false;
		//time
		if (m_time != c.GetTime())
			return false;

		//all match
		return true;
	}
};//class Call

}//namespace mehdi
}//namespace ndn
}//namespace ns3

#endif /* NDN_MEHDI_POSITION_INTERVAL_CALL_H_ */
