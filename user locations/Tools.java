/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mehdi
 */
public class Tools {

    //the date time format
    static String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    static String firstDateString = "0001-01-01 00:00:00";
    static String lastDateString = "9999-12-30 24:59:59";
    static Date firstDate = null;   //should be calculated from data
    static Date lastDate = null;    //should be calculated from data
    //the name of the movements file
    static String movementsFileName = "Location.csv";
    //path for new movement files
    static String newMovementFilesPath = "movements/";
    //parameters regarding the movements file fromat
    static char separatingChar = ',';
    static char quotationChar = '"';
    static int numberOfLinesToSkip = 1;
    //parameters for each movement in new format
    static int numberOfFieldsInEachMovement = 10;
    static int rankIndex = 0;
    static int userFriendlyNameIndex = 1;
    static int originalNameIndex = 2;
    static int dateTimeIndex = 3;
    static int xIndex = 4;
    static int yIndex = 5;
    static int elapsed_seconds_index = 6;
    static int is_weekday_index = 7;
    static int is_day_switched = 8;
	static int is_week_switched = 9;
    //the dictionary of users 
    // in the form of Map<user, list of movements>
    // and each movement is an array of three strings:
    //  time, x, y
    static Map<String, List<String[]>> users = null;
    static List<Integer> numberOfMovementsList = null;

    public static boolean readMovements() {
        System.out.println("reading the original movements file ...");
        //Map<user,list of movements>
        Tools.users = new HashMap<>();
        try {
            FileReader file = new FileReader(Tools.movementsFileName);
            CSVReader reader = new CSVReader(file, Tools.separatingChar, Tools.quotationChar, Tools.numberOfLinesToSkip);
            //Read CSV line by line and use the string array as you want
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                if (nextLine != null) {
                    String user = nextLine[0].trim();
                    String dateTime = nextLine[1].trim();
                    //field 2 is accuracy which is not used
                    String x = nextLine[3].trim();
                    String y = nextLine[4].trim();
                    //ignore invalid movements
                    if (x.isEmpty() || y.isEmpty() || x.equals("Inf") || y.equals("Inf")) {
                        continue;
                    }
                    //create a new movement
                    String[] newMovement = new String[numberOfFieldsInEachMovement];
                    newMovement[Tools.dateTimeIndex] = dateTime;
                    newMovement[Tools.xIndex] = x;
                    newMovement[Tools.yIndex] = y;
                    //if the user exists add the new movement
                    if (Tools.users.containsKey(user)) {
                        Tools.users.get(user).add(newMovement);
                    } //otherwise create the user and add the new movement
                    else {
                        List<String[]> newMovementList = new ArrayList<>();
                        newMovementList.add(newMovement);
                        Tools.users.put(user, newMovementList);
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println("problem reading the movement file\n" + ex.getMessage());
            return false;
        }
        System.out.println("movments read.");
        //sort the movements based on time
        int i = 0;
        for (List<String[]> movements : Tools.users.values()) {
            System.out.printf("Sorting user %d ...\n", ++i);
            Collections.sort(movements, (String[] m1, String[] m2) -> m1[Tools.dateTimeIndex].compareTo(m2[Tools.dateTimeIndex]));
        }
        return true;
    }

    //find the first date
    public static boolean findFirstAndLastDates() {
        if (Tools.users == null) {
            System.out.println("users not read from the file yet");
            return false;
        }
        System.out.println("finding limits of time ...");

        DateFormat formatter = new SimpleDateFormat(Tools.dateTimeFormat);
        try {
            firstDate = (Date) formatter.parse(Tools.lastDateString);
            lastDate = (Date) formatter.parse(Tools.firstDateString);
        } catch (Exception ex) {
            System.out.println("problem converting first or last day.\n" + ex.getMessage());
            firstDate = null;
            lastDate = null;
            return false;
        }
        for (String userName : Tools.users.keySet()) {
            for (String[] movement : Tools.users.get(userName)) {
                try {
                    Date date = (Date) formatter.parse(movement[Tools.dateTimeIndex]);
                    if (date.compareTo(firstDate) < 0) {
                        firstDate = date;
                    }
                    if (date.compareTo(lastDate) > 0) {
                        lastDate = date;
                    }
                } catch (Exception ex) {
                    System.out.println("problem converting date time\n" + ex.getMessage());
                    firstDate = null;
                    lastDate = null;
                    return false;
                }
            }
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(firstDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        firstDate = cal.getTime();
        
        System.out.format("The minimum date is : %s\n", firstDate);
        System.out.format("The maximum date is : %s\n", lastDate);
        return true;
    }

    public static boolean calcualteOtherFields() {
        if (Tools.firstDate == null || Tools.lastDate == null) {
            System.out.println("limits of time have not been calculated yet.");
            return false;
        }
        System.out.println("finding other fields of movements ...");
        
        //create an array containig number of movements of users
        numberOfMovementsList = new ArrayList<>();
        Tools.users.keySet().stream().forEach((userName) -> {
            numberOfMovementsList.add(users.get(userName).size());
        });
        //sort the array
        Collections.sort(numberOfMovementsList);
        Collections.reverse(numberOfMovementsList);

        DateFormat formatter = new SimpleDateFormat(Tools.dateTimeFormat);
        for (String userName : Tools.users.keySet()) {
            String[] previous_movement = null;
            for (String[] movement : Tools.users.get(userName)) {
                movement[rankIndex] = String.valueOf(numberOfMovementsList.indexOf(users.get(userName).size()) + 1);
                movement[userFriendlyNameIndex] = "user " + movement[rankIndex];
                movement[originalNameIndex] = userName;
                try {
                    //elapsed seconds
                    Date date = (Date) formatter.parse(movement[Tools.dateTimeIndex]);
                    movement[elapsed_seconds_index] = String.valueOf((date.getTime() - firstDate.getTime()) / 1000);
                    //is day of week
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(date);
                    int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
                    if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
                        movement[is_weekday_index] = "0";
                    } else {
                        movement[is_weekday_index] = "1";
                    }
                    //is day switched
                    if (previous_movement != null)
                    {
						//prepare previous calendar
                        Date previousDate = (Date) formatter.parse(previous_movement[dateTimeIndex]);
                        Calendar previousCal = Calendar.getInstance();
                        previousCal.setTime(previousDate);

						//day switched or not?
                        if (cal.get(Calendar.DAY_OF_MONTH) == previousCal.get(Calendar.DAY_OF_MONTH) && cal.get(Calendar.MONTH) == previousCal.get(Calendar.MONTH))
                            movement[is_day_switched] = "0";
                        else
                            movement[is_day_switched] = "1";

						//week switched or not?
                        if (cal.get(Calendar.WEEK_OF_YEAR) == previousCal.get(Calendar.WEEK_OF_YEAR))
                            movement[is_week_switched] = "0";
                        else
                            movement[is_week_switched] = "1";
                    }
                    else
					{
                        movement[is_day_switched] = "0";
						movement[is_week_switched] = "0";
					}
                } catch (Exception ex) {
                    System.out.println("problem calculating other fields of movements.\n" + ex.getMessage());
                    return false;
                }
                previous_movement = movement;
            }
        }

        return true;
    }
    
    public static boolean addTagsToFields()
    {
        System.out.println("adding tags to fields ...");
        for (String userName : Tools.users.keySet()) {
            for (String[] movement : Tools.users.get(userName)) {
                movement[rankIndex] = "rank:" + movement[rankIndex];
                movement[userFriendlyNameIndex] = "user_friendly_name:" + movement[userFriendlyNameIndex];
                movement[originalNameIndex] = "original_name:" + movement[originalNameIndex];
                movement[dateTimeIndex] = "date_time:" + movement[dateTimeIndex];
                movement[xIndex] = "x:" + movement[xIndex];
                movement[yIndex] = "y:" + movement[yIndex];
                movement[elapsed_seconds_index] = "elapsed_seconds:" + movement[elapsed_seconds_index];
                movement[is_weekday_index] = "is_weekday:" + movement[is_weekday_index];
                movement[is_day_switched] = "is_day_switched:" + movement[is_day_switched];
				movement[is_week_switched] = "is_week_switched:" + movement[is_week_switched];
            }
        }
        return true;
    }

    public static boolean writeNewMovements() {

        ////report
        System.out.println("writing to the file ...");
        ////report
        int i = 0;

        for (String userName : Tools.users.keySet()) {
            String fileName = newMovementFilesPath + users.get(userName).get(0)[userFriendlyNameIndex].replace("user_friendly_name:", "") + ".csv";
            try (FileWriter fileWriter = new FileWriter(fileName)) {
                CSVWriter writer = new CSVWriter(fileWriter);

                ////report
                i++;
                ////report
                System.out.println("writing user " + i);
                ////report
                int j = 0;

                for (String[] movement : Tools.users.get(userName)) {
                    if (movement[elapsed_seconds_index] == null || movement[elapsed_seconds_index].isEmpty()) {
                        System.out.println("wrong elapsed time!");
                        return false;
                    }
                    writer.writeNext(movement);

                    ////report
                    j++;
                }

                ////report
                System.out.println(j + " movements written for " + userName);
            } catch (Exception ex) {
                System.out.println("problem writing to the file.\n" + ex.getMessage());
                return false;
            }
        }

        return true;
    }
}
