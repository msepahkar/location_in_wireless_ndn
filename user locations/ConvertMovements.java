/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;
 
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

/**
 *
 * @author mehdi
 */
public class ConvertMovements {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        if (Tools.readMovements())
        {
            System.out.println("movements read successfully.");
            if (Tools.findFirstAndLastDates())
            {
                System.out.println("first and last dates calculated successfully.");
                if (Tools.calcualteOtherFields())
                {
                    System.out.println("other movement fields calculated successfully.");
                    if (Tools.addTagsToFields())
                    {
                        System.out.println("tags added to fields successfully.");
                        if (Tools.writeNewMovements())
                            System.out.println("new movement file created successfully.");
                    }
                }
            }
        }
    }

}
